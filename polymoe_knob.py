# SINGLEPY_IGNORE_BEGIN
from enum import Enum

import cadquery as cq
try:
    from ocp_vscode import show_object
except:
    pass

from polymoe_config import *
# SINGLEPY_IGNORE_END


class KnobDirection(Enum):
    Upper = 0,
    RightUpper = 1,
    Right = 2,
    RightLower = 3,
    Lower = 4,
    LeftLower = 5,
    Left = 6,
    LeftUpper = 7


class KnobSide(Enum):
    Left = 1.0
    Right = -1.0


class KnobConfig:
    def __init__(self, config: PolyMoeConfig, x_position, y_position, z_position, direction: KnobDirection, side: KnobSide):
        self.density = config.knob_density
        self.x_position = x_position
        self.y_position = y_position
        self.z_position = z_position
        self.direction = direction
        self.knob_length = config.knob_length
        self.knob_width_max = config.knob_width_max
        self.knob_width_min = config.knob_width_min
        self.fillet_radius = config.knob_fillet_radius
        self.size_factor = config.knob_size_factor
        self.side = side


class Knob:
    def __init__(self, config: KnobConfig):
        self.config = config
        self._built = None

    def _build_cord_knob_sketch(self, workplane: cq.Workplane, knob_width: float) -> cq.Workplane:
        if self.config.direction == KnobDirection.Upper:
            start_direction_x = -1
            start_direction_y = 0
            target_direction_x = 2
            target_direction_y = 0
        elif self.config.direction == KnobDirection.RightUpper:
            start_direction_x = 0
            start_direction_y = 1
            target_direction_x = 1
            target_direction_y = -1
        elif self.config.direction == KnobDirection.Right:
            start_direction_x = 0
            start_direction_y = 1
            target_direction_x = 0
            target_direction_y = -2
        elif self.config.direction == KnobDirection.RightLower:
            start_direction_x = 1
            start_direction_y = 0
            target_direction_x = -1
            target_direction_y = -1
        elif self.config.direction == KnobDirection.Lower:
            start_direction_x = 1
            start_direction_y = 0
            target_direction_x = -2
            target_direction_y = 0
        elif self.config.direction == KnobDirection.LeftLower:
            start_direction_x = 0
            start_direction_y = -1
            target_direction_x = -1
            target_direction_y = 1
        elif self.config.direction == KnobDirection.Left:
            start_direction_x = 0
            start_direction_y = -1
            target_direction_x = 0
            target_direction_y = 2
        elif self.config.direction == KnobDirection.LeftUpper:
            start_direction_x = -1
            start_direction_y = 0
            target_direction_x = 1
            target_direction_y = 1
        else:
            raise Exception(f"unexpected direction {self.config.direction}")
        w = workplane.line(start_direction_x * knob_width, start_direction_y * knob_width)
        w = w.line(0.01 * start_direction_y, 0.01 * -start_direction_x)
        w = w.tangentArcPoint((knob_width * target_direction_x, knob_width * target_direction_y))
        w = w.close()
        return w

    def _build_knob(self) -> cq.Workplane:
        w = cq.Workplane("XZ").workplane(
            offset=(-self.config.y_position / 2 - self.config.knob_length * self.config.size_factor) * self.config.side.value)
        w = w.center(self.config.x_position, -self.config.z_position)
        w = self._build_cord_knob_sketch(w, knob_width=1.0 * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=0), knob_width=1.01 * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=self.config.knob_length / 2 * self.config.side.value * self.config.size_factor),
                                         knob_width=self.config.knob_width_max * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=0.01 * self.config.side.value),
                                         knob_width=self.config.knob_width_max * self.config.size_factor)
        w = self._build_cord_knob_sketch(
            w.workplane(offset=(self.config.knob_length / 2 - 0.01 + self.config.fillet_radius) * self.config.side.value * self.config.size_factor),
            knob_width=self.config.knob_width_min * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=0.01 * self.config.side.value * self.config.size_factor),
                                         knob_width=self.config.knob_width_min * self.config.size_factor)
        #w = w.loft(ruled=True,combine=False,clean=False)
        w = w.loft(ruled=False)
        return w


    def build(self) -> cq.Workplane:
        self._built = self._build_knob()
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig) -> cq.Assembly:
    assembly = cq.Assembly()
    for side in [KnobSide.Left, KnobSide.Right]:
        position = 0
        for direction in [KnobDirection.Upper, KnobDirection.RightUpper, KnobDirection.Right, KnobDirection.RightLower,
                          KnobDirection.Lower, KnobDirection.LeftLower, KnobDirection.Left, KnobDirection.LeftUpper]:
            knob_config = KnobConfig(config=config, x_position=position, y_position=100,
                                     z_position=0,
                                     direction=direction, side=side)
            knob = Knob(knob_config)
            knob_obj = knob.build()
            position += 100
            assembly.add(knob_obj)
            print(f"volume knob {direction} {side}: {knob.volume():.1f} cm^3")
            print(f"weight knob {direction} {side}: {knob.weight():.1f} g")

    return assembly


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        result.save("polymoe.step")
    if 'show_object' in globals():
        show_object(result)
    show_object(result, name="Knobs")
# SINGLEPY_IGNORE_END