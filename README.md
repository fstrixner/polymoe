# Polymoe - the polyphonic flute

<img src="images/polymoe_sml_overview.png" alt="Overview" width="100%"/>

This is a flute that can be played polyphonically with e.g. four independent voices or melody with chords over several octaves.
Depending on configuration the notes of a voice can be discrete (chromatic, diatonic, pentatonic etc.) like a usual flute or completely continuous like a violin or trombone.

## Inspiration

The key idea how to play a single flute with one finger was inspired by the family of 'Moe instruments by Bart Hopkin: http://barthopkin.com/instrumentarium/moe-flute/

<img src="http://barthopkin.com/wp-content/uploads/2017/03/01-03m-moeflute.jpg" alt="Moe Flute" width="25%"/>

Besides Bart Hopkins, Dániel Váczi and Tóbias Terebessy had a very similar idea independently: http://glissonic.com/

<img src="http://glissonic.com/img/glissotar_figure.png" alt="Glissotar" width="25%"/>

The extension of this idea, to use the remaining fingers for more voices is the most prominent new feature of the PolyMoe.

<img src="images/polymoe_m_overview.png" alt="Overview" width="25%"/>

Additionally some design aspects are similar to Native American style and Mayan single pipe and drone flutes: https://youtu.be/b8yfXBOeHvg

<img src="https://upload.wikimedia.org/wikipedia/commons/c/c8/Native_American_flute_Composite_Anatomy.jpg" alt="Native American Flute" width="25%"/>

The concept invented by Hopkins/Váczi/Terebessy works by having flexible magnetic strips spanned over an iron "pitch strip" that either has a slit for continuous pitch
or holes for discrete pitches. (The magnetic strip is made of rubber or flexible fabric with small magnetic particles - this is easily available)

<img src="images/fingerdown.png" alt="Overview" width="50%"/>

When pressing down the magnetic strip to the iron pitch strip with a finger, the part of the pitch strip above the finger gets sealed by the magnetic strip while the lower section of the iron pitch strip stays open. Thus the finger position regulates the length of an open sound pipe that defines the note pitch.  
The magnetic nature of the strip and the magnetizability of the iron strip seals the pipe above the finger air tight.
The distance between the magnetic strip and the iron pitch strip and the tension of the magnetic strip is fine tuned in a way that the magnetic strip lifts away from the iron strip as soon as the finger leaves it's position.

In the case of the PolyMoe, the flexible magnetic strips have two loops on each side. The loop size determines the height of the strips on the lower side on the "wings" strip holders.

<img src="images/wings_with_cords.png" alt="Overview" width="50%"/>

On the upper side the strips are clamped to the flute body via a "stringplate".

<img src="images/stringplate_with_cords.png" alt="Overview" width="50%"/>

Optionally the magnetic strips can have small haptic markers for the right position of e.g. the equal tempered semitones (professionals who solely play continuous pitch strips might prefer magnetic strips without markers so their fingers aren't drawn towards equal tempered tuning in cases where e.g. pure intervals might be preferrable) 

The mouthpiece is shaped like for a mouth harp with the pipe endings close-by, which should make tongue blocking techniques to rapidly mute and unmute voices and chords viable and allows to selectively blow only into a few or even a single pipe, depending on requirements of the played piece.

<img src="images/mouthpiece.png" alt="Overview" width="50%"/>

The PolyMoe pipes are concave. Thus the average pipe diameter of high notes is small so they don't get too breathy and larger for lower notes, so they can still be played without unintentional overblowing.
This property allows a big tonal range of several octaves but also shapes the timbre. (Depending on desired timbre and tonal range, different degrees of conicity are imaginable)

<img src="images/concave1.png" alt="Overview" width="25%"/>
<img src="images/concave2.png" alt="Overview" width="25%"/>

The current design of the PolyMoe is more a family of instruments than a single instrument. But because of the proposed modular nature, many different variants of the instrument can be built by reassembling the instrument parts as desired by the player - perhaps even between pieces:
* The iron pitch strips can be exchanged to be either a slit (for continuous) or with holes (for chromatic, diatonic, pentatonic etc. scales)
The pitch strips are held in place by the wings and the stringplate and are made from magnetizable and optimally rustproof material, like certain kinds of carbon steel.
Any combination of pitch strips is possible. So one can e.g. have hole-strips for chords and one or two continuous strips e.g. for vibrato and glissando on the melody line, interesting frequency-clash effects or pure (non equal-tempered) intonation of intervals.
When the distance between holes becomes too small, discrete pitch strips switch to continuous - that way higher notes can at least still be played (like the last part of a guitar string that has no bounds anymore)

<img src="images/pitchstrips.png" alt="Overview" width="100%"/>

* the octave range can be extended by attaching one or two "extension" parts behind the main flute. One needs iron pitch strips and magnetic strips that fit the overall length of the instrument, so those are needed in all lengths the instrument is planned to be used with.
<img src="images/disassembled.png" alt="Overview" width="100%"/>

* The different sizes of the instrument are chosen on purpose

    * Without an extension the instrument can be held in two hands with the hands staying in position (so the thumbs can rest on the same place, supporting the instrument)

  <img src="images/playersmall.png" alt="Overview" width="50%"/>

    * With the first extension the instrument length corresponds to what can comfortably still be reached with outstretched arms. Here the instrument is either put on a chair or held via staps around the neck
    
  <img src="images/playermedium.png" alt="Overview" width="50%"/>

  * With the second extension the instrument reaches down to the floor, so it can be played standing.
  
  <img src="images/playerlarge.png" alt="Overview" width="50%"/>

* There is a curved mouthpiece extension (like for a saxophone) that can be helpful when playing the instrument with several extensions in order to have a more comfortable playing position and to increase the reach of the hands towards lower notes

  <img src="images/curvedMouthpiece.png" alt="Overview" width="50%"/>


Extensions, wings, stripholder and body and cover are bound together via leather/fabric/rubber cords/loops. The advantage of that approach compared to gluing is that the instrument can be opened for maintenance - and it can be reconfigured (by adding extensions, exchanging pitch strips etc.)
<img src="images/cords1.png" alt="Overview" width="25%"/>
<img src="images/cords2.png" alt="Overview" width="25%"/>
<img src="images/cords3.png" alt="Overview" width="25%"/>
<img src="images/cords4.png" alt="Overview" width="25%"/>


## Pictures of the individual parts
Material unless written otherwise: a light wood (so the instrument doesn't get too heavy). E.g. Red western cedar wood, as often used for the larger Native American drone flutes 

### Body with Body Extensions
#### Body
<img src="images/body0.png" alt="Overview" width="25%"/>
<img src="images/body1.png" alt="Overview" width="25%"/>
<img src="images/body2.png" alt="Overview" width="25%"/>
<img src="images/body3.png" alt="Overview" width="25%"/>
<img src="images/body4.png" alt="Overview" width="25%"/>

#### Extension
<img src="images/extension1.png" alt="Overview" width="25%"/>
<img src="images/extension2.png" alt="Overview" width="25%"/>
<img src="images/extension3.png" alt="Overview" width="25%"/>
<img src="images/extension4.png" alt="Overview" width="25%"/>

### Cover with Cover Extensions
#### Cover
<img src="images/cover1.png" alt="Overview" width="25%"/>
<img src="images/cover2.png" alt="Overview" width="25%"/>
<img src="images/cover3.png" alt="Overview" width="25%"/>

#### Extension
<img src="images/coverextension1.png" alt="Overview" width="25%"/>
<img src="images/coverextension2.png" alt="Overview" width="25%"/>
<img src="images/coverextension3.png" alt="Overview" width="25%"/>
<img src="images/coverextension4.png" alt="Overview" width="25%"/>

### Mouthpiece

<img src="images/mouthpiece1.png" alt="Overview" width="25%"/>
<img src="images/mouthpiece2.png" alt="Overview" width="25%"/>

### Wings

<img src="images/wings1.png" alt="Overview" width="25%"/>
<img src="images/wings2.png" alt="Overview" width="25%"/>

### Strip Plate

<img src="images/stringplate1.png" alt="Overview" width="25%"/>
<img src="images/stringplate2.png" alt="Overview" width="25%"/>

### Carry Hook

<img src="images/hook1.png" alt="Overview" width="25%"/>
<img src="images/hook2.png" alt="Overview" width="25%"/>

### Curved Mouthpiece Extension
#### Body
<img src="images/curvermouthpiecebody1.png" alt="Overview" width="25%"/>
<img src="images/curvermouthpiecebody2.png" alt="Overview" width="25%"/>

#### Cover
<img src="images/curvermouthpiececover1.png" alt="Overview" width="25%"/>
<img src="images/curvermouthpiececover2.png" alt="Overview" width="25%"/>

## working with the CAD drawings

The CAD drawings were created with the Open Source programmatic, Python based CAD-package CadQuery (https://github.com/CadQuery/cadquery).
The main reason for that choice was that it allows instrument makers who aren't necessarily engineers or designers to alter many parameters without in-depth CAD knowledge.
This is important as the knowledge how to make a woodwind instrument sound good is a quite different skill from usual engineering capabilities - and good advise from instrument makers is super important here.
Thus most people don't have to understand the full Python based code, tuning the parameters should be sufficient for most cases.

### Use existing STEP and STL files (currently not checked in yet)
The STEP and STL files can be opened with any compatible CAD/CAM program or slicer.
As the STEP and STL files take up quite some space and the project is currently still work in progress, these files aren't checked in yet atm.
They can be generated with the "make_step_and_stl_files.py" script which should work on any Python installation where CadQuery is installed (see below)

Before generating the STEP/STIL files review polymoe_config.py whether the configuration options are set as desired. (Do not edit the config in polymoe_all.py - it will be overwritten during the generation process!)

Once generated, step files will reside in the step_files folder and stl files in the part_files folder.

### CadHub (work in progress)
The easiest way to alter the CAD design is to go to https://cadhub.xyz/u/NothanUmber/polymoe/ide

For many cases it is sufficient to play around with the parameters on the top of the file on this page and click on "Preview" again to see the altered design.
Once happy one can click to "Share" which generates a link with the full CAD design encoded into it that can e.g. be sent around via email.
If you have created an interesting modification that way please consider to send it to me, then I can look whether and how it makes sense to integrate that into the main project.

Later on it will be possible to export STEP files suitable for CNC carving directly from CadHub - this feature is currently still work in progress.

Disclaimer: Currently the render time on CadHub is limited to < 30s, so as the PolyMoe design is quite demanding, many more complex parameterizations cannot be rendered (if you get the message "network timeout" then this is the problem).
The CadHub team is working on getting rid of this restriction though. Until then the next approaches might be preferrable for full flexibility.

### CQEditor
One can download CQEditor, a very nice UI for CadQuery that works for Windows, Mac and Linux and contains everything that is needed. The latest release is already a little outdated, here an inofficial build that works well with PolyMoe: https://github.com/jmwright/CQ-editor/actions/runs/1770147960
Once downloaded, unzipped and started, open the main polymoe.py file and click on render.
Most relevant parameters are separated out in polymoe_config.py, so most people can ignore the rest.

Instead of working with polymoe_config.py, polymoe.py and the other files one can also open polymoe_all.py, which contains the entire program in one file with the configuration parameters on top (this is the file also used for CadHub).
In order to export a STEP file just ensure that the parameter "save_step" in polymoe_config.py (or polymoe_all.py if you work with that) is set to True.

When individual parts are desired instead of the full assembly just open and render the corresponding file (like polymoe_wings.py for the wings part). All part files are designed to also work standalone.

### advanced: CadQuery in virtualenv or Conda environment with separate editor and STEP viewer (or CQEditor)
In order to use CadQuery with an external Python editor (like PyCharm or Visual Studio Code) it is advisable to install CadQuery in a virtual environment.

The recommended steps with pyenv, virtualenv and poetry are:
1) Install a suitable Python version, currently I can confirm that Python 3.9 works. The imho recommended way to manage several Python distributions on the same machine is via pyenv. This is available for Linux, macOS (https://github.com/pyenv/pyenv) and Windows (https://github.com/pyenv-win/pyenv-win).
2) Install poetry, poetry manages dependencies and automatically creates virtual environments
3) install polymoe dependencies inside a virtual environment via poetry

        pyenv install 3.9.13
        cd /path/to/polymoe
        pyenv local 3.9.13
        pip install poetry
        poetry install

Alternatively one can install CadQuery in a Conda environment.
Just follow the instructions on the main page: https://github.com/CadQuery/cadquery
For visualization one can use a STEP viewer of choice - CQEditor also works as "just" a viewer of course and there are a lot of other choices (e.g. Moment of Inspiration is a commercial one with great triangulation capabilities - e.g. in order to save as high quality STL file).

## Next Steps

The PolyMoe is now migrating from the initial design phase into the prototyping phase, so some changes are to be expected.
People who iterate on building their own PolyMoe prototypes - please consider to give me feedback (or if desired even merge requests) regarding your findings!

Areas where changes are expected include (but are not limited to):
* the algorithm that computes the holes for non-continuous scales is most likely too simplistic and will likely not produce the correct pitches.
(E.g. holes below the lowest open hole can influece the pitch when they are close enough. And the hole diameter also has some influece.
The current algorithm just considers the hole position as the end of an idealized open pipe)
* in order to support several octaves of playing range, concave pipes appear helpful. Currently the pipes widen in both width and height direction.
One could consider to only increase height though and keep the width constant. The advantage would be that the strips stay parallel which might lead to
more consistent hand positions in different ranges and allow to reuse the same wings part no matter how many extensions are used (For variable width different wing parts with a corresponding width
have to be provided for each extension. An advantage could be that the height of the wings could then also be optimized for the overall extension-dependent instrument size).
A varying pipe width could also be helpful to develop muscle memory for pitch locations. And a square pipe cross section might be preferrable to a rectangular heigher-than-wider one timbre-wise.
* it has to be seen whether the cord binding approach for assembling the modular parts is air tight and stable enough. the mechanism might require some iterations to get right.
* The slow airchambers might require too much breath for several pipes. If that is the case one could experiment with making them smaller
* The current rounded mouthpiece extension is probably challenging for 3-axis CNC. If CNC manufacturing is desired, another functionally comparable design of that part might be preferrable
* It has to be seen whether the carry hook on the bottom of the instrument is comfortable for both carrying the (short) instrument with the hands and (for the longer one) as hook for a
saxophone-like carrying strap. Potentially different, optimized hooks could be preferrable for the different scenarios
* First iterations of the PolyMoe (or "Polyphonic Pipes" as they were called back then) experimented with a slidable covers on the opposite side of the air hole, an attached ring for the finger
and haptic markers on the flute body that allow to feel the current pitch before it is played.
This was changed to the 'Moe/Glissonic approach, because expectation was that it would be tricky to get the slider friction- and deadlock-free and air tight at the same time.
Such a slider-cover based design could still be worth experimenting with though. E.g. it allows to "dial in" a certain pitch and keep it that way for a longer time for one pipe,
freeing fingers to add additional voices on top. Thus even a combination of some slider based pipes and some magnetic strip based pipes might make sense to think about 

  <img src="images/polypipes_slider.png" alt="slider based polyphonic pipes (PolyMoe precursor)" width="20%"/>
* Depending on how well lower notes can be played because of the height of the flexible strip on longer instruments, an alternative approach might make sense that allows to keep the distance between flexible magnetic strip and metal pitch strip constant for all pitches:
One could provide "strip lifter rings", three rings that are connected on one side and which can be used to lift the flexible strip at one desired position. One finger can be put into e.g. the front ring to move it around. To decrease friction it might be an idea to use rolls at the outside and a ball bearing.
An additional advantage of the strip lifter could be that it can keep the flexible strip at one particular position even when the finger is removed, freeing the finger for other tasks. (Particularly if no ball bearings are used, so there is at least some friction that keeps the rings into place)

<img src="images/stringlifter.png" alt="Overview" width="50%"/>

<img src="images/stringlifter1.png" alt="Overview" width="20%"/>
<img src="images/stringlifter2.png" alt="Overview" width="20%"/>


## Copyright

The PolyMoe CAD drawings are licensed under CC-BY 2.0 https://creativecommons.org/licenses/by/2.0/

Thus you can use them to create instruments for private or commercial use. If you do so please let me know!
Would gladly add links to builds, variants and sources where PolyMoe-like instruments can be obtained!

Copyrights for assets used in this Readme:

'Moe Flute by Bart Hopkin

Glissotar by Váczi Dániel & Terebessy Tóbiás

Wood, Metal and Fabric Textures © Blender Foundation | cloud.blender.org

PolyMoe concept and CAD files by Ferdinand Strixner