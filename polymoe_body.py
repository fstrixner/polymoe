# SINGLEPY_IGNORE_BEGIN
from typing import List

import cadquery as cq

from polymoe_config import *
from polymoe_knob import KnobConfig, KnobDirection, KnobSide, Knob

from polymoe_util import continuous_connection
# SINGLEPY_IGNORE_END

class FluteBodyConfig:

    def __init__(self, config: PolyMoeConfig):
        self.config = config

        self.num_pipes = config.num_pipes
        self.pipe_width_at_bottom = config.pipe_width_at_bottom
        self.pipe_width_at_top = config.pipe_width_at_top
        self.pipe_height_at_bottom = config.pipe_height_at_bottom
        self.pipe_height_at_top = config.pipe_height_at_top
        self.pipe_border = config.pipe_border
        self.soundpipe_length = config.soundpipe_length
        self.extension_cuts = config.extension_cuts
        self.air_hole_length = config.air_hole_length
        self.wind_channel_height = config.wind_channel_height
        self.wind_channel_length = config.wind_channel_length
        self.wind_channel_ramp_length = config.wind_channel_ramp_length
        self.slow_airchamber_total_length = config.slow_airchamber_total_length
        self.mouthpiece_pipe_height = config.mouthpiece_pipe_height
        self.mouthpiece_pipe_width = config.mouthpiece_pipe_width
        self.mouthpiece_length = config.mouthpiece_length
        self.mouthpiece_outer_border = config.mouthpiece_outer_border
        self.mouthpiece_inner_border = config.mouthpiece_inner_border
        self.mouthpiece_bottom_border = config.mouthpiece_bottom_border
        self.mouthpiece_knob_size_factor = config.mouthpiece_knob_size_factor
        self.fillets = config.fillets
        self.x_fillets = self.fillets
        self.yz_fillets = self.fillets
        self.pipe_x_fillet_radius = config.pipe_x_fillet_radius if self.x_fillets else 0
        self.pipe_x__back_fillet_radius = config.pipe_x__back_fillet_radius if self.x_fillets else 0
        self.pipe_yz_fillet_radius = config.pipe_yz_fillet_radius if self.yz_fillets else 0
        self.mouthpiece_fillet_radius = config.mouthpiece_fillet_radius if self.yz_fillets else 0
        self.mouthpiece_front_fillet_radius = config.mouthpiece_front_fillet_radius if self.x_fillets else 0
        self.stacking_carving_length = config.body_stacking_carving_length
        self.hook_stacking_depth = config.body_hook_stacking_depth
        self.density = config.body_density

    @property
    def bounding_box(self) -> cq.Vector:
        total_length =  self.pipe_length + self.mouthpiece_length
        total_width = self.body_width_at_position(0)
        total_height = self.pipe_height_at_bottom + self.pipe_border
        return cq.Vector(total_length, total_width, total_height)

    @property
    def pipe_length(self):
        return self.soundpipe_length + self.air_hole_length + self.wind_channel_length\
               + self.wind_channel_ramp_length + self.slow_airchamber_length

    @property
    def slow_airchamber_length(self):
        return self.slow_airchamber_total_length - self.wind_channel_ramp_length

    #@property
    #def slow_airchamber_length(self):
    #    return self.pipe_length - self.soundpipe_length - self.air_hole_length - self.wind_channel_length\
    #           - self.wind_channel_ramp_length

    def pipe_height_at_position(self, position: float) -> float:
        return self.pipe_height_at_bottom\
               - (self.pipe_height_at_bottom - self.pipe_height_at_top) * position / self.pipe_length

    def pipe_width_at_position(self, position: float) -> float:
        return self.pipe_width_at_bottom\
                - (self.pipe_width_at_bottom - self.pipe_width_at_top) * position / self.pipe_length

    def body_width_at_position(self, position: float) -> float:
        return self.pipe_width_at_position(position) * self.num_pipes + self.pipe_border * (self.num_pipes + 1)

    def body_height_at_position(self, position: float) -> float:
        return self.pipe_height_at_position(position) + self.pipe_border

    @property
    def mouthpiece_width(self) -> float:
        return self.mouthpiece_pipe_width * self.num_pipes + self.mouthpiece_outer_border * 2 + self.mouthpiece_inner_border * (self.num_pipes - 1)

    @property
    def mouthpiece_height(self) -> float:
        return self.mouthpiece_pipe_height + self.mouthpiece_bottom_border

    @property
    def total_length(self):
        return self.pipe_length + self.mouthpiece_length


class FluteBody:
    def __init__(self, config: FluteBodyConfig):
        self.config = config
        self._built = None

    def build_width_height_pipe_sketch(self, workplane: cq.Workplane, width: float, height: float,
                                       pipe_width: float,
                                       outer_border_width: float,
                                       inner_border_width: float,
                                       border_height_delta: float,
                                       bottom_height: float, bottom_fillet_radius: float):
        w = workplane.center(-width / 2,
                             -height + self.config.pipe_yz_fillet_radius)
        w = w.line(0, height - self.config.pipe_yz_fillet_radius + border_height_delta)
        w = w.line(outer_border_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            w = w.line(0, -bottom_height - border_height_delta + bottom_fillet_radius)
            if self.config.yz_fillets:
                w = w.tangentArcPoint((bottom_fillet_radius, -bottom_fillet_radius))
            w = w.line(pipe_width - 2 * bottom_fillet_radius, 0)
            if self.config.yz_fillets:
                w = w.tangentArcPoint((bottom_fillet_radius, bottom_fillet_radius))
            w = w.line(0, bottom_height + border_height_delta - bottom_fillet_radius)
            if pipe_num < self.config.num_pipes - 1:
                w = w.line(inner_border_width, 0)
        w = w.line(outer_border_width, 0)
        w = w.line(0, -height + self.config.pipe_yz_fillet_radius - border_height_delta - outer_border_width)
        if self.config.yz_fillets:
            w = w.tangentArcPoint((-self.config.pipe_yz_fillet_radius, -self.config.pipe_yz_fillet_radius))
        w = w.line(-width + 2 * self.config.pipe_yz_fillet_radius, 0)
        if self.config.yz_fillets:
            w = w.tangentArcPoint((-self.config.pipe_yz_fillet_radius, self.config.pipe_yz_fillet_radius))
        w = w.close()
        return w

    def _build_pipe_sketch(self, workplane: cq.Workplane, position: float, border_height_delta: float,
                           bottom_height: float, bottom_fillet_radius: float, height: float = None,
                           outer_border_width: float = None, inner_border_width: float = None
                           ):
        if height is None:
            height = self.config.pipe_height_at_position(position)
        if outer_border_width is None:
            outer_border_width = self.config.pipe_border
        if inner_border_width is None:
            inner_border_width = self.config.pipe_border
        return self.build_width_height_pipe_sketch(workplane=workplane,
                                                   width=self.config.pipe_width_at_position(position) *
                                                          self.config.num_pipes + inner_border_width
                                                          * (self.config.num_pipes - 1) + outer_border_width * 2,
                                                   height=height,
                                                   pipe_width=self.config.pipe_width_at_position(position),
                                                   outer_border_width=outer_border_width,
                                                   inner_border_width=inner_border_width,
                                                   border_height_delta=border_height_delta,
                                                   bottom_height=bottom_height,
                                                   bottom_fillet_radius=bottom_fillet_radius)

    def _build_pipe_soundpipe_sketch(self, workplane: cq.Workplane, position: float, height: float = None,
                                     bottom_height: float = None,
                                     outer_border_width: float = None,
                                     inner_border_width: float = None
                                     ) -> cq.Workplane:
        if bottom_height is None:
            bottom_height = self.config.pipe_height_at_position(position)
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=-self.config.wind_channel_height / 2,
                                       bottom_height=bottom_height,
                                       bottom_fillet_radius=self.config.pipe_yz_fillet_radius,
                                       height=height,
                                       outer_border_width=outer_border_width,
                                       inner_border_width=inner_border_width)

    def _build_pipe_air_hole_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=0,
                                       bottom_height=self.config.pipe_height_at_position(position),
                                       bottom_fillet_radius=self.config.pipe_yz_fillet_radius)

    def _build_pipe_body_block_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        if self.config.yz_fillets:
            bottom_radius = self.config.wind_channel_height - 0.001
        else:
            bottom_radius = 0
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=0,
                                       bottom_height=self.config.wind_channel_height,
                                       bottom_fillet_radius=bottom_radius)

    def _build_pipe_slow_airchamber_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=0,
                                       bottom_height=self.config.pipe_height_at_position(
                                           position),
                                       bottom_fillet_radius=self.config.mouthpiece_fillet_radius)

    def build_mouth_piece(self, workplane: cq.Workplane, extra_height=0.0) -> cq.Workplane:
        return self.build_width_height_pipe_sketch(workplane=workplane,
                                                   width=self.config.mouthpiece_width,
                                                   height=self.config.mouthpiece_height+extra_height,
                                                   pipe_width=self.config.mouthpiece_pipe_width,
                                                   outer_border_width=self.config.mouthpiece_outer_border,
                                                   inner_border_width=self.config.mouthpiece_inner_border,
                                                   border_height_delta=0,
                                                   bottom_height=self.config.mouthpiece_height,
                                                   bottom_fillet_radius=self.config.mouthpiece_fillet_radius).translate((0, 0, extra_height))

    def build_soundpipe(self, position: float, length: float,
                        lower_height: float = None, upper_height: float = None,
                        lower_bottom_height: float = None, upper_bottom_height: float = None,
                        outer_border_width: float = None,
                        inner_border_width: float = None
                        ):
        # if self.config._x_fillets:
        #     soundpipe_fillet_sketch = self._build_pipe_soundpipe_sketch(
        #         cq.Workplane("YZ").workplane(offset=position, origin=(0,0)), position=position, offset=0.0)
        #     sound_pipe_fillet = continuous_endpiece(
        #         soundpipe_fillet_sketch.wires().val(),
        #         fillet_radius=self.config.pipe_x__back_fillet_radius
        #     )

        soundpipe_lower = self._build_pipe_soundpipe_sketch(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position,
            height=lower_height, bottom_height=lower_bottom_height,
            outer_border_width=outer_border_width,
            inner_border_width=inner_border_width)
        position += length #- self.config.pipe_x__back_fillet_radius
        soundpipe_upper = self._build_pipe_soundpipe_sketch(
            soundpipe_lower.workplane(offset=length, #- self.config.pipe_x__back_fillet_radius,
                                      origin=(0, 0)), position=position, height=upper_height,
            bottom_height=upper_bottom_height,
            outer_border_width=outer_border_width,
            inner_border_width=inner_border_width)
        sound_pipe = soundpipe_upper.loft()
        return sound_pipe

    def build_stacking_carving(self, position: float) -> cq.Workplane:
        stacking_position = position - self.config.stacking_carving_length
        stacking_carving = self.build_soundpipe(stacking_position, length=self.config.stacking_carving_length,
                                                lower_height=self.config.pipe_height_at_position(
                                                    stacking_position),
                                                upper_height=self.config.pipe_height_at_position(
                                                    position),
                                                lower_bottom_height=self.config.pipe_height_at_position(
                                                    stacking_position),
                                                upper_bottom_height=self.config.pipe_height_at_position(
                                                    position),
                                                outer_border_width = self.config.pipe_border / 2,
                                                inner_border_width = self.config.pipe_border)
        return stacking_carving

    def build(self) -> List[cq.Workplane]:
        sound_pipe_extensions = []
        remaining_soundpipe = self.config.soundpipe_length
        position = 0
        for extension_length in self.config.extension_cuts:
            if self.config.stacking_carving_length != 0:
                stacking_carving = self.build_stacking_carving(position)

            knob_config_upper = KnobConfig(self.config.config, x_position=position,
                                           y_position=self.config.body_width_at_position(position),
                                           z_position=self.config.wind_channel_height / 2,
                                           direction=KnobDirection.RightLower,
                                           side=KnobSide.Left)
            cord_knob_upper_left = Knob(knob_config_upper).build()
            knob_config_upper.side = KnobSide.Right
            cord_knob_upper_right = Knob(knob_config_upper).build()
            knob_config_upper.z_position += self.config.body_height_at_position(position) - knob_config_upper.knob_width_max
            knob_config_upper.direction = KnobDirection.Right
            cord_knob_upper_right_down = Knob(knob_config_upper).build()
            knob_config_upper.side = KnobSide.Left
            cord_knob_upper_left_down = Knob(knob_config_upper).build()
            extension = cq.Workplane().add(self.build_soundpipe(position, length=extension_length))
            position += extension_length
            knob_config_lower = KnobConfig(self.config.config, x_position=position,
                                           y_position=self.config.body_width_at_position(position),
                                           z_position=self.config.wind_channel_height / 2,
                                           direction=KnobDirection.LeftLower,
                                           side=KnobSide.Left)
            cord_knob_lower_left = Knob(knob_config_lower).build()
            knob_config_lower.side = KnobSide.Right
            cord_knob_lower_right = Knob(knob_config_lower).build()
            knob_config_lower.z_position += self.config.body_height_at_position(position) - knob_config_lower.knob_width_max
            knob_config_lower.direction = KnobDirection.Left
            cord_knob_lower_right_down = Knob(knob_config_lower).build()
            knob_config_lower.side = KnobSide.Left
            cord_knob_lower_left_down = Knob(knob_config_lower).build()
            extension = extension.union(cord_knob_upper_right, clean=False)   # TODO: find out why clean leads to defective surfaces here
            extension = extension.union(cord_knob_upper_left, clean=False)
            extension = extension.union(cord_knob_lower_right, clean=False)
            extension = extension.union(cord_knob_lower_left, clean=False)
            extension = extension.union(cord_knob_upper_right_down, clean=False)
            extension = extension.union(cord_knob_upper_left_down, clean=False)
            extension = extension.union(cord_knob_lower_right_down, clean=False)
            extension = extension.union(cord_knob_lower_left_down, clean=False)
            if self.config.stacking_carving_length != 0:
                extension = extension.add(stacking_carving)  # TODO: why doesn't union work here?
            sound_pipe_extensions.append(extension)
            remaining_soundpipe -= extension_length
        remaining_soundpipe -= self.config.pipe_x__back_fillet_radius
        knob_config_upper = KnobConfig(self.config.config, x_position=position,
                                       y_position=self.config.body_width_at_position(position),
                                       z_position=self.config.wind_channel_height / 2,
                                       direction=KnobDirection.RightLower,
                                       side=KnobSide.Left)
        cord_knob_left = Knob(knob_config_upper).build()
        knob_config_upper.side = KnobSide.Right
        cord_knob_right = Knob(knob_config_upper).build()
        knob_config_upper.z_position += self.config.body_height_at_position(position) - knob_config_upper.knob_width_max
        knob_config_upper.direction = KnobDirection.Right
        cord_knob_right_down = Knob(knob_config_upper).build()
        knob_config_upper.side = KnobSide.Left
        cord_knob_left_down = Knob(knob_config_upper).build()
        sound_pipe = self.build_soundpipe(position, remaining_soundpipe)
        if self.config.stacking_carving_length != 0:
            stacking_carving = self.build_stacking_carving(position)
            sound_pipe.add(stacking_carving) # TODO: why doesn't union work here?
        position += remaining_soundpipe

        air_hole_lower = self._build_pipe_air_hole_sketch(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
        position += self.config.air_hole_length
        air_hole_upper = self._build_pipe_air_hole_sketch(
            air_hole_lower.workplane(offset=self.config.air_hole_length
                                     ,origin=(0, 0)), position=position)
        air_hole = air_hole_upper.loft()

        # if self.config._x_fillets:
        #     air_hole_to_block_connection_lower = self._build_pipe_air_hole_sketch(
        #         workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
        #     position += self.config.pipe_x_fillet_radius
        #     air_hole_to_block_connection_upper = self._build_pipe_body_block_sketch(workplane= air_hole_to_block_connection_lower.workplane(
        #         offset=self.config.pipe_x_fillet_radius, origin=(0, 0)), position=position)
        #     air_hole_to_block_connection = continuous_connection(
        #         air_hole_to_block_connection_lower.wires().val(),
        #         air_hole_to_block_connection_upper.wires().val(),
        #         invert_normals=True)

        block_lower = self._build_pipe_body_block_sketch(
            cq.Workplane("YZ", origin=(0, 0)).workplane(offset=position, origin=(0, 0)), position=position)
        position += self.config.wind_channel_length
        block_upper = self._build_pipe_body_block_sketch(
            block_lower.workplane(offset=self.config.wind_channel_length, origin=(0, 0)), position=position)
        block = block_upper.loft()

        if self.config.x_fillets:
            block_ramp_connection_1 = self._build_pipe_body_block_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += 0.1
            block_ramp_connection_2 = self._build_pipe_body_block_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.wind_channel_ramp_length - 0.2
            block_ramp_connection_3 = self._build_pipe_slow_airchamber_sketch(workplane=cq.Workplane("YZ").workplane(
                offset=position, origin=(0, 0)), position=position)
            position += 0.1
            block_ramp_connection_4 = self._build_pipe_slow_airchamber_sketch(workplane= cq.Workplane("YZ").workplane(
                offset=position, origin=(0, 0)), position=position)
            block_ramp = continuous_connection(
                block_ramp_connection_1.wires().val(),
                block_ramp_connection_4.wires().val(),
                invert_normals=True,
                middle_wires=[block_ramp_connection_2.wires().val(), block_ramp_connection_3.wires().val()]
            )
        else:
            block_ramp_lower = self._build_pipe_body_block_sketch(
                cq.Workplane("YZ", origin=(0, 0)).workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.wind_channel_ramp_length
            block_ramp_upper = self._build_pipe_slow_airchamber_sketch(
                block_ramp_lower.workplane(offset=self.config.wind_channel_ramp_length, origin=(0, 0)), position=position)
            block_ramp = block_ramp_upper.loft()

        slow_airchamber_lower = self._build_pipe_slow_airchamber_sketch(
            cq.Workplane("YZ", origin=(0, 0)).workplane(offset=position, origin=(0, 0)), position=position)
        position += self.config.slow_airchamber_length
        slow_airchamber_upper = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_lower.workplane(offset=self.config.slow_airchamber_length, origin=(0, 0)), position=position)
        slow_airchamber = slow_airchamber_upper.loft()

        if self.config.x_fillets:
            pipe_to_mouthpiece__connection_1 = self._build_pipe_slow_airchamber_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += 0.1
            pipe_to_mouthpiece__connection_2 = self._build_pipe_slow_airchamber_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8  - 0.1
            pipe_to_mouthpiece__connection_3 = self._build_pipe_slow_airchamber_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 * 6 - 0.1
            pipe_to_mouthpiece__connection_4 = self.build_mouth_piece(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            position += 0.1
            pipe_to_mouthpiece__connection_5 = self.build_mouth_piece(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            mouthpiece1 = continuous_connection(
                pipe_to_mouthpiece__connection_1.wires().val(),
                pipe_to_mouthpiece__connection_5.wires().val(),
                invert_normals=True,
                middle_wires=[pipe_to_mouthpiece__connection_2.wires().val(),
                              pipe_to_mouthpiece__connection_3.wires().val(),
                              pipe_to_mouthpiece__connection_4.wires().val()
                              ]
            )
        else:
            mouthpiece_lower = self._build_pipe_slow_airchamber_sketch(
                cq.Workplane("YZ", origin=(0,0)).workplane(offset=position, origin=(0, 0)), position=position)
            mouthpiece_middle = self.build_mouth_piece(
                mouthpiece_lower.workplane(
                    offset=self.config.mouthpiece_length / 8 * 7,
                    origin=(0, 0)
                ))
            mouthpiece1 = mouthpiece_middle.loft()
            position += self.config.mouthpiece_length / 8 * 7

        mouthpiece_middle2 = self.build_mouth_piece(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
        mouthpiece_upper = self.build_mouth_piece(
            mouthpiece_middle2.workplane(
                offset=self.config.mouthpiece_length / 8 * 1, origin=(0, 0)))
        mouthpiece2 = mouthpiece_upper.loft()
        position += self.config.mouthpiece_length / 8 * 1

        knob_config_mouthpiece = KnobConfig(self.config.config, x_position=position,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.LeftLower,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.z_position += self.config.mouthpiece_height
        knob_config_mouthpiece.direction = KnobDirection.Left
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()

        if self.config.x_fillets:
            flute_body = (cq.Workplane()
                          #.union(sound_pipe_fillet, clean=True)
                          .union(sound_pipe, clean=True)
                          .union(air_hole, clean=True)
                          #.union(air_hole_to_block_connection, clean=True)
                          .union(block, clean=True)
                          .union(block_ramp, clean=True)
                          .union(slow_airchamber, clean=True)
                          .union(mouthpiece1, clean=True)
                          .union(mouthpiece2, clean=True)
                          .union(cord_knob_left, clean=False) # TODO: why is cleaning leading to defective surfaces in these cases?
                          .union(cord_knob_right, clean=False)
                          .union(cord_knob_left_down, clean=False)
                          .union(cord_knob_right_down, clean=False)
                          .union(cord_knob_mouthpiece_left, clean=False)
                          .union(cord_knob_mouthpiece_right, clean=False)
                          .union(cord_knob_mouthpiece_left_down, clean=False)
                          .union(cord_knob_mouthpiece_right_down, clean=False)
                          )
        else:
            flute_body = sound_pipe.union(
                air_hole.union(
                    block.union(
                        block_ramp.union(
                            slow_airchamber.union(
                                mouthpiece1.union(
                                    mouthpiece2, clean=True
                                ), clean=True
                            ), clean=True
                        ), clean=True
                    ), clean=True
                ), clean=True
            ).union(cord_knob_left, clean=False).union(cord_knob_right, clean=False).union(cord_knob_left_down, clean=False).union(cord_knob_right_down, clean=False) \
                .union(cord_knob_mouthpiece_left, clean=False).union(cord_knob_mouthpiece_right, clean=False) \
                .union(cord_knob_mouthpiece_left_down, clean=False).union(cord_knob_mouthpiece_right_down, clean=False) # TODO: why is cleaning leading to defective surfaces in these cases?

        self._built = sound_pipe_extensions
        self._built.append(flute_body)

        return self._built

    def add_stacking(self) -> List[cq.Workplane]:
        if self._built is None:
            self.build()
        if self.config.stacking_carving_length != 0:
            for index in range(0, len(self._built) - 1):
                self._built[index] = self._built[index].cut(self._built[index + 1], clean=False)  # TODO: find out why clean doesn't work here
        return self._built

    def carve_mouthpiece_stacking(self, mouthpiece_cover_obj: cq.Workplane):
        if self._built is None:
            self.build()
        if self.config.stacking_carving_length != 0:
            self._built[-1] = self._built[-1].cut(mouthpiece_cover_obj, clean=False)  # TODO: find out why clean doesn't work here
        return self._built

    def carve_hook_stacking(self, carrying_hook_obj: cq.Workplane):
        if self._built is None:
            self.build()
        if self.config.hook_stacking_depth != 0:
            self._built[-1] = self._built[-1].cut(carrying_hook_obj, clean=False)  # TODO: find out why clean doesn't work here
        return self._built


    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig) -> cq.Assembly:
    from polymoe_carry_hook import CarryHookConfig, CarryHook
    from polymoe_cover import CoverConfig
    from polymoe_stringplate import StringPlateConfig
    from polymoe_mouthpiece_coverplate import MouthpieceCoverplate, MouthpieceCoverplateConfig

    body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(body_config)
    stringplate_config = StringPlateConfig(body_config, cover_config)
    hook_config = CarryHookConfig(body_config, stringplate_config)
    mouthpiece_coverplate_config = MouthpieceCoverplateConfig(body_config, cover_config)

    # print(f"height at 700: {body_config.pipe_height_at_position(700)}")
    # print(f"width at 700: {body_config.pipe_width_at_position(700)}")
    hook = CarryHook(hook_config)
    hook_obj = hook.build()
    mouthpiece_coverplate = MouthpieceCoverplate(mouthpiece_coverplate_config)
    mouthpiece_coverplate_obj = mouthpiece_coverplate.build()

    body = FluteBody(body_config)
    body.carve_hook_stacking(hook_obj)
    body.carve_mouthpiece_stacking(mouthpiece_coverplate_obj)
    body_with_extensions_objs = body.add_stacking()
    volume = body.volume()
    weight = body.weight()
    print(f"volume body: {volume:.1f} cm^3")
    print(f"weight body: {weight:.1f} g")
    bounding_box = body_config.bounding_box
    print(f"bounding box: length: {bounding_box.x/10:.1f} cm, width: {bounding_box.y/10:.1f} cm, height: {bounding_box.z/10:.1f} cm")

    assembly = cq.Assembly()
    for index, part in enumerate(body_with_extensions_objs):
        #cq.exporters.export(part, f"polymoe_part_{index}.step", cq.exporters.ExportTypes.STEP)
        assembly.add(part)
    return assembly


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        result.save("polymoe.step")
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END