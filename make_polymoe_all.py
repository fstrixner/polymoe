def feed_file(file_name, output_stream):
    output_stream.write(f"# {file_name}\n")
    with open(file_name, 'r') as input_script:
        skip = False
        for line in input_script.readlines():
            if 'SINGLEPY_IGNORE_BEGIN' in line:
                skip = True
            elif 'SINGLEPY_IGNORE_END' in line:
                skip = False
            elif not skip:
                output_stream.write(line)
    output_stream.write("\n")

def make_all():
    with open('polymoe_all.py', 'w') as single_script:
        single_script.write("'''\n")
        single_script.write("This is a concatenated version for Cadhub and CQEditor. For actual work on the scripts the multi-file version from the repository is recommended.\n")
        single_script.write("The concatenated version can be regenerated via 'make_polymoe_all.py'\n")
        single_script.write("\n")
        single_script.write("This is a flute that can be played polyphonically with e.g. four independent voices or melody with chords over several octaves. Depending on configuration the notes of a voice can be discrete (chromatic, diatonic, pentatonic etc.) like a usual flute or completely continuous like a violin or trombone.\n")
        single_script.write("\n")
        single_script.write("For full description look here: https://gitlab.com/fstrixner/polymoe\n")
        single_script.write("\n")
        single_script.write("To CadHub users: Many features increase the rendering time over the current 30s limit of CadHub. In order to use the full configurations besides the demo config 'CH', better use CQEditor or a Conda based local CadQuery installation for the moment. Afaik the 30s limit is supposed to be eliminated in newer CadHub versions - then the full PolyMoe configurations should be usable.\n")
        single_script.write("'''\n")

        single_script.write("\n")
        single_script.write("from typing import List, Union, Dict, Any\n")
        single_script.write("from enum import Enum\n")
        single_script.write("import cadquery as cq\n")
        single_script.write("import math\n")
        single_script.write("\n")

        feed_file('polymoe_config.py', single_script)
        feed_file('polymoe_util.py', single_script)
        feed_file('polymoe_knob.py', single_script)
        feed_file('polymoe_body.py', single_script)
        feed_file('polymoe_wings.py', single_script)
        feed_file('polymoe_cover.py', single_script)
        feed_file('polymoe_stringplate.py', single_script)
        feed_file('polymoe_pitch_strips.py', single_script)
        feed_file('polymoe_mouthpiece_coverplate.py', single_script)
        feed_file('polymoe_carry_hook.py', single_script)
        feed_file('polymoe_curved_mouthpiece.py', single_script)
        feed_file('polymoe.py', single_script)

        single_script.write("\n")
        single_script.write("config = PolyMoeConfig()\n")
        single_script.write("assembly = main(config)\n")
        single_script.write("if config.save_step:\n")
        single_script.write("   assembly.save('polymoe.step')\n")
        single_script.write("if 'show_object' in globals():\n")
        single_script.write("   compound = assembly.toCompound()\n")
        single_script.write("   show_object(compound)\n")

if __name__ == '__main__':
    make_all()