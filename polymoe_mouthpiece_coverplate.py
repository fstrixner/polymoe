# SINGLEPY_IGNORE_BEGIN
import cadquery as cq

from polymoe_body import FluteBody
from polymoe_body import FluteBodyConfig
from polymoe_cover import CoverConfig, Cover
from polymoe_knob import KnobConfig, KnobDirection, KnobSide, Knob
from polymoe_util import continuous_endpiece
from polymoe_config import *
# SINGLEPY_IGNORE_END

class MouthpieceCoverplateConfig:
    def __init__(self, flute_body_config: FluteBodyConfig, cover_config: CoverConfig):
        self._polymoe_config = flute_body_config.config

        self.density = self._polymoe_config.mouthpiece_coverplate_density
        self.length = self._polymoe_config.mouthpiece_coverplate_length
        self.flute_body_config = flute_body_config
        self.cover_config = cover_config
        self.mouthpiece_front_fillet_radius = flute_body_config.mouthpiece_front_fillet_radius
        self.mouthpiece_fillet_radius = flute_body_config.mouthpiece_fillet_radius
        self.mouthpiece_knob_size_factor = flute_body_config.mouthpiece_knob_size_factor
        self.startpos = flute_body_config.total_length - flute_body_config.pipe_x__back_fillet_radius
        self.use_curved_mouthpiece = self._polymoe_config.use_curved_mouthpiece
        self.stacking_carving_length = flute_body_config.stacking_carving_length
        self.mouthpiece_pipe_width = flute_body_config.mouthpiece_pipe_width
        self.mouthpiece_pipe_height = flute_body_config.mouthpiece_pipe_height
        self.mouthpiece_outer_border = flute_body_config.mouthpiece_outer_border
        self.mouthpiece_inner_border = flute_body_config.mouthpiece_inner_border
        self.mouthpiece_width = flute_body_config.mouthpiece_width
        self.mouthpiece_height = flute_body_config.mouthpiece_height
        self.mouthpiece_bottom_border = flute_body_config.mouthpiece_bottom_border
        self.stacking_carving_height = cover_config.stacking_carving_height
        self.curvedmouthpiece_length1 = self._polymoe_config.curvedmouthpiece_length1
        self.curvedmouthpiece_length2 = self._polymoe_config.curvedmouthpiece_length2
        self.curvedmouthpiece_length3 = self._polymoe_config.curvedmouthpiece_length3
        self.curvedmouthpiece_length4 = self._polymoe_config.curvedmouthpiece_length4
        self.mouthpiece_coverplate_length = self._polymoe_config.mouthpiece_coverplate_length


class MouthpieceCoverplate:
    def __init__(self, config: MouthpieceCoverplateConfig):
        self._built = None
        self.config = config
        self.flute_body = FluteBody(config.flute_body_config)
        self.cover = Cover(config.cover_config)

    def _build_mouth_piece(self, workplane: cq.Workplane, extra_height: float):
        w = self.flute_body.build_mouth_piece(workplane, extra_height)
        w = w.center(self.config.flute_body_config.mouthpiece_width / 2,
                     self.config.flute_body_config.mouthpiece_width - self.config.flute_body_config.pipe_yz_fillet_radius)
        return w

    def build_stacking_carving_body(self, position: float):
        body_stacking_body_lower = self.flute_body.build_width_height_pipe_sketch(
            workplane=cq.Workplane("YZ").workplane(offset=position),
            width=self.config.mouthpiece_width - self.config.mouthpiece_outer_border,
            height=self.config.mouthpiece_height,
            pipe_width=self.config.mouthpiece_pipe_width,
            outer_border_width=self.config.mouthpiece_outer_border / 2,
            inner_border_width=self.config.mouthpiece_inner_border,
            border_height_delta=0,
            bottom_height=self.config.mouthpiece_height,
            bottom_fillet_radius=self.config.mouthpiece_fillet_radius)
        body_stacking_body_upper = self.flute_body.build_width_height_pipe_sketch(
            workplane=body_stacking_body_lower.workplane(offset=self.config.stacking_carving_length, origin=(0, 0)),
            width=self.config.mouthpiece_width - self.config.mouthpiece_outer_border,
            height=self.config.mouthpiece_height,
            pipe_width=self.config.mouthpiece_pipe_width,
            outer_border_width=self.config.mouthpiece_outer_border / 2,
            inner_border_width=self.config.mouthpiece_inner_border,
            border_height_delta=0,
            bottom_height=self.config.mouthpiece_height,
            bottom_fillet_radius=self.config.mouthpiece_fillet_radius)
        body_stacking = body_stacking_body_upper.loft(ruled=True)
        return body_stacking

    def build_stacking_carving_cover(self, position: float):
        cover_stacking = cq.Workplane("YZ").workplane(offset=position)\
            .center(0, self.config.mouthpiece_bottom_border / 2)\
            .rect(self.config.mouthpiece_width - self.config.mouthpiece_outer_border,
                 self.config.mouthpiece_bottom_border + self.config.stacking_carving_height * 2)\
            .extrude(self.config.stacking_carving_length)
        return cover_stacking

    def build_stacking_carving(self, position: float):
        stacking_position = position - self.config.stacking_carving_length
        body_stacking = self.build_stacking_carving_body(stacking_position)
        cover_stacking = self.build_stacking_carving_cover(stacking_position)
        return body_stacking.union(cover_stacking)

    def build(self):
        position = self.config.startpos

        if self.config.stacking_carving_length != 0:
            stacking = self.build_stacking_carving(position)

        mouthpiece_fillet_sketch = self.flute_body.build_mouth_piece(
            cq.Workplane("YZ").workplane(offset=position),
            extra_height=0)
        mouthpiece_base_lower = mouthpiece_fillet_sketch.extrude(self.config.length)
        mouthpiece_fillet_sketch = self.cover.build_mouthpiece_cover_sketch(
            cq.Workplane("YZ").workplane(offset=position))
        mouthpiece_base_upper = mouthpiece_fillet_sketch.extrude(self.config.length)

        knob_config_mouthpiece = KnobConfig(self.config._polymoe_config, x_position=position,
                                            y_position=self.config.flute_body_config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.Right,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.z_position += self.config.flute_body_config.mouthpiece_height
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()

        position += self.config.length

        mouthpiece_fillet_lower_sketch = self.flute_body.build_mouth_piece(
            cq.Workplane("YZ").workplane(offset=position),
            extra_height=self.config.mouthpiece_front_fillet_radius * 2)
        if self.config.mouthpiece_front_fillet_radius > 0:
            mouthpiece_lower_fillet = continuous_endpiece(mouthpiece_fillet_lower_sketch.wires().val(),
                                                          self.config.mouthpiece_front_fillet_radius,
                                                          invert_normals=True)
        else:
            mouthpiece_lower_fillet = mouthpiece_fillet_lower_sketch.extrude(self.config._polymoe_config.mouthpiece_front_fillet_radius)
        # position += self.config.mouthpiece_front_fillet_radius

        mouthpiece_fillet_upper_sketch = self.cover.build_mouthpiece_cover_sketch(
            cq.Workplane("YZ").workplane(offset=position))
        if self.config.mouthpiece_front_fillet_radius > 0:
            mouthpiece_upper_fillet = continuous_endpiece(mouthpiece_fillet_upper_sketch.wires().val(),
                                                          self.config.mouthpiece_front_fillet_radius,
                                                          invert_normals=True)
        else:
            mouthpiece_upper_fillet = mouthpiece_fillet_upper_sketch.extrude(self.config._polymoe_config.mouthpiece_front_fillet_radius)

        #position += self.config.mouthpiece_front_fillet_radius

        coverplate = cq.Workplane().union(mouthpiece_base_lower, clean=True).union(mouthpiece_base_upper, clean=True)
        if self.config.stacking_carving_length != 0:
            coverplate = coverplate.union(stacking, clean=True)
        coverplate = (coverplate.union(mouthpiece_lower_fillet, clean=True)
            .union(mouthpiece_upper_fillet, clean=True, tol=0.2)
            .union(cord_knob_mouthpiece_left, clean=True).union(cord_knob_mouthpiece_right, clean=True)
            .union(cord_knob_mouthpiece_left_down, clean=True).union(cord_knob_mouthpiece_right_down, clean=True))

        self._built = coverplate
        return self._built

    def move_coverplate_behind_curved_mouthpiece(self):
        if self._built is None:
            self.build()
        angle = 90
        mouthpiece_cover_height = self.config.flute_body_config.mouthpiece_height\
                                  + self.config.flute_body_config.mouthpiece_outer_border\
                                  + self.config.cover_config.height
        offset = cq.Vector(self.config.curvedmouthpiece_length1 - self.config.curvedmouthpiece_length3
                           - self.config.cover_config.height
                           - self.config.mouthpiece_coverplate_length
                           -0.128, # TODO where is this constant coming from (wrong positioning of rotation axis)?
                           0,
                           -self.config.curvedmouthpiece_length2 - self.config.curvedmouthpiece_length4
                           - mouthpiece_cover_height / 2
                           -self.config.length
                           + 3.8   # TODO where is this constant coming from (wrong positioning of rotation axis)?
        )
        position = self.config.startpos + self.config.length
        self._built = self._built.rotate(axisStartPoint=cq.Vector(position, -1, 0), axisEndPoint=cq.Vector(position, 1, 0), angleDegrees=angle).translate(offset)
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig):
    flute_body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(flute_body_config)
    coverplate_config = MouthpieceCoverplateConfig(flute_body_config, cover_config)
    coverplate = MouthpieceCoverplate(coverplate_config)
    coverplate_obj = coverplate.build()
    volume = coverplate.volume()
    weight = coverplate.weight()
    print(f"volume coverplate: {volume:.1f} cm^3")
    print(f"weight coverplate: {weight:.1f} g")

    return coverplate_obj


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END