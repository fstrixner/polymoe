# SINGLEPY_IGNORE_BEGIN
from enum import Enum
from typing import List

import cadquery as cq

from polymoe_body import FluteBodyConfig, FluteBody
from polymoe_cover import Cover, CoverConfig
from polymoe_knob import KnobConfig, KnobDirection, KnobSide, Knob
from polymoe_mouthpiece_coverplate import MouthpieceCoverplateConfig, MouthpieceCoverplate
from polymoe_util import continuous_connection
from polymoe_config import *
# SINGLEPY_IGNORE_END

class CurvedMouthpieceConfig:
    def __init__(self, body_config: FluteBodyConfig, cover_config: CoverConfig,
                 mouthpiece_coverplate_config: MouthpieceCoverplateConfig):
        self._polymoe_config = body_config.config

        self.density = self._polymoe_config.curvedmouthpiece_density
        self.body_config = body_config
        self.cover_config = cover_config
        self.mouthpiece_coverplate_config = mouthpiece_coverplate_config
        self.start_pos = cq.Vector(body_config.total_length - self.body_config.pipe_x__back_fillet_radius, 0, 0)
        self.mouthpiece_length1 = self._polymoe_config.curvedmouthpiece_length1
        self.mouthpiece_length2 = self._polymoe_config.curvedmouthpiece_length2
        self.mouthpiece_length3 = self._polymoe_config.curvedmouthpiece_length3
        self.mouthpiece_length4 = self._polymoe_config.curvedmouthpiece_length4
        self.mouthpiece_height = body_config.mouthpiece_height + body_config.mouthpiece_outer_border
        self.mouthpiece_width = body_config.mouthpiece_width
        self.mouthpiece_knob_size_factor = body_config.mouthpiece_knob_size_factor
        self.round_corners = self._polymoe_config.curvedmouthpiece_round_corners
        self.stacking_carving_length = body_config.stacking_carving_length


class CurvedMouthpiece:
    class _Shape(Enum):
        body = 0
        cover = 1

    def __init__(self, config: CurvedMouthpieceConfig):
        self.config = config
        self._built = None
        self.body = FluteBody(self.config.body_config)
        self.cover = Cover(self.config.cover_config)
        self.mouthpiece_coverplate = MouthpieceCoverplate(self.config.mouthpiece_coverplate_config)

    def _build_body_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = self.body.build_mouth_piece(workplane)
        return w


    def _build_cover_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = self.cover.build_mouthpiece_cover_sketch(workplane=workplane)
        return w

    def _build_stacking_adapter(self, position: float, shape: _Shape) -> cq.Workplane:
        if shape == CurvedMouthpiece._Shape.body:
            return self.mouthpiece_coverplate.build_stacking_carving_body(position)
        elif shape == CurvedMouthpiece._Shape.cover:
            return self.mouthpiece_coverplate.build_stacking_carving_cover(position)
        else:
            raise Exception(f"unecpacted shape {shape}")

    def _build_sketch(self, workplane: cq.Workplane, shape: _Shape) -> cq.Workplane:
        if shape == CurvedMouthpiece._Shape.body:
            return self._build_body_sketch(workplane)
        elif shape == CurvedMouthpiece._Shape.cover:
            return self._build_cover_sketch(workplane)
        else:
            raise Exception(f"unexpected shape {shape}")

    def _recenter(self, workplane: cq.Workplane, shape: _Shape):
        if shape == CurvedMouthpiece._Shape.body:
            w = workplane.center(self.config.body_config.mouthpiece_width / 2,
                         self.config.body_config.mouthpiece_height - self.config.body_config.pipe_yz_fillet_radius)
        elif shape == CurvedMouthpiece._Shape.cover:
            w = workplane.center(self.config.cover_config.mouthpiece_width / 2, 1)
        else:
            raise Exception(f"unexpected shape {shape}")
        return w

    def _build_mouthpiece_part(self, shape: _Shape) -> cq.Workplane:
        NINETY_DEGREE = 89.99 # 90 TODO: why does 90° not work...
        position_x = self.config.start_pos.x
        position_z = 0
        if self.config.stacking_carving_length != 0:
            stacking = self._build_stacking_adapter(position_x - self.config.stacking_carving_length, shape)
        w = cq.Workplane("YZ").workplane(offset=position_x, origin=(0,0))
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length1, origin=(0, 0))
        w = self._build_sketch(w, shape=shape)
        part = w.loft(ruled=True)
        if self.config.stacking_carving_length != 0:
            part = part.union(stacking, clean=True)
        position_x += self.config.mouthpiece_length1

        if self.config.round_corners:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x, origin=(0, 0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch1_centered = self._recenter(sketch1, shape=shape)
            part = part.union(sketch1_centered.revolve(NINETY_DEGREE,axisStart=cq.Vector(0,-self.config.mouthpiece_height,0) ,axisEnd=cq.Vector(1,-self.config.mouthpiece_height,0)), clean=True)
        else:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x, origin=(0,0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch2 = self._recenter(sketch1, shape=shape)
            sketch2 = sketch2.workplane(offset=self.config.mouthpiece_height, origin=(0, 0))\
                .transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=self.config.mouthpiece_height)
            sketch2 = self._build_sketch(sketch2, shape=shape)
            part = part.union(sketch2.loft(ruled=True), clean=True)
        position_x += self.config.mouthpiece_height
        position_z += self.config.mouthpiece_height

        w = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length2 - self.config.mouthpiece_height * 2)
        w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)
        part = part.union(w, clean=True)
        position_z += self.config.mouthpiece_length2 - self.config.mouthpiece_height * 2

        if self.config.round_corners:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch1_centered = self._recenter(sketch1, shape=shape)
            part = part.union(sketch1_centered.revolve(NINETY_DEGREE, axisStart=cq.Vector(0, -self.config.mouthpiece_height, 0), axisEnd=cq.Vector(1, -self.config.mouthpiece_height, 0)), clean=True)
        else:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch2 = self._recenter(sketch1, shape=shape)
            sketch2 = sketch2.workplane(offset=self.config.mouthpiece_height)\
                .transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=self.config.mouthpiece_height) # TODO: why does 90° not work?
            sketch2 = self._build_sketch(sketch2, shape=shape)
            part = part.union(sketch2.loft(ruled=True), clean=True)
        position_x -= self.config.mouthpiece_height
        position_z += self.config.mouthpiece_height

        w = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
        w = w.transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0))
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length3)
        w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)
        part = part.union(w, clean=True)
        position_x -= self.config.mouthpiece_length3

        if self.config.round_corners:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = sketch1.transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch1_centered = self._recenter(sketch1, shape=shape)
            part = part.union(sketch1_centered.revolve(NINETY_DEGREE, axisStart=cq.Vector(0, self.config.cover_config.height, 0),axisEnd=cq.Vector(-1, self.config.cover_config.height, 0)), clean=True)
        else:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = sketch1.transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch2 = self._recenter(sketch1, shape=shape)
            sketch2 = sketch2.workplane(offset=self.config.mouthpiece_height / 2) \
                .transformed(rotate=cq.Vector(-NINETY_DEGREE, 0, 0)).workplane(
                offset=self.config.mouthpiece_height / 2)
            sketch2 = self._build_sketch(sketch2, shape=shape)
            part = part.union(sketch2.loft(ruled=True), clean=True)
        position_x -= self.config.mouthpiece_height / 2
        position_z += self.config.mouthpiece_height / 2

        w = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length4)
        w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)
        part = part.union(w, clean=True)

        return part

    def _build_body_knobs_body_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.RightLower,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.z_position += self.config.body_config.mouthpiece_height
        knob_config_mouthpiece.direction = KnobDirection.Right
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)\
            .union(cord_knob_mouthpiece_right_down, clean=True).union(cord_knob_mouthpiece_left_down, clean=True)
        return knobs

    def _build_body_knobs_mouth_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x + self.config.mouthpiece_length1
                                                       - self.config.mouthpiece_length3
                                                       - self.config.mouthpiece_height / 2,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=self.config.mouthpiece_height / 2
                                                       + self.config.mouthpiece_length2
                                                       + self.config.mouthpiece_length4,
                                            direction=KnobDirection.LeftUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.x_position -= self.config.body_config.mouthpiece_height
        knob_config_mouthpiece.direction = KnobDirection.Upper
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)\
            .union(cord_knob_mouthpiece_right_down, clean=True).union(cord_knob_mouthpiece_left_down, clean=True)
        return knobs


    def _build_cover_knobs_body_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.RightUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)
        return knobs

    def _build_cover_knobs_mouth_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x + self.config.mouthpiece_length1
                                                       - self.config.mouthpiece_length3
                                                       - self.config.mouthpiece_height / 2,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=self.config.mouthpiece_height / 2
                                                       + self.config.mouthpiece_length2
                                                       + self.config.mouthpiece_length4,
                                            direction=KnobDirection.RightUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)
        return knobs

    def build(self) -> List[cq.Workplane]:
        body = self._build_mouthpiece_part(shape=CurvedMouthpiece._Shape.body)
        body_knobs_body_side = self._build_body_knobs_body_side()
        body_knobs_mouth_side = self._build_body_knobs_mouth_side()
        body = body.union(body_knobs_body_side, clean=True).union(body_knobs_mouth_side, clean=True)
        cover = self._build_mouthpiece_part(shape=CurvedMouthpiece._Shape.cover)
        cover_knobs_body_side = self._build_cover_knobs_body_side()
        cover_knobs_mouth_side = self._build_cover_knobs_mouth_side()
        cover = cover.union(cover_knobs_body_side, clean=True).union(cover_knobs_mouth_side, clean=True)
        self._built = [body, cover]
        return self._built

    def carve_stacking(self, mouthpiece_cover_obj: cq.Workplane):
        if self._built is None:
            self.build()
        for part_index in range(len(self._built)):
            self._built[part_index] = self._built[part_index].cut(mouthpiece_cover_obj, clean=True)
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig) -> cq.Assembly:
    assembly = cq.Assembly()

    body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(body_config)
    coverplate_config = MouthpieceCoverplateConfig(body_config, cover_config)
    curved_mouthpiece_config = CurvedMouthpieceConfig(body_config=body_config, cover_config=cover_config,
                                                      mouthpiece_coverplate_config=coverplate_config)
    curved_mouthpiece = CurvedMouthpiece(curved_mouthpiece_config)
    curved_mouthpiece_halfpipe_objs = curved_mouthpiece.build()
    assembly.add(curved_mouthpiece_halfpipe_objs[0], name="curved mouthpiece body")
    assembly.add(curved_mouthpiece_halfpipe_objs[1], name="curved mouthpiece cover")

    #print(f"volume curved mouthpiece: {curved_mouthpiece.volume():.1f} cm^3")
    #print(f"weight curved mouthpiece: {curved_mouthpiece.weight():.1f} g")

    return assembly


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        result.save("polymoe.step")
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END