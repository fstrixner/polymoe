# SINGLEPY_IGNORE_BEGIN
from typing import Dict, Any
# SINGLEPY_IGNORE_END


class PolyMoeConfig:

    def __init__(self, config_abbreviation: str = None):
        if config_abbreviation is not None:
            self.configuration = config_abbreviation
        else:
            '''
                Currently active configuration. In the optimal case this could  be the only parameter that might need to be changed.
                
                configurations:
                S: small: only the front flute that is held in place with the thumbs in the carry hooks
                M: medium: with first extension. To be played either by supporting the instrument via a chair or by using a strap around the neck that connects to the upper carry hook
                L: large: with two extensions. Long enough to place one end on the floor. Played with the rounded mouthpiece
                CH: Configuration that still works with CadHub (within the 20s timeout)
                '''
            self.configuration = "S"

        #####################
        # common parameters #
        #####################
        # These parameters are used by the main assembly and control some behavior common to all or at least several parts

        # export result as multi-part STEP file (for CNC, visualization etc.) [True; False]
        self.save_step = self.conf({"CH": False, "default": True})
        # export STL files individually for each part
        self.save_stl_parts = self.conf({"CH": False, "default": True})

        # number of pipes (for polyphonic playing one needs at least two pipes) [1-6]
        self.num_pipes = self.conf({"CH": 1, "default": 3})
        # a curved sax-like tubing can help to play comfortably with longer instrument sizes, so the arms can span a bigger range
        self.use_curved_mouthpiece = self.conf({"L": True, "default": False})

        # layouts define whether notes are continuous (like a trombone) or whether the pitch strips have discrete holes like a flute.
        # Different layouts can be mixed, e.g. to allow easier chord play with discrete pitch strips and continuous strips
        # for expressive melody play with vibrato, continuous portamento. playing with interferences etc.
        # available layouts:
        # * continuous (open slit)
        # * chromatic (12 discrete, equal tempered semitones)
        # * diatonic (major/minor scale)
        # * pentatonic (5 note scale)
        # * octaves (only the base note per octave)
        # * bourdon ( no holes or slit, only the base note of the open pipe, like an overtone flute)

        # Here a number of sane proposals - but new combos can be built at will
        # If num_pipes is smaller than the specified strips then the last entries are skipped, if it has more then
        # continuous is assumed as default
        interference_layout_proposal = { # having two continuous scales allows to play with interferences
            0: "continuous",
            1: "continuous",
            2: "chromatic",
            3: "chromatic",
            4: "chromatic",
        }
        melody_and_chords_layout_proposal = { # chromatic for chords, one continuous melody scale
            0: "continuous",
            1: "chromatic",
            2: "chromatic",
            3: "chromatic",
            4: "chromatic"
        }
        all_continuous = { # profi configuration: allows chords built from pure intervalls, expressive melodies, interferences etc. - but hard to intonate
            0: "continuous",
            1: "continuous",
            2: "continous",
            3: "continous",
            4: "continuous"
        }
        all_diatonic = { # "organ style", for easier intonation, but losing some expressiveness and pure intervals
            0: "chromatic",
            1: "chromatic",
            2: "chromatic",
            3: "chromatic",
            4: "chromatic"
        }
        all_pentatonic = { # "native american flute" like - for easy improvisation
            0: "pentatonic",
            1: "pentatonic",
            2: "pentatonic",
            3: "pentatonic",
            4: "pentatonic"
        }
        try_all_layouts = { # these are mainly for demonstration, doesn't make too much sense musically...
            0: "continuous",
            1: "chromatic",
            2: "diatonic",
            3: "octaves",
            4: "penatonic",
            5: "bourdon"
        }

        # actually used layout
        self.pitchstrip_layout = try_all_layouts

        # the frequency of the lowest hole on a pitch strip (only applies to pitch strips with discrete holes)
        frequency_for_lowest_hole_proposal = self.conf({
            "S": 1046.50, # C6
            "M": 261.63, # C4 #220.00, # A3
            "L": 130.81, # C3
            "CH": 261.63
        })
        frequency_for_lowest_hole_proposal2 = self.conf({"S": 1046.50, "M": 223.5, "L": 130.81, "CH": 223.5}) # example: let scale go down to A3 for config "M" for this pipe - TODO: why do I have to use 223.5 instead of 220 to keep the scale in sync with the scale starting from C4 at 261.63 Hz? The frequencies should already be the well-tempered ones?
        # lowest frequency per pitch strip
        self.frequency_of_lowest_hole = {
            0: frequency_for_lowest_hole_proposal,
            1: frequency_for_lowest_hole_proposal2,
            2: frequency_for_lowest_hole_proposal,
            3: frequency_for_lowest_hole_proposal,
            4: frequency_for_lowest_hole_proposal
        }

        ##############################
        # body and common parameters #
        ##############################
        # these parameters are mainly used for the main instrument body and it's optional extensions
        # But some are also reused by other parts like the cover etc.
        # (in cases where dimensions have to be consistent between several parts)


        # the width and height of a single pipe at the bottom and top (if wider/higher on the bottom than on the top then the
        # pipe is conical which can be helpful so the pipe diameter is smaller for high and bigger for low notes,
        # making it easier to hit them without overblowing)
        # An increasing width/height can also give some haptic feedback at which instrument position the fingers are.
        # But the downside particularly with increasing width is that the stretch between fingers on adjacent strips gets bigger.
        # So one might want experiment with keeping bottom and top width the same and partially (or fully) using the
        # height instead of the width the make the diameter of lower notes bigger.
        # TODO: How do non quadratic pipe shapes might affect the timbre(?)
        #       How does the conicity of the pipe affect the timbre(?)
        self.pipe_width_at_bottom = self.conf({"S": 27.5, "M": 42.402, "L": 57.060, "CH": 42.402}) # 20
        self.pipe_width_at_top = 20
        #self.pipe_height_at_bottom = self.conf({"S": 27.5, "M": 42.402, "L": 57.060, "CH": 42.402})
        self.pipe_height_at_bottom = 20
        self.pipe_height_at_top = 20

        # Width (and height) of the pipe border.
        # Overall instrument width is: num_pipes * width_pipe + (num_pipes + 1) * pipe_border
        # Overall instrument height is: height_pipe + 2 * pipe_border
        self.pipe_border = 7.6

        # length of the open pipe from bottom opening to labium
        self.soundpipe_length = self.conf({"S": 240, "M": 850, "L": 1450, "CH": 850})

        # the sound pipe can be divided into the main instrument and extensions that can be bound to the
        # main instrument to extend it's length.
        # This allows to change the instrument length for different playing approaches:
        # * instrument held in hands with only fingers moving (pipe length ~ 2*finger span)
        # * instrument held by a strip around the neck (pipe length ~ arm length)
        # * instrument standing on the floor and played via curved mouthpiece (as long as a standing person)
        # This also simplifies manufacturing.
        # If modularity isn't a priority and manufacturing allows it, then a large instrument can of course also be made
        # without extensions in one piece
        #self.extension_cuts = self.conf({"S": [], "M": [610], "L": [600, 610], "CH": []})
        self.extension_cuts = self.conf({"S": [], "M": [300,310], "L": [300, 300, 300, 310], "CH": []})

        # length of the open hole between labium and wind channel
        self.air_hole_length = 7

        # height and length of the wind channel (the labium is always vertically centered in front of the wind channel)
        self.wind_channel_height = 2
        self.wind_channel_length = 20
        # length of the ramp that leads to the wind channel (this also affects the slope of the ramp)
        self.wind_channel_ramp_length = 25

        # length of the chamber behind the wind channel ramp that is used to linearize the air stream
        # (similar to native american flutes)
        self.slow_airchamber_total_length = 40

        # width, height and length of the pipes after the slow air chamber that lead to the mouthpiece
        # these have usually a smaller diameter than the slow airchamber before it
        self.mouthpiece_pipe_height = 9 #6
        self.mouthpiece_pipe_width = 9 #6
        self.mouthpiece_length = 60

        # the outer border of the mouthpiece pipes can be thinner than the walls of the soundpipe
        # - particularly the inner borders. This allows a more compact mouth piece that fits into the mouth more comfortably
        self.mouthpiece_outer_border = 6.5
        self.mouthpiece_inner_border = 4
        self.mouthpiece_bottom_border = 3

        # factor how much smaller the chord knob that is used to bind the mouthpiece coverplate to the mouthiece pipes are
        # compared to the normal knobs are (see knob for the normal knob dimensions)
        self.mouthpiece_knob_size_factor = 0.5 # 0.15

        # defines whether some corners of the instrument will be rounded (True) or edgy (False)
        self.fillets = self.conf({"CH": False, "default": True})

        # radius of fillets along the width and height of the instrument
        self.pipe_x_fillet_radius = 10
        # a smaller radius for certain fillets along the width and height of the instrument for cases where the one above is too big
        self.pipe_x__back_fillet_radius = 2 # TODO: still needed?
        # a radius for certain fillets along the length of the instrument (e.g. the rounding of the lower body edges)
        self.pipe_yz_fillet_radius = 2 #5
        # radius for certain fillets of the mouthpiece
        self.mouthpiece_fillet_radius = 2
        # radius for certain fillets of smaller details of the mouthpiece
        self.mouthpiece_front_fillet_radius = 0.9

        # length of the carved stacking adapters that help to keep the extensions connected to each other and to the main body
        self.body_stacking_carving_length = self.conf({"CH": 0, "default": 5})

        # depth of the carving at the bottom that holds the carrying hook in place
        self.body_hook_stacking_depth = self.conf({"CH": 0, "default": 2})

        # density of the material used to create the main instrument body and the extension bodies
        self.body_density = 0.34  # g/cm^3 cedar wood

        #############################
        # cover specific parameters #
        #############################
        # these parameters only affect the cover and cover extensions that are bound on top of the body and it's extensions
        # It closes the sound pipe, slow air pipe and mouthpiece from above and contains functional elements like the labium

        # radius used for certain cover fillets (like the length-wise upper cover fillets)
        self.cover_upper_fillet_radius = 5 #0.2
        # length of the labium
        self.cover_labium_length = 15
        # the labium is narrower than the pipe width by that offset on the left and right side
        self.cover_labium_offset = 2
        # in the case of continuous tracks this is the width of the cut, in all other cases the hole diameter
        self.cover_cut_diameter = 5
        # width and depth of the inlay for the pitch strip
        self.cover_inlay_diameter = 15
        self.cover_inlay_depth = 1
        # length of area between the end of the last hole of the manual and the labium
        self.cover_closed_fingerboard_length = 50
        # length of area between the end of the inlay and the labium
        self.cover_above_inlay_length = 10

        # the body subtracted from the cover to carve some pockets into the cover that help to keep the cover attached to the body
        # this defines the height of this carving
        self.cover_stacking_carving_height = self.conf({"default": 1, "CH": 0})
        # currently the rounded parts of the cover at the transition between body and moundpiece is slightly bigger than the body
        # and thus creates artifacts when cutting out the stacking area. One possible workaround is to make the cover width
        # slightly smaller than the body, so it fits at the rounded area (TODO: get rid of width deviations so no more correction is needed)
        self.cover_stacking_width_correction = 0#1.3
        # density of the cover material
        self.cover_density = 0.34 # g/cm^3 cedar wood

        #############################
        # wings specific parameters #
        #############################
        # The "wings" are a structure that is bound on top of the lower side of the cover.
        # It is used to keep the magnetic, flexible strips away from the metal pitch strips below the finger position
        # If the pipe width at the bottom is unequal to the pipe width at top, then different wings have to be
        # provided depending on which extensions are bound to the main body.
        # If the width is constant then one wings part can be used with or without extensions, assuming the wing height already
        # copes for the maximum extension length (thus even for constant pipe width several wing parts could make sense
        # so a less high and thus more light weight wing part can be used for shorter instruments without extensions)

        # The heigher the wing the further it can keep the flexible strip away from the metal pitch strip. Thus depends on
        # the overall instrument length
        self.wing_height = self.conf({"S": 37.5, "M": 132.81225, "L": 226.56225, "default": 132.81225})
        self.wing_stopper_length = 10
        self.wing_ramp_length = self.conf({"S": 5.647, "M": 20, "L": 34.118, "default": 20})
        self.wing_ramp_top_length = 10
        self.wing_blocker_height = 15
        self.wing_blocker_gap_length = 5
        self.wing_corner_radius = 2
        self.wing_mount_depth = -self.pipe_border / 2
        self.wing_mount_length = self.conf({"default": self.body_stacking_carving_length, "CH": 5})
        self.wing_start_position_x = 0
        self.wing_density = 0.34  # g/cm^3 cedar wood

        ###################################
        # stringplate specific parameters #
        ###################################
        # The stringplate is the counterpart of the "wings". It presses the top of the magnetic, flexible strip to the
        # instrument cover. It should be far enough away from the beginning of the holes/continuous pitch carving
        # that the flexible strip gets pulled away from the metal strip as soon as the finger doesn't press down the
        # flexible strip anymore

        self.stringplate_distance_from_manual = 10
        self.stringplate_height = 12
        self.stringplate_stopper_length = 5
        self.stringplate_ramp_length = 10
        self.stringplate_ramp_top_length = 5
        self.stringplate_blocker_height = 12
        self.stringplate_density = 0.34  # g/cm^3 cedar wood

        ##################################
        # pitchstrip specific parameters #
        ##################################
        # The pitch strips are the metal plates that get bound to the cover. These either contain
        # holes for the discrete case and/or a continuous pitch carving
        # If body and cover extensions are bound to the main instrument then longer pitch strips have to be used
        # (So for intended modular length of the instrument a fitting set of pitch strips has to be provided)

        # if the holes of a discrete strip get closer than this value then a continuous pitch carving is created instead
        # of further holes. That way high pitches notes are still playable even though the holes would start to intersect
        self.pitchstrip_continuous_if_hole_distance_smaller_than = 7
        # radius of holes (only relevant for discrete strips)
        self.pitchstrip_hole_radius = 2.5
        # density of the material for the metal pitch strips (the material should be both magnetic and rustproof)
        self.pitchstrip_density = 7.85  # g/cm^3 carbon steel

        #############################################
        # mouthpiece coverplate specific parameters #
        #############################################
        # The cover plate is either bound to the main instrument body or to the rounded mouthpiece extension
        # It is the part that the player takes into the mouth, though it should be smooth and in one piece

        self.mouthpiece_coverplate_length = 5
        self.mouthpiece_coverplate_density = 0.34  # g/cm^3 cedar wood

        #########################################
        # curved mouthpiece specific parameters #
        #########################################
        # the curves (sax-like) extension of the mouthpiece that can optionally be used for longer instruments

        self.curvedmouthpiece_density = 0.34  # g/cm^3 cedar wood
        # length of different sections of the curved mouthpiece
        '''
        2222
        1  3
           3
           3444
        '''
        self.curvedmouthpiece_length1 = 5 # from start to highest point
        self.curvedmouthpiece_length2 = 70 # from highest point sideways
        self.curvedmouthpiece_length3 = 200 # downwards
        self.curvedmouthpiece_length4 = 50 # towards the player
        # use rounded corners (True) or edgy corners (False)
        self.curvedmouthpiece_round_corners = self.conf({"CH": False, "default": True})

        ##################################
        # carry hook specific parameters #
        ##################################
        # This hook can be used by the thumbs to carry the instrument in the case it is short enough.
        # Or the hook can be attached to a neck strip for longer instrument (that are not long enough that they reach the floor)

        self.hook_width = None # None: use entire body width
        self.hook_gap_depth = 5
        self.hook_blocker_length = 20
        self.hook_on_pipe_length = 100 # length projected to the x axis
        self.hook_density = 0.34 # g/cm^3 cedar wood
        self.hook_radius = 20
        self.hook_upper_part_length = 50 # projected to the x axis
        self.hook_thickness = 10
        self.hook_fillet_radius = 1.5
        # angle between x|z axis and left|right wall of the hook (mirrored)
        self.hook_rotation_angle_y = None  # None: calculate from body slope
        self.hook_rotation_angle_z = None  # None: calculate from body slope

        ############################
        # knob specific parameters #
        ############################
        # The knobs are used to bind several parts of the instrument together with cords.
        # Compared to gluing everything together this has two advantages:
        # * The instrument stays modular, so different slized variants can be assembled from the same base instrument
        # * The instrument can be disassembled for maintenance (e.g. oiling, cleaning etc.)

        self.knob_density = 0.34  # g/cm^3 cedar wood
        self.knob_length = 8
        self.knob_fillet_radius = 5
        self.knob_size_factor = 1.0
        self.knob_width_min = self.pipe_border - 2
        self.knob_width_max = self.pipe_border

    # small helper function that allows to define values per configuration - or a default value
    def conf(self, x: Dict[str, Any]):
        if self.configuration in x.keys():
            return x[self.configuration]
        elif 'default' in x.keys():
            return x['default']
        else:
            raise Exception(f"value not defined for configuration {self.configuration}")