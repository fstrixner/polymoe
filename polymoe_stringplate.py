# SINGLEPY_IGNORE_BEGIN
from polymoe_body import FluteBodyConfig

import cadquery as cq

from polymoe_cover import CoverConfig
from polymoe_wings import WingsConfig, Wings
from polymoe_config import *
# SINGLEPY_IGNORE_END

class StringPlateConfig(WingsConfig):

    def __init__(self, flute_body_config: FluteBodyConfig, cover_config: CoverConfig):
        super().__init__(flute_body_config)

        config = flute_body_config.config

        self.distance_from_manual = config.stringplate_distance_from_manual

        self.start_position_x = cover_config.manual_length() + self.distance_from_manual
        self.wing_height = config.stringplate_height
        self.wing_stopper_length = config.stringplate_stopper_length
        self.wing_ramp_length = config.stringplate_ramp_length
        self.wing_ramp_top_length = config.stringplate_ramp_top_length
        self.blocker_height = config.stringplate_blocker_height
        #self.blocker_gap_length = 5
        #self.corner_radius = 2
        self.density = config.stringplate_density
        self.add_mount = False

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig):
    body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(body_config)

    wings_config = StringPlateConfig(body_config, cover_config)
    wings = Wings(wings_config)
    wings_obj = wings.build()

    return wings_obj


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END