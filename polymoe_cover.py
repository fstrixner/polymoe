# SINGLEPY_IGNORE_BEGIN
from typing import List

import cadquery as cq
try:
    from ocp_vscode import show_object
except:
    pass

from polymoe_body import FluteBodyConfig, FluteBody
from polymoe_knob import KnobConfig, KnobDirection, KnobSide, Knob
from polymoe_util import continuous_connection
from polymoe_config import *
# SINGLEPY_IGNORE_END


class CoverConfig:


    def __init__(self, flute_body_config: FluteBodyConfig):
        self._polymoe_config = flute_body_config.config

        self.upper_fillet_radius = self._polymoe_config.cover_upper_fillet_radius
        self.labium_length = self._polymoe_config.cover_labium_length
        self.labium_offset = self._polymoe_config.cover_labium_offset
        self.cut_diameter = self._polymoe_config.cover_cut_diameter
        self.inlay_diameter = self._polymoe_config.cover_inlay_diameter
        self.inlay_depth = self._polymoe_config.cover_inlay_depth
        self.closed_fingerboard_length = self._polymoe_config.cover_closed_fingerboard_length
        self.above_inlay_length = self._polymoe_config.cover_above_inlay_length
        self.stacking_carving_height = self._polymoe_config.cover_stacking_carving_height
        self.stacking_width_correction = self._polymoe_config.cover_stacking_width_correction
        self.density = self._polymoe_config.cover_density

        self._flute_body_config = flute_body_config
        # the cover for the sound chamber is windchannel_offset mm thicker than the rest of the cover in order to place
        # the labium at the center of the windchannel
        self.windchannel_offset = self._flute_body_config.wind_channel_height / 2
        self.height = flute_body_config.pipe_border
        self.width_bottom = flute_body_config.body_width_at_position(0) - self.stacking_width_correction * self.stacking_carving_height
        self.length = flute_body_config.soundpipe_length
        self.width_top = flute_body_config.body_width_at_position(self.length) - self.stacking_width_correction * self.stacking_carving_height
        self.num_pipes = flute_body_config.num_pipes
        self.labium_width = flute_body_config.pipe_width_at_position(self.length) - self.labium_offset
        self.air_hole_length = flute_body_config.air_hole_length
        self.pipe_border = flute_body_config.pipe_border
        self.mouthpiece_length = flute_body_config.mouthpiece_length
        self.mouthpiece_width = flute_body_config.mouthpiece_width - self.stacking_width_correction * self.stacking_carving_height
        self.mouthpiece_front_fillet_radius = flute_body_config.mouthpiece_front_fillet_radius
        self.mouthpiece_knob_size_factor = flute_body_config.mouthpiece_knob_size_factor
        self.slow_airchamber_cover_length = flute_body_config.slow_airchamber_total_length + flute_body_config.wind_channel_length
        self.x_fillets = flute_body_config.x_fillets
        self.yz_fillets = flute_body_config.yz_fillets
        self.extension_cuts = flute_body_config.extension_cuts
        self.stacking_adapter_length = flute_body_config.stacking_carving_length

    def width_at_position(self, position: float) -> float:
        return self.width_bottom \
               - (self.width_bottom - self.width_top) * position / self.length

    def labium_border_width_at_position(self, position: float) -> float:
        return (self.width_at_position(position) - self.num_pipes * self.labium_width) / (self.num_pipes + 1)

    def fingerboard_border_at_position(self, position: float) -> float:
        return (self.width_at_position(position) - self.pipe_border) / (self.num_pipes * 2) - self.cut_diameter / 2
        #return (self.width_at_position(position) - self.cut_diameter * self.num_pipes) / (self.num_pipes * 2)

    def inlay_border_at_position(self, position: float) -> float:
        return (self.width_at_position(position) - self.pipe_border) / (self.num_pipes * 2) - self.inlay_diameter / 2
        #return self.width_at_position(position) / (self.num_pipes * 2) - self.inlay_diameter / 2

    def manual_length(self) -> float:
        return self.length - self.closed_fingerboard_length - self.labium_length

    @property
    def inlay_length(self) -> float:
        return self.length - self.above_inlay_length - self.labium_length



class Cover:
    def __init__(self, config: CoverConfig):
        self.config = config
        self._built = None

    def _build_stacking_adapter_sketch(self, workplane: cq.Workplane, position: float, part_index: int) -> cq.Workplane:
        width = self.config.width_at_position(position) - self.config.pipe_border
        stacking_width = self.config.pipe_border / 2
        x1_step = width / (self.config.num_pipes * 2) - self.config.cut_diameter / 2
        x1_offset = -width / 2
        y1_offset = -self.config.windchannel_offset - self.config.stacking_carving_height
        x2_step = 2 * x1_step
        x2_offset = x1_step + part_index * self.config.cut_diameter + (part_index - 1) * x2_step
        y2_offset = stacking_width

        w = workplane.center(x1_offset, y1_offset)
        if part_index == 0:
            w = w.line(0, stacking_width)
            w = w.line(x1_step, 0)
            w = w.line(0, - stacking_width)
            w = w.close()
        elif part_index < self.config.num_pipes:
            w = w.center(x2_offset, y2_offset)
            w = w.line(x2_step, 0)
            w = w.line(0, -stacking_width)
            w = w.line(-x2_step, 0)
            w = w.close()
            w = w.center(-x2_offset, -y2_offset)
        else:
            w = w.center(x2_offset, y2_offset)
            w = w.line(x1_step, 0)
            w = w.line(0, -stacking_width)
            w = w.line(-x1_step, 0)
            w = w.close()
            w = w.center(-x2_offset, -y2_offset)

        w = w.center(-x1_offset, -y1_offset)

        return w

    def build_stacking_adapter(self, position: float) -> List[cq.Workplane]:
        parts = []
        for part_index in range(self.config.num_pipes + 1):
            lower_sketch = self._build_stacking_adapter_sketch(workplane=cq.Workplane("YZ").workplane(offset=position),
                                                               position=position,
                                                               part_index=part_index)
            upper_sketch = self._build_stacking_adapter_sketch(workplane=lower_sketch.workplane(
                offset=self.config.stacking_adapter_length),
                position=position + self.config.stacking_adapter_length,
                part_index=part_index)
            parts.append(upper_sketch.loft(ruled=False))
        return parts

    def _build_plate_sketch(self, workplane: cq.Workplane, position: float, height_offset = 0) -> cq.Workplane:
        w = workplane.center(-self.config.width_at_position(position) / 2, -height_offset - self.config.stacking_carving_height)
        w = w.line(0, self.config.height - self.config.upper_fillet_radius + height_offset)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
        w = w.line(self.config.width_at_position(position) - 2 * self.config.upper_fillet_radius, 0)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
        w = w.line(0, -self.config.height + self.config.upper_fillet_radius - height_offset)
        w = w.close()
        return w

    def _build_labium_sketch(self, workplane: cq.Workplane, position: float, labium_height) -> cq.Workplane:
        w = workplane.center(-self.config.width_at_position(position) / 2, -self.config.windchannel_offset
                             - self.config.stacking_carving_height)
        w = w.line(0, self.config.height - self.config.upper_fillet_radius + self.config.windchannel_offset)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
        w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
        for pipe_num in range(0, self.config.num_pipes):
            w = w.line(0, -labium_height)
            w = w.line(self.config.labium_width, 0)
            w = w.line(0, labium_height)
            if pipe_num < self.config.num_pipes - 1:
                w = w.line(self.config.labium_border_width_at_position(position), 0)
        w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
        w = w.line(0, -self.config.height + self.config.upper_fillet_radius - self.config.windchannel_offset)
        w = w.close()
        return w

    def build_pitch_carving(self, pipe_num: int, length: float, start_pos: float = 0) -> cq.Workplane:
        w = cq.Workplane("XY").workplane(offset=self.config.height + self.config.windchannel_offset
                                                - self.config.stacking_carving_height, origin=(0, 0))
        y_pos_0 = (self.config.width_at_position(start_pos) / 2 - self.config.pipe_border / 2
                   - self.config.fingerboard_border_at_position(start_pos)
                   - pipe_num * self.config.cut_diameter
                   - self.config.fingerboard_border_at_position(start_pos) * pipe_num * 2
                   )
        w = w.center(start_pos,
                     -y_pos_0
        )
        y_manual_end = (y_pos_0
                        - self.config.width_at_position(length) / 2 + self.config.pipe_border / 2
                        + self.config.fingerboard_border_at_position(length)
                        + pipe_num * self.config.cut_diameter
                        + self.config.fingerboard_border_at_position(length) * pipe_num * 2
                        )
        length = length - start_pos
        w = w.line(length, y_manual_end)

        w = w.line(0, self.config.cut_diameter)
        w = w.line(-length, -y_manual_end)
        w = w.close()
        w = w.extrude(-self.config.height - self.config.upper_fillet_radius - self.config.windchannel_offset)
        return w

    def build_cover_inlay_carving(self, pipe_num: int, length: float) -> cq.Workplane:
        w = cq.Workplane("XY").workplane(offset=self.config.height + self.config.windchannel_offset
                                                - self.config.inlay_depth
                                                - self.config.stacking_carving_height, origin=(0, 0))
        y_pos_0 = (self.config.width_at_position(0) / 2 - self.config.pipe_border / 2
                   - self.config.inlay_border_at_position(0)
                   - pipe_num * self.config.inlay_diameter
                   - self.config.inlay_border_at_position(0) * pipe_num * 2
                   )
        w = w.center(0,
                     -y_pos_0
                     )
        y_manual_end = (y_pos_0
                        - self.config.width_at_position(length) / 2 + self.config.pipe_border / 2
                        + self.config.inlay_border_at_position(length)
                        + pipe_num * self.config.inlay_diameter
                        + self.config.inlay_border_at_position(length) * pipe_num * 2
                        )
        w = w.line(length, y_manual_end)

        w = w.line(0, self.config.inlay_diameter)
        w = w.line(-length, -y_manual_end)
        w = w.close()
        w = w.extrude(-self.config.inlay_depth)
        return w

    def _build_air_hole_wall_sketch(self, workplane: cq.Workplane, position: float, wall_index: int):
        w = workplane.center(-self.config.width_at_position(position) / 2
                             + wall_index * (self.config.labium_border_width_at_position(position) + self.config.labium_width),
                             -self.config.stacking_carving_height)
        if wall_index == 0: # leftmost, rounded wall
            w = w.line(0, self.config.height - self.config.upper_fillet_radius)
            w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
            w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
            w = w.line(0, -self.config.height)
            w = w.close()
        elif wall_index == self.config.num_pipes: # rightmost, rounded wall
            w = w.line(0, self.config.height)
            w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
            w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
            w = w.line(0, -self.config.height + self.config.upper_fillet_radius)
            w = w.close()
        else: # straight middle walls
            w = w.line(0, self.config.height)
            w = w.line(self.config.labium_border_width_at_position(position), 0)
            w = w.line(0, -self.config.height)
            w = w.close()
        return w

    #def _build_slow_airchamber_cover_sketch(self, workplane: cq.Workplane, position: float) -> cq.Workplane:
    #    pass

    def build_mouthpiece_cover_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = workplane.center(-self.config.mouthpiece_width / 2, -self.config.stacking_carving_height)
        w = w.line(0, self.config.height - self.config.upper_fillet_radius)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
        w = w.line(self.config.mouthpiece_width - 2 * self.config.upper_fillet_radius, 0)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
        w = w.line(0, -self.config.height + self.config.upper_fillet_radius)
        w = w.close()
        return w

    def _build_soundpipe_cover(self, position: float, length: float) -> cq.Workplane:
        plate_lower = self._build_plate_sketch(cq.Workplane('YZ').workplane(offset=position), position,
                                               height_offset=self.config.windchannel_offset)
        position += length
        plate_upper = self._build_plate_sketch(plate_lower.workplane(offset=length,
                                                                     origin=(0, 0)),
                                               position, height_offset=self.config.windchannel_offset)
        plate = plate_upper.loft()
        return plate

    def build(self) -> List[cq.Workplane]:
        if self._built is not None:
            return self._built

        position = 0
        pitch_carving = []
        inlay_carving = []
        for pipe_num in range(self.config.num_pipes):
            pitch_carving.append(self.build_pitch_carving(pipe_num, start_pos=0, length=self.config.inlay_length))
            inlay_carving.append(self.build_cover_inlay_carving(pipe_num, length=self.config.inlay_length))
        sound_pipe_extensions = []
        remaining_soundpipe_cover = self.config.length
        for extension_length in self.config.extension_cuts:
            if self.config.stacking_adapter_length != 0:
                stacking_adapters = self.build_stacking_adapter(position=position - self.config.stacking_adapter_length)
            knob_config_upper = KnobConfig(config=self.config._polymoe_config,
                                           x_position=position,
                                           y_position=self.config.width_at_position(position),
                                           z_position=self.config.windchannel_offset,
                                           direction=KnobDirection.RightUpper,
                                           side=KnobSide.Left)
            cord_knob_upper_left = Knob(knob_config_upper).build()
            knob_config_upper.side = KnobSide.Right
            cord_knob_upper_right = Knob(knob_config_upper).build()
            extension = cq.Workplane().union(self._build_soundpipe_cover(position, length=extension_length), clean=True)
            position += extension_length
            knob_config_lower = KnobConfig(config=self.config._polymoe_config,
                                           x_position=position,
                                           y_position=self.config.width_at_position(position),
                                           z_position=self.config.windchannel_offset,
                                           direction=KnobDirection.LeftUpper,
                                           side=KnobSide.Left)
            cord_knob_lower_left = Knob(knob_config_lower).build()
            knob_config_lower.side = KnobSide.Right
            cord_knob_lower_right = Knob(knob_config_lower).build()
            extension = extension.union(cord_knob_upper_right, clean=True)
            extension = extension.union(cord_knob_upper_left, clean=True)
            extension = extension.union(cord_knob_lower_right, clean=True)
            extension = extension.union(cord_knob_lower_left, clean=True)
            if self.config.stacking_adapter_length != 0:
                for stacking_adapter in stacking_adapters:
                    extension = extension.union(stacking_adapter, clean=True)
            #for pipe_num in range(self.config.num_pipes):
            #    extension = extension.cut(pitch_carving[pipe_num])
            #    extension = extension.cut(inlay_carving[pipe_num])
            sound_pipe_extensions.append(extension)
            remaining_soundpipe_cover -= extension_length
        remaining_soundpipe_cover -=  self.config.x_fillets * 2 + self.config.labium_length
        if self.config.stacking_adapter_length != 0:
            stacking_adapters = self.build_stacking_adapter(position=position - self.config.stacking_adapter_length)
        knob_config_upper = KnobConfig(config=self.config._polymoe_config,
                                       x_position=position,
                                       y_position=self.config.width_at_position(position),
                                       z_position=self.config.windchannel_offset,
                                       direction=KnobDirection.RightUpper,
                                       side=KnobSide.Left)
        cord_knob_left = Knob(knob_config_upper).build()
        knob_config_upper.side = KnobSide.Right
        cord_knob_right = Knob(knob_config_upper).build()
        plate = self._build_soundpipe_cover(position, length=remaining_soundpipe_cover)
        if self.config.stacking_adapter_length != 0:
            for stacking_adapter in stacking_adapters:
                plate = plate.union(stacking_adapter, clean=True)
        position += remaining_soundpipe_cover

        labium_lower = self._build_labium_sketch(cq.Workplane('YZ').workplane(offset=position, origin=(0,0)),
                                                 position, labium_height=0.01)
        position += self.config.labium_length
        labium_upper = self._build_labium_sketch(labium_lower.workplane(offset=self.config.labium_length, origin=(0,0)),
                                                 position,
                                                 labium_height=self.config.height + self.config.windchannel_offset)
        labium = labium_upper.loft()
        for extension_index in range(len(sound_pipe_extensions)):
            for pipe_num in range(self.config.num_pipes):
                sound_pipe_extensions[extension_index] = sound_pipe_extensions[extension_index].cut(pitch_carving[pipe_num])
                sound_pipe_extensions[extension_index] = sound_pipe_extensions[extension_index].cut(inlay_carving[pipe_num])
        for pipe_num in range(self.config.num_pipes):
            plate = plate.cut(pitch_carving[pipe_num])
            plate = plate.cut(inlay_carving[pipe_num])

        air_hole_walls = []
        for wall_index in range(0, self.config.num_pipes + 1):
            air_hole_wall_lower = self._build_air_hole_wall_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0,0)), position=position, wall_index=wall_index)
            air_hole_wall_upper = self._build_air_hole_wall_sketch(air_hole_wall_lower.workplane(
                offset=self.config.air_hole_length, origin=(0,0)), position=position+self.config.air_hole_length, wall_index=wall_index)
            air_hole_walls.append(air_hole_wall_upper.loft())
        position += self.config.air_hole_length

        slow_airchamber_cover_lower = self._build_plate_sketch(cq.Workplane('YZ').workplane(offset=position, origin=(0,0)), position = position)
        position += self.config.slow_airchamber_cover_length
        slow_airchamber_cover_upper = self._build_plate_sketch(
                workplane= slow_airchamber_cover_lower.workplane(
                offset=self.config.slow_airchamber_cover_length, origin=(0, 0)), position=position)
        slow_airchamber_cover = slow_airchamber_cover_upper.loft()

        if self.config.x_fillets:
            pipe_to_mouthpiece__connection_1 = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0,0)), position = position)
            position += 0.1
            pipe_to_mouthpiece__connection_2 = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 - 0.1
            pipe_to_mouthpiece__connection_3 = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 * 6 - 0.1
            pipe_to_mouthpiece__connection_4 = self.build_mouthpiece_cover_sketch(
                workplane= cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            position += 0.1
            pipe_to_mouthpiece__connection_5 = self.build_mouthpiece_cover_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            mouthpiece1 = continuous_connection(
                pipe_to_mouthpiece__connection_1.wires().val(),
                pipe_to_mouthpiece__connection_5.wires().val(),
                invert_normals=True,
                middle_wires=[pipe_to_mouthpiece__connection_2.wires().val(),
                              pipe_to_mouthpiece__connection_3.wires().val(),
                              pipe_to_mouthpiece__connection_4.wires().val()]
            )
        else:
            pipe_to_mouthpiece__connection_lower = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 * 7
            pipe_to_mouthpiece__connection_upper = self.build_mouthpiece_cover_sketch(
                workplane=pipe_to_mouthpiece__connection_lower.workplane(
                    offset=self.config.mouthpiece_length / 8 * 7, origin=(0, 0)))
            mouthpiece1 = pipe_to_mouthpiece__connection_upper.loft()

        mouthpiece_middle2 = self.build_mouthpiece_cover_sketch(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
        mouthpiece_upper = self.build_mouthpiece_cover_sketch(
            mouthpiece_middle2.workplane(
                offset=self.config.mouthpiece_length / 8 * 1, origin=(0, 0)))
        mouthpiece2 = mouthpiece_upper.loft()
        position += self.config.mouthpiece_length / 8 * 1

        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=position,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.LeftUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()

        top_cover = plate.union(labium, clean=True)\
            .union(cord_knob_left, clean=True)\
            .union(cord_knob_right, clean=True)
        for air_hole_wall in air_hole_walls:
            top_cover = top_cover.union(air_hole_wall, clean=True)\
                .union(slow_airchamber_cover, clean=True)\
                .union(mouthpiece1, clean=True)\
                .union(mouthpiece2, clean=True)\
                .union(cord_knob_mouthpiece_left, clean=True)\
                .union(cord_knob_mouthpiece_right, clean=True)

        self._built = sound_pipe_extensions
        self._built.append(top_cover)
        if self.config.stacking_adapter_length != 0:
            for index in range(0, len(self._built) - 1):
                self._built[index] = self._built[index].cut(self._built[index + 1], clean=False)  # TODO: find out why clean fails here
        return self._built

    def carve_body_stacking(self, body_with_extensions: List[cq.Workplane]) -> cq.Workplane:
        if self._built is None:
            self.build()
        if self.config.stacking_carving_height != 0:
            for index, part in enumerate(self._built):
                self._built[index] = part.cut(body_with_extensions[index], clean=False)  # TODO: find out why clean fails here
        return self._built


    def carve_mouthpiece_stacking(self, mouthpiece_cover_obj: cq.Workplane) -> cq.Workplane:
        if self._built is None:
            self.build()
        if self.config.stacking_adapter_length != 0:
            self._built[-1] = self._built[-1].cut(mouthpiece_cover_obj, clean=False)  # TODO: find out why clean fails here
        return self._built

    # def _artifact_remover_sketch(self, position, width, left_side):
    #     side = -1 if left_side else 1
    #     sketch = (cq.Workplane("YZ").workplane(offset=position)
    #               .center((width / 2 - self.config.pipe_border / 2) * side, 0)
    #               .line(self.config.pipe_border * side, 0)
    #               .line(0, -20)
    #               .line(-self.config.pipe_border * side, 0)
    #               .close())
    #     return sketch

    # # the mouthpiece curvature of the cover and body isn't exactly the same, so some artifacts remain when subtracting
    # # the body from the cover via carve_body_stacking. As fixing this divergence isn't easy the current workaround is
    # # to remove the artifacts
    # def remove_artifacts(self):
    #     position = self.config.length + self.config.slow_airchamber_cover_length + self.config.air_hole_length - 5
    #     #left_0 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     #right_0 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position),left_side=False)
    #     position += 5
    #     left_1 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     right_1 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=False)
    #     position += 0.1
    #     left_2 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     right_2 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=False)
    #     position += self.config.mouthpiece_length / 8 - 0.1
    #     left_3 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     right_3 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=False)
    #     position += self.config.mouthpiece_length / 8 * 6 - 0.1
    #     left_4 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=True)
    #     right_4 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=False)
    #     position += 0.1
    #     left_5 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=True)
    #     right_5 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=False)
    #     left_block = continuous_connection(left_1.wires().val(), left_5.wires().val(), invert_normals=False,
    #                                        middle_wires=[left_2.wires().val(), left_3.wires().val(), left_4.wires().val()])
    #     right_block = continuous_connection(right_1.wires().val(), right_5.wires().val(), invert_normals=False,
    #                                         middle_wires=[right_2.wires().val(), right_3.wires().val(), right_4.wires().val()])
    #     #left_miniblock = continuous_connection(left_0.wires().val(), left_1.wires().val(), invert_normals=False)
    #     #right_miniblock = continuous_connection(right_0.wires().val(), right_1.wires().val(), invert_normals=False)
    #     if self._built is None:
    #         self.build()
    #     self._built[-1] = self._built[-1].cut(left_block, clean=False)
    #     self._built[-1] = self._built[-1].cut(right_block, clean=False)
    #     # removing the mini face-only artifact doesn't seem to work
    #     #self._built[-1] = self._built[-1].cut(left_miniblock, clean=False)
    #     #self._built[-1] = self._built[-1].cut(right_miniblock, clean=False)
    #     return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig) -> cq.Assembly:
    from polymoe_mouthpiece_coverplate import MouthpieceCoverplate, MouthpieceCoverplateConfig

    body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(body_config)
    mouthpiece_coverplate_config = MouthpieceCoverplateConfig(body_config, cover_config)

    body = FluteBody(body_config)
    body_objs = body.add_stacking()
    mouthpiece_coverplate = MouthpieceCoverplate(mouthpiece_coverplate_config)
    mouthpiece_coverplate_obj = mouthpiece_coverplate.build()

    cover = Cover(cover_config)
    cover.carve_mouthpiece_stacking(mouthpiece_coverplate_obj)
    cover.carve_body_stacking(body_objs)
    #cover.remove_artifacts()
    cover_with_extensions_objs = cover.build()
    volume = cover.volume()
    weight = cover.weight()
    print(f"volume cover: {volume:.1f} cm^3")
    print(f"weight cover: {weight:.1f} g")

    assembly = cq.Assembly()
    for part in cover_with_extensions_objs:
        assembly.add(part)
    return assembly


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        result.save("polymoe.step")
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END