def calc_slow_air_chamber_factor(num_octaves):
    z=1
    n=3
    for octave in range(1, num_octaves):
       z=n
       n=z*2+1
    return z/n


def calc_fretboard_length_factor(num_octaves):
    z = 1
    n = 3
    for octave in range(1, num_octaves):
        z = n
        n = z * 2 + 1
    return z / (z+1)


def calc_sound_chamber_factor(slow_air_chamber_factor):
    return 1 - slow_air_chamber_factor


def calc_frequency_and_length(frequency, length, slow_air_chamber_length, sound_chamber_length,
                              min_octave_length, sound_chamber_factor, num_octaves):
    if length is not None:
        frequency = 34300/(2*sound_chamber_factor*length)
    elif frequency is not None:
        length = 34300/(2*frequency)/sound_chamber_factor
    elif slow_air_chamber_length is not None:
        length = slow_air_chamber_length / (1 - sound_chamber_factor)
        frequency = 34300 / (2 * sound_chamber_factor * length)
    elif sound_chamber_length is not None:
        length = sound_chamber_length / sound_chamber_factor
        frequency = 34300 / (2 * sound_chamber_length)
    elif min_octave_length is not None:
        slow_air_chamber_length = min_octave_length * (pow(2, num_octaves) - 1)
        length = slow_air_chamber_length / (1 - sound_chamber_factor)
        frequency = 34300 / (2 * sound_chamber_factor * length)
    return frequency, length

def calc_frequency_and_length_fixed_slow_airchamber(frequency, length, slow_air_chamber_length, sound_chamber_length,
                              min_octave_length, num_octaves):
    if length is not None:
        frequency = 34300/(2*(length - slow_air_chamber_length))
    elif frequency is not None:
        length = 34300/(2*frequency) + slow_air_chamber_length
    elif sound_chamber_length is not None:
        length = sound_chamber_length + slow_air_chamber_length
        frequency = 34300 / (2 * sound_chamber_length)
    elif min_octave_length is not None:
        sound_chamber_length = min_octave_length * pow(2, num_octaves)
        length = slow_air_chamber_length + sound_chamber_length
        frequency = 34300 / (2 * sound_chamber_length)
    return frequency, length


def calc_frequency_and_length_closed(frequency,
                                     length,
                                     min_octave_length,
                                     slow_airchamber_length,
                                     sound_chamber_length,
                                     octaves):
    if length is not None:
        frequency = 34300/(4*(length - slow_airchamber_length))
    elif frequency is not None:
        length = 34300/(4*frequency)+slow_airchamber_length
    elif sound_chamber_length is not None:
        length = sound_chamber_length + slow_airchamber_length
        frequency = 34300 / (4 * sound_chamber_length)
    elif min_octave_length is not None:
        length = min_octave_length * (pow(2, octaves)) + slow_airchamber_length
        frequency = 34300/(4*(length - slow_airchamber_length))
    return frequency, length

def calc_octave_lengths(octaves, slow_air_chamber_factor, length):
    lengths = []
    octave_length = 1 / (pow(2, octaves) - 1) * slow_air_chamber_factor * length
    for octave in range(1, octaves + 1):
        lengths.append(f"{octave_length:.1f}")
        octave_length = octave_length * 2
    return lengths

def calc_octave_lengths2(octaves, sound_chamber_length):
    lengths = []
    octave_length = 1 / pow(2, octaves) * sound_chamber_length
    for octave in range(1, octaves + 1):
        lengths.append(f"{octave_length:.1f}")
        octave_length = octave_length * 2
    return lengths


def main():
    init_length = None  # 122 42
    init_frequency = None  # 440
    init_min_octave_length = 6  # 2.8 6
    init_sound_chamber_length = None  # 42

    init_slow_air_chamber_length = 5  # 160

    if init_slow_air_chamber_length is None:
        print("open flute with variable slow air chamber:")
        for octaves in range(1, 6):
            slow_air_chamber_factor = calc_slow_air_chamber_factor(octaves)
            sound_chamber_factor = calc_sound_chamber_factor(slow_air_chamber_factor)
            frequency, length = calc_frequency_and_length(init_frequency,
                                                          init_length,
                                                          init_slow_air_chamber_length,
                                                          init_sound_chamber_length,
                                                          init_min_octave_length,
                                                          sound_chamber_factor,
                                                          octaves)
            octave_lengths = calc_octave_lengths(octaves, slow_air_chamber_factor, length)
            print(f"octaves: {octaves},"
                  f" slow air chamber length: {slow_air_chamber_factor * length:.1f},"
                  f" sound chamber length: {sound_chamber_factor * length:.1f},"
                  f" frequency: {frequency:.1f}"
                  f" length: {length:.1f}"
                  f" octave lengths: {octave_lengths}")
    else:
        print("open flute with fixed slow air chamber:")
        for octaves in range(1, 6):
            frequency, length = calc_frequency_and_length_fixed_slow_airchamber(
                init_frequency,
                init_length,
                init_slow_air_chamber_length,
                init_sound_chamber_length,
                init_min_octave_length,
                octaves)
            slow_air_chamber_factor = init_slow_air_chamber_length / length
            sound_chamber_factor = 1 - slow_air_chamber_factor
            sound_chamber_length = length - init_slow_air_chamber_length
            octave_lengths = calc_octave_lengths2(octaves, sound_chamber_length)
            print(f"octaves: {octaves},"
                  f" slow air chamber length: {slow_air_chamber_factor * length:.1f},"
                  f" sound chamber length: {sound_chamber_factor * length:.1f},"
                  f" frequency: {frequency:.1f}"
                  f" length: {length:.1f}"
                  f" octave lengths: {octave_lengths}")
    print("closed flute:")
    init_slow_airchamber_length=2
    for octaves in range(1, 6):
        frequency, length = calc_frequency_and_length_closed(init_frequency,
                                                             init_length,
                                                             init_min_octave_length,
                                                             init_slow_airchamber_length,
                                                             init_sound_chamber_length,
                                                             octaves)
        fretboard_length_factor = calc_fretboard_length_factor(octaves)
        octave_lengths = calc_octave_lengths(octaves, fretboard_length_factor, length - init_slow_airchamber_length)
        print(f"octaves: {octaves},"
              f" sound chamber length: {length - init_slow_airchamber_length:.1f},"
              f" frequency: {frequency:.1f}"
              f" length: {length:.1f}"
              f" octave lengths: {octave_lengths}")


main()
