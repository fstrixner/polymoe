import cadquery as cq

from polymoe_config import FluteBodyConfig

# TODO: Deprecated - rework

class WingsConfig:
    wing_height = 100
    wing_gap_distance = 10
    wing_gap_height = 20
    wing_gap_length = 8
    wing_length_after_gap = 40
    start_position_x = -(wing_gap_length / 2 + wing_gap_distance + wing_length_after_gap)  # TODO: use assembly for that
    density = 0.34  # g/cm^3 cedar wood

    def __init__(self, flute_body_config: FluteBodyConfig):
        self.pipe_border = flute_body_config.pipe_border
        self._flute_body_config = flute_body_config
        self.wing_gap_min = self.wing_height - self.pipe_height_at_position(0) - self.pipe_border * 2 + self._flute_body_config.wind_channel_height / 2
        self.num_pipes = self._flute_body_config.num_pipes

    def pipe_width_at_position(self, position: float) -> float:
        return self._flute_body_config.pipe_width_at_position(position)

    def pipe_height_at_position(self, position: float) -> float:
        return self._flute_body_config.pipe_height_at_position(position)

    def width_at_position(self, position: float) -> float:
        return self._flute_body_config.body_width_at_position(position)

    def position_z(self, position) -> float:
        return -self.pipe_height_at_position(position) - self._flute_body_config.pipe_border


class Wings:
    def __init__(self, config: WingsConfig):
        self.config = config
        self._built = None


    def _build_wings_sketch(self, workplane: cq.Workplane, position: float, gap_depth) -> cq.Workplane:
        w = workplane
        w = w.center(-self.config.width_at_position(position) / 2, 0)
        w = w.line(0, self.config.wing_height - gap_depth)
        w = w.line(self.config.pipe_border, 0)
        for pipe_num in range(0,self.config.num_pipes):
            w = w.line(0, -self.config.wing_height + self.config.pipe_border + gap_depth)
            w = w.line(self.config.pipe_width_at_position(position), 0)
            w = w.line(0, self.config.wing_height - self.config.pipe_border - gap_depth)
            w = w.line(self.config.pipe_border, 0)
        w = w.line(0, -self.config.wing_height + gap_depth)
        w = w.close()
        return w

    def build(self) -> cq.Workplane:
        position = self.config.start_position_x
        workplane = cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)).center(0, self.config.position_z(position))
        wings_before_gap_lower = self._build_wings_sketch(workplane, position=position, gap_depth=1)
        position += self.config.wing_gap_distance
        wings_before_gap_upper = self._build_wings_sketch(
            wings_before_gap_lower.workplane(offset=self.config.wing_gap_distance, origin=(0, 0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=0
        )
        wings_before_gap = wings_before_gap_upper.loft()

        wings_gap_lower = self._build_wings_sketch(
            wings_before_gap_upper.workplane(origin=(0, 0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=self.config.wing_gap_height
        )
        position += self.config.wing_gap_length
        wings_gap_upper = self._build_wings_sketch(
            wings_gap_lower.workplane(offset=self.config.wing_gap_length, origin=(0, 0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=self.config.wing_gap_height
        )
        wings_gap = wings_gap_upper.loft()

        wings_after_gap_lower = self._build_wings_sketch(
            wings_gap_upper.workplane(origin=(0,0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=0
        )
        position += self.config.wing_length_after_gap * 0.3
        wings_after_gap_middle = self._build_wings_sketch(
            wings_after_gap_lower.workplane(offset=self.config.wing_length_after_gap * 0.3, origin=(0, 0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=self.config.wing_gap_height * 0.3
        )
        position += self.config.wing_length_after_gap * 0.1
        wings_after_gap_middle2 = self._build_wings_sketch(
            wings_after_gap_middle.workplane(offset=self.config.wing_length_after_gap * 0.1,
                                             origin=(0, 0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=self.config.wing_gap_min * 0.3
        )
        position += self.config.wing_length_after_gap * 0.6
        wings_after_gap_upper = self._build_wings_sketch(
            wings_after_gap_middle.workplane(offset=self.config.wing_length_after_gap * 0.6, origin=(0, 0)).center(0, self.config.position_z(position)),
            position=position,
            gap_depth=self.config.wing_gap_min
        )
        wings_after_gap = wings_after_gap_upper.loft()

        wings = (cq.Workplane()
                .add(wings_before_gap)
                .add(wings_gap)
                .add(wings_after_gap)
                 )

        self._built = wings
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

def main():
    body_config = FluteBodyConfig()

    wings_config = WingsConfig(body_config)
    wings = Wings(wings_config)
    wings_obj = wings.build()

    return wings_obj


if __name__ == '__main__':
    result = main()
    cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)