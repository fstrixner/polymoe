import math

import cadquery as cq


class FluteConfig:
    num_pipes = 4
    pipe_length = 1000
    pipe_width_at_bottom = 300
    pipe_width_at_top = 30
    pipe_height_at_bottom = 30
    pipe_height_at_top = 30
    pipe_border_width = 3
    pipe_notch_width = 2
    pipe_notch_height = 3
    soundpipe_length = 900
    air_hole_length = 10
    wind_channel_height = 2
    wind_channel_length = 20
    wind_channel_ramp_length = 15
    left_lower_fillet = 5
    right_lower_fillet = 5
    mouthpiece_height = 10
    mouthpiece_width = 12
    mouthpiece_length =40
    mouthpiece_border_width = 3
    wing_height = 100
    wing_gap_distance = 10
    wing_gap_height = 20
    wing_gap_length = 8
    wing_length_after_gap = 40

    @property
    def slow_airchamber_length(self):
        return self.pipe_length - self.soundpipe_length - self.air_hole_length - self.wind_channel_length - self.wind_channel_ramp_length

    def pipe_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_bottom\
               - (self.pipe_height_at_bottom - self.pipe_height_at_top) * position / self.pipe_length

    def pipe_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_bottom\
                - (self.pipe_width_at_bottom - self.pipe_width_at_top) * position / self.pipe_length

    def mouthpiece_turning_angle(self, pipe_index: int) -> float:
        angle_between_cutting_points = 2 * math.asin(
            (self.pipe_width_at_bottom - self.pipe_width_at_top) / self.pipe_length)
        taper_angle = (math.pi - angle_between_cutting_points / 4) / 2
        turn_angle = int(pipe_index - self.num_pipes / 2) *  (math.pi / 2 - taper_angle)
        log(f"angle_between_cutting_points: {angle_between_cutting_points / math.pi * 180}")
        log(f"taper_angle: {taper_angle / math.pi * 180}")
        log(f"turn_angle: {turn_angle / math.pi * 180}")
        if pipe_index < self.num_pipes / 2:
            return turn_angle / math.pi * 180
        else:
            return -turn_angle / math.pi * 180

    def mouthpiece_lower_left_position(self, pipe_index: int) -> cq.Vector:
        if self.pipe_width_at_top == self.pipe_width_at_bottom:
            instrument_width = self.pipe_width_at_top * self.num_pipes
        else:
            angle_between_cutting_points = 2 * math.asin((self.pipe_width_at_bottom - self.pipe_width_at_top) / self.pipe_length)
            #distance_top_from_cutting_point_between_pipe_borders = self.pipe_width_at_top /  (2 * math.sin(angle_between_cutting_points / 2))
            distance_top_from_cutting_point_between_pipe_borders = self.pipe_width_at_top / (
                    2 * (self.pipe_width_at_bottom - self.pipe_width_at_top) / self.pipe_length)
            instrument_width = 2 * math.sin(angle_between_cutting_points / 2 * self.num_pipes) * distance_top_from_cutting_point_between_pipe_borders
        instrument_left_x_pos = self.pipe_lower_left_at_position(
            self.pipe_length).x - pipe_index * self.pipe_width_at_top
        rel_x_pos = (instrument_width - 4 * self.mouthpiece_width) / 2 + pipe_index * self.mouthpiece_width
        x_pos = instrument_left_x_pos + rel_x_pos
        y_pos = self.pipe_height_at_position(self.pipe_length) - self.mouthpiece_height
        return cq.Vector(x_pos, y_pos)

    def pipe_lower_left_at_position(self, position: int) -> cq.Vector:
        w = (self.pipe_width_at_bottom - self.pipe_width_at_top) / 2 * position / self.pipe_length
        h = (self.pipe_height_at_bottom - self.pipe_height_at_top) / 2 * position / self.pipe_length
        return cq.Vector(h, w)



class FluteBody:
    def __init__(self, config: FluteConfig, pipe_index: int):
        self.config = config
        self.pipe_index = pipe_index

    def _build_wings_sketch(self, workplane: cq.Workplane, position: int, gap_depth) -> cq.Workplane:
        return workplane \
            .line(0, self.config.wing_height - gap_depth) \
            .line(self.config.pipe_border_width, 0) \
            .line(0, -self.config.wing_height + self.config.pipe_border_width + gap_depth) \
            .line(self.config.pipe_width_at_position(position) - 2 * self.config.pipe_border_width, 0) \
            .line(0, self.config.wing_height - self.config.pipe_border_width - gap_depth) \
            .line(self.config.pipe_border_width, 0) \
            .line(0, -self.config.wing_height + gap_depth) \
            .close()

    def _build_pipe_soundpipe_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return workplane \
            .line(0, self.config.pipe_height_at_position(position)) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_notch_height) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position) + self.config.pipe_notch_height + self.config.pipe_border_width) \
            .line(self.config.pipe_width_at_position(position) - 2 * self.config.pipe_border_width, 0) \
            .line(0, self.config.pipe_height_at_position(position) - self.config.pipe_border_width - self.config.pipe_notch_height) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, self.config.pipe_notch_height) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position)) \
            .close()

    def _build_pipe_air_hole_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return workplane \
            .line(0, self.config.pipe_height_at_position(position)) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_notch_height + self.config.wind_channel_height / 2) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position) + self.config.pipe_notch_height
                  - self.config.wind_channel_height / 2 + self.config.pipe_border_width) \
            .line(self.config.pipe_width_at_position(position) - 2 * self.config.pipe_border_width, 0) \
            .line(0, self.config.pipe_height_at_position(position) - self.config.pipe_border_width
                  - self.config.pipe_notch_height + self.config.wind_channel_height / 2) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, self.config.pipe_notch_height - self.config.wind_channel_height / 2) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position)) \
            .close()

    def _build_pipe_body_block_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return workplane \
            .line(0, self.config.pipe_height_at_position(position)) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_notch_height + self.config.wind_channel_height / 2) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, -self.config.wind_channel_height) \
            .line(self.config.pipe_width_at_position(position) - 2 * self.config.pipe_border_width, 0) \
            .line(0, self.config.wind_channel_height) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, self.config.pipe_notch_height - self.config.wind_channel_height / 2) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position)) \
            .close()

    def _build_pipe_slow_airchamber_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return workplane \
            .line(0, self.config.pipe_height_at_position(position)) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_notch_height + self.config.wind_channel_height / 2) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position) + self.config.pipe_notch_height + self.config.pipe_border_width) \
            .line(self.config.pipe_width_at_position(position) - 2 * self.config.pipe_border_width, 0) \
            .line(0, self.config.pipe_height_at_position(position) - self.config.pipe_border_width - self.config.pipe_notch_height) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, self.config.pipe_notch_height - self.config.wind_channel_height / 2) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position)) \
            .close()

    def _build_pipe_body_top_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return workplane \
            .line(0, self.config.pipe_height_at_position(position)) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position) + self.config.pipe_border_width - self.config.pipe_notch_height) \
            .line(self.config.pipe_width_at_position(position) - 2 * (self.config.pipe_border_width - self.config.pipe_notch_width), 0) \
            .line(0, self.config.pipe_height_at_position(position) - self.config.pipe_border_width + self.config.pipe_notch_height) \
            .line(self.config.pipe_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_height_at_position(position)) \
            .close()

    def _build_mouth_piece(self, workplane: cq.Workplane) -> cq.Workplane:
        return workplane \
            .line(0, self.config.mouthpiece_height) \
            .line(self.config.mouthpiece_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_notch_height + self.config.wind_channel_height / 2) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, -self.config.mouthpiece_height
                  + self.config.pipe_notch_height + self.config.mouthpiece_border_width) \
            .line(self.config.mouthpiece_width - 2 * self.config.mouthpiece_border_width, 0) \
            .line(0, self.config.mouthpiece_height
                  - self.config.mouthpiece_border_width - self.config.pipe_notch_height) \
            .line(self.config.pipe_notch_width, 0) \
            .line(0, self.config.pipe_notch_height - self.config.wind_channel_height / 2) \
            .line(self.config.mouthpiece_border_width - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.mouthpiece_height) \
            .close()

    def build(self):
        position = 0
        workplane = cq.Workplane("YZ")

        wings_before_gap_lower = self._build_wings_sketch(workplane, position=position, gap_depth=1)
        position += self.config.wing_gap_distance
        wings_before_gap_upper = self._build_wings_sketch(
            wings_before_gap_lower.workplane(offset=self.config.wing_gap_distance, origin=self.config.pipe_lower_left_at_position(position)),
            position=position,
            gap_depth=1
        )
        wings_before_gap = wings_before_gap_upper.loft()

        wings_gap_lower = self._build_wings_sketch(
            wings_before_gap_upper.workplane(),
            position=position,
            gap_depth=self.config.wing_gap_height
        )
        position += self.config.wing_gap_length
        wings_gap_upper = self._build_wings_sketch(
            wings_gap_lower.workplane(offset=self.config.wing_gap_length, origin=self.config.pipe_lower_left_at_position(position)),
            position=position,
            gap_depth=self.config.wing_gap_height
        )
        wings_gap = wings_gap_upper.loft()

        wings_after_gap_lower = self._build_wings_sketch(
            wings_gap_upper.workplane(),
            position=position,
            gap_depth=1
        )
        position += self.config.wing_length_after_gap * 0.3
        wings_after_gap_middle = self._build_wings_sketch(
            wings_after_gap_lower.workplane(offset=self.config.wing_length_after_gap * 0.3, origin=self.config.pipe_lower_left_at_position(position)),
            position=position,
            gap_depth=self.config.wing_gap_height * 0.3
        )
        position += self.config.wing_length_after_gap * 0.1
        wings_after_gap_middle2 = self._build_wings_sketch(
            wings_after_gap_middle.workplane(offset=self.config.wing_length_after_gap * 0.1,
                                            origin=self.config.pipe_lower_left_at_position(position)),
            position=position,
            gap_depth=(self.config.wing_height - self.config.pipe_height_at_position(position)) * 0.3
        )
        position += self.config.wing_length_after_gap * 0.6
        wings_after_gap_upper = self._build_wings_sketch(
            wings_after_gap_middle.workplane(offset=self.config.wing_length_after_gap * 0.6, origin=self.config.pipe_lower_left_at_position(position)),
            position=position,
            gap_depth=self.config.wing_height - self.config.pipe_height_at_position(position)
        )
        wings_after_gap = wings_after_gap_upper.loft()

        soundpipe_lower = self._build_pipe_soundpipe_sketch(
            wings_after_gap_upper.workplane()
            , position=position)
        position += self.config.soundpipe_length
        soundpipe_upper = self._build_pipe_soundpipe_sketch(
            soundpipe_lower.workplane(offset=self.config.soundpipe_length, origin=self.config.pipe_lower_left_at_position(position)), position=position)
        soundpipe = soundpipe_upper.loft()

        air_hole_lower = self._build_pipe_air_hole_sketch(
            soundpipe_upper.workplane(origin=self.config.pipe_lower_left_at_position(position)), position=position)
        position += self.config.air_hole_length
        air_hole_upper = self._build_pipe_air_hole_sketch(
            air_hole_lower.workplane(offset=self.config.air_hole_length, origin=self.config.pipe_lower_left_at_position(position)), position=position)
        air_hole = air_hole_upper.loft()

        block_lower = self._build_pipe_body_block_sketch(
            air_hole_upper.workplane(origin=self.config.pipe_lower_left_at_position(position)), position=position)
        position += self.config.wind_channel_length
        block_upper = self._build_pipe_body_block_sketch(
            block_lower.workplane(offset=self.config.wind_channel_length, origin=self.config.pipe_lower_left_at_position(position)), position=position)
        block = block_upper.loft()

        block_ramp_lower = self._build_pipe_body_block_sketch(
            block_upper.workplane(origin=self.config.pipe_lower_left_at_position(position)), position=position)
        position += self.config.wind_channel_ramp_length
        block_ramp_upper = self._build_pipe_slow_airchamber_sketch(
            block_ramp_lower.workplane(offset=self.config.wind_channel_ramp_length, origin=self.config.pipe_lower_left_at_position(position)), position=position)
        block_ramp = block_ramp_upper.loft()

        slow_airchamber_lower = self._build_pipe_slow_airchamber_sketch(
            block_ramp_upper.workplane(origin=self.config.pipe_lower_left_at_position(position)), position=position)
        position += self.config.slow_airchamber_length
        slow_airchamber_upper = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_lower.workplane(offset=self.config.slow_airchamber_length, origin=self.config.pipe_lower_left_at_position(position)), position=position)
        slow_airchamber = slow_airchamber_upper.loft()

        # top_lower = self._build_pipe_body_top_sketch(
        #     slow_airchamber_upper.vertices("<X and <Y and <Z").workplane(), position=position)
        # position += self.config.pipe_notch_width
        # top_upper =  self._build_pipe_body_top_sketch(
        #     top_lower.vertices("<X and <Y and <Z").workplane(self.config.pipe_notch_width), position=position)
        # top = top_upper.loft()

        mouthpiece_lower = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_upper.workplane(), position=position)
        mouthpiece_pos = self.config.mouthpiece_lower_left_position(self.pipe_index)
        position += self.config.mouthpiece_length / 2
        mouthpiece_turning_angle = self.config.mouthpiece_turning_angle(self.pipe_index)
        mouthpiece_middle = self._build_mouth_piece(
            mouthpiece_lower.workplane(
                offset=self.config.mouthpiece_length / 2,
                origin=(mouthpiece_pos.y, mouthpiece_pos.x)
            ).transformed(rotate=(0, mouthpiece_turning_angle, 0))) # .center(mouthpiece_pos.x, mouthpiece_pos.y)
        mouthpiece_base = mouthpiece_middle.loft()
        mouthpiece_middle2 = self._build_mouth_piece(
            mouthpiece_middle.workplane())
        position += self.config.mouthpiece_length / 2
        mouthpiece_upper = self._build_mouth_piece(
            mouthpiece_middle2.workplane(offset=self.config.mouthpiece_length / 2))
        mouthpiece_top = mouthpiece_upper.loft()

        flute_body = wings_before_gap.add(
            wings_gap.add(
                wings_after_gap.add(
                    soundpipe.add(air_hole.add(block.add(block_ramp.add(slow_airchamber.add(mouthpiece_base.add(mouthpiece_top))))))
                )
            )
        )
        #body_with_fillets = flute_body.tag("body")\
        #    .edges("<Y and <Z").fillet(self.config.left_lower_fillet)

        #body_with_fillets3 = body_with_fillets.workplaneFromTagged("body")\
        #    .edges("<Y and >Z").fillet(5)

        # body_with_fillets2 = body_with_fillets\
        #    .edges(cq.selectors.AndSelector(
        #         cq.selectors.BaseDirSelector(cq.Vector(-self.config.pipe_length,
        #                                                self.config.pipe_width_at_bottom - self.config.pipe_width_at_top,
        #                                                0), tolerance=1.0),
        #         cq.selectors.DirectionMinMaxSelector(cq.Vector(0, 0, 1), directionMax=False)))\
        #     .fillet(self.config.right_lower_fillet)

        return flute_body


def main():
    config = FluteConfig()
    body = FluteBody(config, 1)
    return body.build()


result = main()
