# SINGLEPY_IGNORE_BEGIN
import cadquery as cq
from polymoe_body import FluteBody, FluteBodyConfig
from polymoe_carry_hook import CarryHookConfig, CarryHook
from polymoe_cover import Cover, CoverConfig
from polymoe_curved_mouthpiece import CurvedMouthpieceConfig, CurvedMouthpiece
from polymoe_mouthpiece_coverplate import MouthpieceCoverplateConfig, MouthpieceCoverplate
from polymoe_pitch_strips import PitchStripConfig, PitchStrip
from polymoe_stringplate import StringPlateConfig
from polymoe_wings import WingsConfig, Wings
from polymoe_config import *
# SINGLEPY_IGNORE_END

class WeightAndVolumeCounter:
    def __init__(self):
        self.volume = 0
        self.weight = 0

    def add(self, obj, name):
        volume = obj.volume()
        weight = obj.weight()

        print(f"volume {name}: {volume:.1f} cm^3")
        print(f"weight {name}: {weight:.1f} g")

        self.volume += volume
        self.weight += weight

    def print_sum(self):
        print(f"weight total: {self.weight:.1f} g")
        print(f"volume total: {self.volume:.1f} cm^3")

def main(config: PolyMoeConfig):
    body_config = FluteBodyConfig(config)
    body = FluteBody(body_config)

    fingerboard_config = CoverConfig(body_config)
    stringplate_config = StringPlateConfig(body_config, fingerboard_config)
    hook_config = CarryHookConfig(body_config=body_config, stringplate_config=stringplate_config)
    mouthpiece_cover_config = MouthpieceCoverplateConfig(body_config, fingerboard_config)
    wings_config = WingsConfig(body_config)
    if config.use_curved_mouthpiece:
        curved_mouthpiece_config = CurvedMouthpieceConfig(body_config=body_config, cover_config=fingerboard_config,
                                                          mouthpiece_coverplate_config=mouthpiece_cover_config)

    hook = CarryHook(hook_config)
    fingerboard = Cover(fingerboard_config)
    wings = Wings(wings_config)
    stringplate = Wings(stringplate_config)
    mouthpiece_cover = MouthpieceCoverplate(mouthpiece_cover_config)
    if config.use_curved_mouthpiece:
        curved_mouthpiece = CurvedMouthpiece(curved_mouthpiece_config)

    hook_obj = hook.build()
    body_with_extensions_objs = body.build()
    mouthpiece_cover_obj = mouthpiece_cover.build()
    fingerboard.build()
    fingerboard.carve_body_stacking(body_with_extensions_objs)
    #fingerboard.remove_artifacts()
    fingerboard_and_extensions_objs = fingerboard.carve_mouthpiece_stacking(mouthpiece_cover_obj)
    wings_obj = wings.build()
    stringplate_obj = stringplate.build()
    if config.use_curved_mouthpiece:
        curved_mouthpiece_objs = curved_mouthpiece.build()

    body.carve_mouthpiece_stacking(mouthpiece_cover_obj)
    body.carve_hook_stacking(hook_obj)
    body_with_extensions_objs_and_stacking = body.add_stacking()

    if config.use_curved_mouthpiece:
        mouthpiece_cover_obj = mouthpiece_cover.move_coverplate_behind_curved_mouthpiece()
        #TODO: why is this intersection not working? curved_mouthpiece_objs = curved_mouthpiece.carve_stacking(mouthpiece_cover_obj)

    flute = cq.Assembly()
    counter = WeightAndVolumeCounter()
    for index, part in enumerate(body_with_extensions_objs_and_stacking):
        if index < len(body_with_extensions_objs) - 1:
            text = f"body extension {index + 1}"
        else:
            text = "body"
        flute = flute.add(part, name=text, color=cq.Color("burlywood3"))
    for index, part in enumerate(fingerboard_and_extensions_objs):
        if index < len(fingerboard_and_extensions_objs) - 1:
            text = f"fingerboard extension {index + 1}"
        else:
            text = "fingerboard"
        flute = flute.add(part, name=text, color=cq.Color("burlywood3"))
    flute = flute.add(wings_obj, name="wings", color=cq.Color("burlywood3"))
    flute = flute.add(stringplate_obj, name="stringplate", color=cq.Color("burlywood3"))
    flute = flute.add(hook_obj, name="hook", color=cq.Color("burlywood3"))
    if config.use_curved_mouthpiece:
        flute = flute.add(curved_mouthpiece_objs[0], name="curved mouthpiece body", color=cq.Color("burlywood3"))
        flute = flute.add(curved_mouthpiece_objs[1], name="curved mouthpiece cover", color=cq.Color("burlywood3"))
        counter.add(curved_mouthpiece, "curved mouthpiece")
    flute = flute.add(mouthpiece_cover_obj, name="mouthpiece cover", color=cq.Color("burlywood3"))

    for pipe_index in range(body_config.num_pipes):
        if pipe_index in config.frequency_of_lowest_hole:
            frequency_of_lowest_hole = config.frequency_of_lowest_hole[pipe_index]
        else:
            frequency_of_lowest_hole = 0
        if pipe_index in config.pitchstrip_layout.keys():
            pitchstrip_layout = config.pitchstrip_layout[pipe_index]
        else:
            pitchstrip_layout = "continuous"
        if pitchstrip_layout == "continuous":
            strip_config = PitchStripConfig.continuous(body_config, fingerboard_config, pipe_index=pipe_index)
        elif pitchstrip_layout == "octaves":
            strip_config = PitchStripConfig.octaves(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "diatonic":
            strip_config = PitchStripConfig.diatonic(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "chromatic":
            strip_config = PitchStripConfig.chromatic(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "pentatonic":
            strip_config = PitchStripConfig.pentatonic(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "bourdon":
            strip_config = PitchStripConfig.bourdon(body_config, fingerboard_config, pipe_index=pipe_index)
        else:
            raise Exception(f"unexpected pitchstrip layout {pitchstrip_layout}")
        strip = PitchStrip(strip_config)
        strip_obj = strip.build()
        flute.add(strip_obj, name=f"pitch strip {pipe_index + 1}", color=cq.Color("gainsboro"))
        counter.add(strip, f"pitch strip {pipe_index + 1}")

    counter.add(body, "body")
    counter.add(fingerboard, "fingerboard")
    counter.add(wings, "wings")
    counter.add(mouthpiece_cover, "mouthpiece cover")
    counter.add(stringplate, "stringplate")
    counter.add(hook, "hook")

    counter.print_sum()

    return flute

# SINGLEPY_IGNORE_BEGIN
if __name__ == '__main__':
    # from importlib import reload
    # import sys
    #if 'polymoe_config' in sys.modules:
    #    del sys.modules['polymoe_config']
    #import polymoe_config
    #reload(polymoe_config)
    #from polymoe_config import PolyMoeConfig

    config = PolyMoeConfig()
    assembly = main(config)
   
    if config.save_step:
        assembly.save('polymoe.step')
    try:
       from ocp_vscode import show_object
    except:
       pass

    if 'show_object' in globals():
        show_object(assembly, name="polymoe", clear=True, reset_camera=True)
# SINGLEPY_IGNORE_END