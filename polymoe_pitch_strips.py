# SINGLEPY_IGNORE_BEGIN
import math
from typing import List, Union

import cadquery as cq

from polymoe_body import FluteBodyConfig
from polymoe_cover import CoverConfig, Cover
from polymoe_config import *
# SINGLEPY_IGNORE_END

# hole_positions:
#   * None: completely open - continuous
#   * list of cent values - hole at given cent position per octave
#   * empty list: no holes - bourdon pipe
HolePositions = Union[List[int], None]

class PitchStripConfig:

    @staticmethod
    def continuous(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int):
        config = PitchStripConfig(body_config, cover_config, hole_positions = None, pipe_index = pipe_index)
        return config

    @staticmethod
    def chromatic(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions = [100 for i in range(12)], pipe_index = pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def diatonic(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions = [200, 200, 100, 200, 200, 200, 100], pipe_index = pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def pentatonic(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions = [300, 200, 200, 300, 200], pipe_index = pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def octaves(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions=[1200], pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def bourdon(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int):
        config = PitchStripConfig(body_config, cover_config, hole_positions=[], pipe_index=pipe_index)
        return config

    def __init__(self, body_config: FluteBodyConfig, cover_config: CoverConfig, hole_positions: HolePositions, pipe_index: int, frequency_of_lowest_hole: float=None):
        self.polymoe_config = body_config.config

        self.density = self.polymoe_config.pitchstrip_density
        self.continuous_if_hole_distance_smaller_than = self.polymoe_config.pitchstrip_continuous_if_hole_distance_smaller_than
        self.hole_radius = self.polymoe_config.pitchstrip_hole_radius

        self.hole_positions = hole_positions
        self.frequency_of_lowest_hole=frequency_of_lowest_hole
        self.pipe_index = pipe_index
        self._cover_config = cover_config
        self.cover = Cover(self._cover_config)
        self.height = cover_config.height
        self.windchannel_offset = cover_config.windchannel_offset
        self.cut_diameter = cover_config.cut_diameter
        self.upper_fillet_radius = cover_config.upper_fillet_radius
        self.manual_length = cover_config.manual_length()
        self.inlay_depth = cover_config.inlay_depth
        self.inlay_length = cover_config.inlay_length
        self.soundpipe_length = body_config.soundpipe_length
        self.pipe_border = body_config.pipe_border

    def width_at_position(self, position: float):
        return self._cover_config.width_at_position(position)

    def fingerboard_border_at_position(self, position: float):
        return self._cover_config.fingerboard_border_at_position(position)


class PitchStrip:
    def __init__(self, config: PitchStripConfig):
        self.config = config
        self._built = None

    def _build_cover_inlay_carving(self, pipe_num: int) -> cq.Workplane:
        return self.config.cover.build_cover_inlay_carving(pipe_num, length=self.config.inlay_length)

    def _build_continuous_pitch_carving(self, pipe_num, continuous_start_pos: float) -> cq.Workplane:
        return self.config.cover.build_pitch_carving(pipe_num, length=self.config.manual_length, start_pos=continuous_start_pos)

    def _carve_hole(self, hole_position: float) -> cq.Workplane:
        w = cq.Workplane("XY").workplane(offset=self.config.height + self.config.windchannel_offset, origin=(0, 0))
        y_pos_0 = (self.config.width_at_position(hole_position) / 2 - self.config.pipe_border / 2
                   - self.config.fingerboard_border_at_position(hole_position)
                   - self.config.pipe_index * self.config.cut_diameter
                   - self.config.fingerboard_border_at_position(hole_position) * self.config.pipe_index * 2
                   - self.config.cut_diameter / 2
                   )
        w = w.center(hole_position,
                     -y_pos_0
                     ).circle(self.config.hole_radius).extrude(-self.config.inlay_depth * 3)
        return w

    def _hole_position_for_frequency(self, frequency_hz: float):
        speed_of_sound_mps = 343.2
        wavelength_m = speed_of_sound_mps / frequency_hz
        open_pipe_length_m = wavelength_m / 2
        position_in_mm = self.config.soundpipe_length - open_pipe_length_m * 1000
        return position_in_mm

    def _next_hole_position(self, current_hole_position: float, cent_offset: int) -> float:
        manual_length = self.config.manual_length
        remaining_manual = manual_length - current_hole_position
        if current_hole_position == 0:
            position = self._hole_position_for_frequency(self.config.frequency_of_lowest_hole)
        else:
            if cent_offset != 0:
                frequency_ratio = (2 ** (cent_offset / 1200))
                position = current_hole_position + remaining_manual * (1 - 1 / frequency_ratio)
            else:
                position = current_hole_position
        return position


    def build(self) -> cq.Workplane:
        self._built = self._build_cover_inlay_carving(self.config.pipe_index)
        current_hole_position = 0
        if self.config.hole_positions is None or len(self.config.hole_positions) != 0: # not bourdon
            if self.config.hole_positions is not None:
                hole_cent_index = 0
                current_hole_position = self._next_hole_position(0, 0)
                last_hole_position = -1000
                while current_hole_position - last_hole_position > self.config.continuous_if_hole_distance_smaller_than \
                    and current_hole_position < self.config.manual_length:
                    self._built = self._built.cut(self._carve_hole(current_hole_position))
                    last_hole_position = current_hole_position
                    cent_offset = self.config.hole_positions[hole_cent_index]
                    current_hole_position = self._next_hole_position(current_hole_position, cent_offset)
                    hole_cent_index = (hole_cent_index + 1) % len(self.config.hole_positions)
            self._built = self._built.cut(self._carve_hole(current_hole_position))
            self._built = self._built.cut(self._build_continuous_pitch_carving(self.config.pipe_index,
                                                                               continuous_start_pos=current_hole_position))
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig):
    flute_body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(flute_body_config)
    strips = cq.Workplane()
    volume = 0
    weight = 0
    for pipe_num in range(cover_config.num_pipes):
        if pipe_num == 0:
            strip_config = PitchStripConfig.octaves(body_config=flute_body_config, cover_config=cover_config,
                                                    pipe_index=pipe_num,
                                                    frequency_of_lowest_hole=config.frequency_of_lowest_hole)
        elif pipe_num == 1:
            strip_config = PitchStripConfig.chromatic(body_config=flute_body_config, cover_config=cover_config,
                                                      pipe_index=pipe_num,
                                                      frequency_of_lowest_hole=config.frequency_of_lowest_hole)
        elif pipe_num == 2:
            strip_config = PitchStripConfig.diatonic(body_config=flute_body_config, cover_config=cover_config,
                                                     pipe_index=pipe_num,
                                                     frequency_of_lowest_hole=config.frequency_of_lowest_hole)
        elif pipe_num == 3:
            strip_config = PitchStripConfig.pentatonic(body_config=flute_body_config, cover_config=cover_config,
                                                       pipe_index=pipe_num,
                                                       frequency_of_lowest_hole=config.frequency_of_lowest_hole)
        else:
            raise Exception("only four pitch strip configurations expected")
        strip = PitchStrip(strip_config)
        strip_obj = strip.build()
        volume += strip.volume()
        weight += strip.weight()
        strips.add(strip_obj)
    print(f"volume strips: {volume:.1f} cm^3")
    print(f"weight strips: {weight:.1f} g")

    return strips


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END