import cadquery as cq


class FluteConfig:
    num_pipes = 4
    pipe_length = 950
    pipe_width_at_bottom = 25
    pipe_width_at_top = 20
    pipe_height_at_bottom = 35
    pipe_height_at_top = 20
    pipe_border = 10
    pipe_notch_width = 2.5
    pipe_notch_height = 5
    soundpipe_length = 850
    air_hole_length = 10
    wind_channel_height = 2
    wind_channel_length = 20
    wind_channel_ramp_length = 15
    mouthpiece_pipe_height = 6
    mouthpiece_pipe_width = 6
    mouthpiece_length = 50
    mouthpiece_outer_border = 6.5
    mouthpiece_inner_border = 2
    mouthpiece_bottom_border = 3

    @property
    def slow_airchamber_length(self):
        return self.pipe_length - self.soundpipe_length - self.air_hole_length - self.wind_channel_length\
               - self.wind_channel_ramp_length

    def pipe_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_bottom\
               - (self.pipe_height_at_bottom - self.pipe_height_at_top) * position / self.pipe_length

    def pipe_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_bottom\
                - (self.pipe_width_at_bottom - self.pipe_width_at_top) * position / self.pipe_length

    def body_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_position(position) * self.num_pipes + self.pipe_border * (self.num_pipes + 1)

    def body_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_position(position) + self.pipe_border + self.pipe_notch_height

    def mouthpiece_width(self) -> float:
        return self.mouthpiece_pipe_width * self.num_pipes + self.mouthpiece_outer_border * (self.num_pipes + 1)

    def mouthpiece_height(self) -> float:
        return self.mouthpiece_pipe_height + self.mouthpiece_bottom_border + self.pipe_notch_height

    def total_length(self):
        return self.pipe_length + self.mouthpiece_length


class FluteBody:
    def __init__(self, config: FluteConfig, pipe_index: int):
        self.config = config
        self.pipe_index = pipe_index

    def _build_pipe_sketch(self, workplane: cq.Workplane, position: int, notch_height: float, bottom_height: float):
        sketch = workplane \
            .center(-self.config.body_width_at_position(position) / 2,
                    -self.config.body_height_at_position(position)) \
            .line(0, self.config.body_height_at_position(position)) \
            .line(self.config.pipe_border - self.config.pipe_notch_width, 0) \
            .line(0, -notch_height) \
            .line(self.config.pipe_notch_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            sketch = sketch.line(0, -bottom_height) \
                .line(self.config.pipe_width_at_position(position), 0) \
                .line(0, bottom_height)
            if pipe_num < self.config.num_pipes - 1:
                sketch = sketch.line(self.config.pipe_border, 0)
        sketch = sketch.line(self.config.pipe_notch_width, 0) \
            .line(0, notch_height) \
            .line(self.config.pipe_border - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.body_height_at_position(position)) \
            .close()
        return sketch

    def _build_pipe_soundpipe_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height + self.config.wind_channel_height / 2,
                                       self.config.pipe_height_at_position(position) - self.config.wind_channel_height / 2)

    def _build_pipe_air_hole_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.pipe_height_at_position(position))

    def _build_pipe_body_block_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.wind_channel_height)

    def _build_pipe_slow_airchamber_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.pipe_height_at_position(
                                           position))

    def _build_mouth_piece(self, workplane: cq.Workplane) -> cq.Workplane:
        sketch = workplane \
            .center(-self.config.mouthpiece_width()/2,
                    -self.config.mouthpiece_height())\
            .line(0, self.config.mouthpiece_height()) \
            .line(self.config.mouthpiece_outer_border - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.pipe_notch_height) \
            .line(self.config.pipe_notch_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            sketch = sketch.line(0, -self.config.mouthpiece_pipe_height) \
                .line(self.config.mouthpiece_pipe_width, 0) \
                .line(0, self.config.mouthpiece_pipe_height)
            if pipe_num < self.config.num_pipes - 1:
                sketch = sketch.line(self.config.mouthpiece_inner_border, 0)
        sketch.line(self.config.pipe_notch_width, 0) \
            .line(0, self.config.pipe_notch_height)  \
            .line(self.config.mouthpiece_outer_border - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.mouthpiece_height()) \
            .close()
        return sketch

    def build(self):
        position = 0
        workplane = cq.Workplane("YZ", origin=(0, 0))

        soundpipe_lower = self._build_pipe_soundpipe_sketch(workplane.workplane(), position=position)
        position += self.config.soundpipe_length
        soundpipe_upper = self._build_pipe_soundpipe_sketch(
            soundpipe_lower.workplane(offset=self.config.soundpipe_length, origin=(0, 0)), position=position)

        # TODO: without this very specific offset of 0.5 rendering fails with "BRep_API: command not done". Why?
        air_hole_lower = self._build_pipe_air_hole_sketch(
            soundpipe_upper.workplane(origin=(0, 0), offset=0.4), position=position)
        position += self.config.air_hole_length
        air_hole_upper = self._build_pipe_air_hole_sketch(
            air_hole_lower.workplane(offset=self.config.air_hole_length, origin=(0, 0)), position=position)

        # TODO: without an offset >= 0.001 rendering fails with "BRep_API: command not done". Why?
        block_lower = self._build_pipe_body_block_sketch(
            air_hole_upper.workplane(origin=(0, 0), offset=0.001), position=position)
        position += self.config.wind_channel_length
        block_upper = self._build_pipe_body_block_sketch(
            block_lower.workplane(offset=self.config.wind_channel_length, origin=(0, 0)), position=position)

        block_ramp_lower = self._build_pipe_body_block_sketch(
            block_upper.workplane(origin=(0, 0)), position=position)
        position += self.config.wind_channel_ramp_length
        block_ramp_upper = self._build_pipe_slow_airchamber_sketch(
            block_ramp_lower.workplane(offset=self.config.wind_channel_ramp_length, origin=(0, 0)), position=position)

        slow_airchamber_lower = self._build_pipe_slow_airchamber_sketch(
            block_ramp_upper.workplane(origin=(0, 0)), position=position)
        position += self.config.slow_airchamber_length
        slow_airchamber_upper = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_lower.workplane(offset=self.config.slow_airchamber_length, origin=(0, 0)), position=position)

        mouthpiece_lower = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_upper.workplane(origin=(0, 0)), position=position)
        mouthpiece_middle = self._build_mouth_piece(
            mouthpiece_lower.workplane(
                offset=self.config.mouthpiece_length / 2,
                origin=(0, 0)
            ))
        mouthpiece_middle2 = self._build_mouth_piece(
            mouthpiece_middle.workplane(origin=(0, 0)))
        mouthpiece_upper = self._build_mouth_piece(
            mouthpiece_middle2.workplane(offset=self.config.mouthpiece_length / 2, origin=(0, 0)))
        flute_body = mouthpiece_upper.loft(ruled=True)

        return flute_body


def main():
    config = FluteConfig()
    body = FluteBody(config, 1)
    return body.build()


result = main()
