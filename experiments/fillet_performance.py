import cadquery as cq

ENABLE_FILLETS = True

import cProfile
import re


#def log(msg):
#    print(msg)


#def debuglog(val):
#    log(val)
#    return val


class FluteConfig:
    num_pipes = 4
    pipe_length = 950
    pipe_width_at_bottom = 25
    pipe_width_at_top = 20
    pipe_height_at_bottom = 35
    pipe_height_at_top = 20
    pipe_border = 7.6
    pipe_notch_width = 2.5
    pipe_notch_height = 5
    soundpipe_length = 850
    air_hole_length = 10
    wind_channel_height = 2
    wind_channel_length = 20
    wind_channel_ramp_length = 15
    mouthpiece_pipe_height = 6
    mouthpiece_pipe_width = 6
    mouthpiece_length = 50
    mouthpiece_outer_border = 6.5
    mouthpiece_inner_border = 2
    mouthpiece_bottom_border = 3
    fillets = ENABLE_FILLETS
    arc_point_fillets = fillets  # True
    pipe_fillet_radius = 5 if arc_point_fillets else 0
    mouthpiece_fillet_radius = 2 if arc_point_fillets else 0
    mouthpiece_front_fillet_radius = 0.9 if arc_point_fillets else 0

    # def __init__(self):
    #     log("config:")
    #     log(f"width bottom: {self.body_width_at_position(0)}")
    #     log(f"height bottom: {self.body_height_at_position(0)}")
    #     log(f"height top: {self.body_height_at_position(self.pipe_length)}")
    #     log(f"width mouthpiece: {self.mouthpiece_width()}")
    #     log(f"length: {self.total_length()}")

    @property
    def slow_airchamber_length(self):
        return self.pipe_length - self.soundpipe_length - self.air_hole_length - self.wind_channel_length\
               - self.wind_channel_ramp_length

    def pipe_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_bottom\
               - (self.pipe_height_at_bottom - self.pipe_height_at_top) * position / self.pipe_length

    def pipe_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_bottom\
                - (self.pipe_width_at_bottom - self.pipe_width_at_top) * position / self.pipe_length

    def body_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_position(position) * self.num_pipes + self.pipe_border * (self.num_pipes + 1)

    def body_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_position(position) + self.pipe_border + self.pipe_notch_height

    def mouthpiece_width(self) -> float:
        return self.mouthpiece_pipe_width * self.num_pipes + self.mouthpiece_outer_border * 2 + self.mouthpiece_inner_border * (self.num_pipes - 1)

    def mouthpiece_height(self) -> float:
        return self.mouthpiece_pipe_height + self.mouthpiece_bottom_border + self.pipe_notch_height

    def total_length(self):
        return self.pipe_length + self.mouthpiece_length


class FluteBody:
    def __init__(self, config: FluteConfig):
        self.config = config

    def _build_pipe_sketch(self, workplane: cq.Workplane, position: int, notch_height: float, bottom_height: float):
        w = workplane.center(-self.config.body_width_at_position(position) / 2,
                             -self.config.body_height_at_position(position) + self.config.pipe_fillet_radius)
        w = w.line(0, self.config.body_height_at_position(position) - 2 * self.config.pipe_fillet_radius)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((self.config.pipe_fillet_radius, self.config.pipe_fillet_radius))
        w = w.line(self.config.pipe_border - self.config.pipe_notch_width - self.config.pipe_fillet_radius, 0)
        w = w.line(0, -notch_height)
        w = w.line(self.config.pipe_notch_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            w = w.line(0, -bottom_height)
            w = w.line(self.config.pipe_width_at_position(position), 0)
            w = w.line(0, bottom_height)
            if pipe_num < self.config.num_pipes - 1:
                w = w.line(self.config.pipe_border, 0)
        w = w.line(self.config.pipe_notch_width, 0)
        w = w.line(0, notch_height)
        w = w.line(self.config.pipe_border - self.config.pipe_notch_width - self.config.pipe_fillet_radius, 0)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((self.config.pipe_fillet_radius, -self.config.pipe_fillet_radius))
        w = w.line(0, -self.config.body_height_at_position(position) + 2 * self.config.pipe_fillet_radius)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((-self.config.pipe_fillet_radius, -self.config.pipe_fillet_radius))
        w = w.line(-self.config.body_width_at_position(position) + 2*self.config.pipe_fillet_radius, 0)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((-self.config.pipe_fillet_radius, self.config.pipe_fillet_radius))
        w = w.close()
        return w

    def _build_pipe_soundpipe_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height + self.config.wind_channel_height / 2,
                                       self.config.pipe_height_at_position(position) - self.config.wind_channel_height / 2)

    def _build_pipe_air_hole_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.pipe_height_at_position(position))

    def _build_pipe_body_block_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.wind_channel_height)

    def _build_pipe_slow_airchamber_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.pipe_height_at_position(
                                           position))

    def _build_mouth_piece(self, workplane: cq.Workplane) -> cq.Workplane:
        w = workplane.center(-self.config.mouthpiece_width()/2,
                             -self.config.mouthpiece_height() + self.config.mouthpiece_fillet_radius)
        w = w.line(0, self.config.mouthpiece_height() - 2 * self.config.mouthpiece_fillet_radius)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((self.config.mouthpiece_fillet_radius, self.config.mouthpiece_fillet_radius))
        w = w.line(self.config.mouthpiece_outer_border - self.config.pipe_notch_width - self.config.mouthpiece_fillet_radius, 0)
        w = w.line(0, -self.config.pipe_notch_height)
        w = w.line(self.config.pipe_notch_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            w = w.line(0, -self.config.mouthpiece_pipe_height)
            w = w.line(self.config.mouthpiece_pipe_width, 0)
            w = w.line(0, self.config.mouthpiece_pipe_height)
            if pipe_num < self.config.num_pipes - 1:
                w = w.line(self.config.mouthpiece_inner_border, 0)
        w = w.line(self.config.pipe_notch_width, 0)
        w = w.line(0, self.config.pipe_notch_height)
        w = w.line(self.config.mouthpiece_outer_border - self.config.pipe_notch_width - self.config.mouthpiece_fillet_radius, 0)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((self.config.mouthpiece_fillet_radius, -self.config.mouthpiece_fillet_radius))
        w = w.line(0, -self.config.mouthpiece_height() + 2 * self.config.mouthpiece_fillet_radius)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((-self.config.mouthpiece_fillet_radius, -self.config.mouthpiece_fillet_radius))
        w = w.line(-self.config.mouthpiece_width() + 2 * self.config.mouthpiece_fillet_radius, 0)
        if self.config.arc_point_fillets:
            w = w.tangentArcPoint((-self.config.mouthpiece_fillet_radius, self.config.mouthpiece_fillet_radius))
        w = w.close()
        return w

    def build(self):
        position = 0
        workplane = cq.Workplane("YZ", origin=(0, 0))

        soundpipe_lower = self._build_pipe_soundpipe_sketch(workplane.workplane(), position=position)
        position += self.config.soundpipe_length
        soundpipe_upper = self._build_pipe_soundpipe_sketch(
            soundpipe_lower.workplane(offset=self.config.soundpipe_length, origin=(0, 0)), position=position)
        sound_pipe = soundpipe_upper.loft()

        air_hole_lower = self._build_pipe_air_hole_sketch(
            soundpipe_upper.workplane(origin=(0, 0)), position=position)
        position += self.config.air_hole_length
        air_hole_upper = self._build_pipe_air_hole_sketch(
            air_hole_lower.workplane(offset=self.config.air_hole_length, origin=(0, 0)), position=position)
        air_hole = air_hole_upper.loft()

        block_lower = self._build_pipe_body_block_sketch(
            air_hole_upper.workplane(origin=(0, 0)), position=position)
        position += self.config.wind_channel_length
        block_upper = self._build_pipe_body_block_sketch(
            block_lower.workplane(offset=self.config.wind_channel_length, origin=(0, 0)), position=position)
        block = block_upper.loft()

        block_ramp_lower = self._build_pipe_body_block_sketch(
            block_upper.workplane(origin=(0, 0)), position=position)
        position += self.config.wind_channel_ramp_length
        block_ramp_upper = self._build_pipe_slow_airchamber_sketch(
            block_ramp_lower.workplane(offset=self.config.wind_channel_ramp_length, origin=(0, 0)), position=position)
        block_ramp = block_ramp_upper.loft()

        slow_airchamber_lower = self._build_pipe_slow_airchamber_sketch(
            block_ramp_upper.workplane(origin=(0, 0)), position=position)
        position += self.config.slow_airchamber_length
        slow_airchamber_upper = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_lower.workplane(offset=self.config.slow_airchamber_length, origin=(0, 0)), position=position)
        slow_airchamber = slow_airchamber_upper.loft()

        mouthpiece_lower = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_upper.workplane(origin=(0, 0)), position=position)
        mouthpiece_middle = self._build_mouth_piece(
            mouthpiece_lower.workplane(
                offset=self.config.mouthpiece_length / 2,
                origin=(0, 0)
            ))
        mouthpiece1 = mouthpiece_middle.loft()

        mouthpiece_middle2 = self._build_mouth_piece(
            mouthpiece_middle.workplane(origin=(0, 0)))
        mouthpiece_upper = self._build_mouth_piece(
            mouthpiece_middle2.workplane(offset=self.config.mouthpiece_length / 2, origin=(0, 0)))
        mouthpiece2 = mouthpiece_upper.loft()

        flute_body = sound_pipe.union(
            air_hole.union(
                block.union(
                    block_ramp.union(
                        slow_airchamber.union(
                            mouthpiece1.union(
                                mouthpiece2
                            )
                        )
                    )
                )
            )
        )
        self.config.pipe_fillet_radius = 3

        if self.config.fillets:
            # body back fillets
            flute_body = flute_body.edges('(<X and (<Z or <Y or >Y))').fillet(self.config.pipe_fillet_radius)

            # mouthpiece outer fillets
            flute_body = flute_body \
                .edges(cq.selectors.NearestToPointSelector(
                    (self.config.pipe_length + self.config.mouthpiece_length, 0, -self.config.mouthpiece_height())
                )).fillet(self.config.mouthpiece_fillet_radius)

            # moutpiece front fillets
            flute_body = flute_body.edges(cq.selectors.BoxSelector(
                (self.config.total_length() - 2, -self.config.mouthpiece_width() / 2 + 5, -self.config.pipe_notch_height-2),
                (self.config.total_length() + 2,self.config.mouthpiece_width() / 2 - 5,-self.config.mouthpiece_height() - 5),boundingbox=False
            )).fillet(self.config.mouthpiece_front_fillet_radius)

            # body front fillets
            flute_body = flute_body.edges(cq.selectors.BoxSelector(
                (self.config.pipe_length - 2,
                 -self.config.body_width_at_position(self.config.pipe_length - 2)/2 - 2,
                 2),
                (self.config.pipe_length + 2,
                 self.config.body_width_at_position(self.config.pipe_length - 2)/2 + 2,
                 -self.config.body_height_at_position(self.config.pipe_length - 2) - 2)
            )).fillet(2 / self.config.num_pipes)

            # mouthpiece middle fillet
            flute_body = flute_body.edges(cq.selectors.BoxSelector(
                (self.config.pipe_length + self.config.mouthpiece_length / 2 - 2,
                 -self.config.mouthpiece_width()/2 - 2,
                 2),
                (self.config.pipe_length + self.config.mouthpiece_length / 2 + 2,
                 self.config.mouthpiece_width()/2 + 2,
                 -self.config.mouthpiece_height() - 2)
            )).fillet(2.5)

            #block fillets
            flute_body = flute_body.edges((cq.selectors.BoxSelector(
                (self.config.soundpipe_length - 2,
                 -self.config.body_width_at_position(self.config.soundpipe_length - 2) / 2 - 2,
                 -6),
                (self.config.soundpipe_length + self.config.air_hole_length + self.config.wind_channel_length
                 + self.config.wind_channel_ramp_length + 2,
                 self.config.body_width_at_position(self.config.soundpipe_length - 2) / 2 + 2,
                 -self.config.body_height_at_position(self.config.soundpipe_length - 2) + 2)
            ))).fillet(1.5)

        return flute_body


def main():
    config = FluteConfig()
    body = FluteBody(config)
    result = body.build()
    #cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    return result

#cProfile.run('main()', sort="tottime")

obj = main()