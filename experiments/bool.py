import cadquery as cq

a1 = cq.Workplane("XY").rect(10, 10).extrude(10)
a2 = cq.Workplane("XY").workplane(offset = 10).rect(5, 5).extrude(5)
a = a1.add(a2)

b1 = cq.Workplane("XY").center(2,0).rect(2,2).extrude(5)
b2 = cq.Workplane("XY").workplane(offset=5).center(2,0).rect(1,1).extrude(20)
b = b1.add(b2)

c = a.cut(b)

cq.exporters.export(c, "../polymoe.step", cq.exporters.ExportTypes.STEP)