import cadquery as cq

# This throws a "OCP.StdFail.StdFail_NotDone: BRep_API: command not done" error in CadQuery 2.2
# it works one does one of the changes mentioned in comments 1 to 5

def sketch(workplane = None) -> cq.Workplane:
    if workplane is None:
        workplane = cq.Workplane("XZ").workplane()
    obj = (workplane
        .line(10, 0)
        .line(0, -1) # 1. comment either this...
        .tangentArcPoint((5, -5))
        .line(10, 0.01) # 2. ...replace 0,01 with 0 here...
        .line(0, -20)
        .close())
    return obj

def build() -> cq.Workplane:
    use_extrude = False # 3. ...set this to True...

    sketch_left = sketch()
    if use_extrude:
        obj = sketch_left.extrude(5)
    else:
        sketch_right = sketch(sketch_left.transformed(rotate=(0,5.0,0)).workplane(offset=20)) # 4. change the angle to rotate=(0,0.0,0)
        obj = sketch_right.loft(ruled=True)
    #obj = obj.edges().fillet(0.1) # 5. ...or comment that
    return obj

obj = build()

cq.exporters.export(obj, "polymoe.step", cq.exporters.ExportTypes.STEP)