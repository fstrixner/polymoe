from OCP.gp import gp_Pnt
from cadquery import *

from OCP.GeomAbs import GeomAbs_Shape
from OCP.BRepOffsetAPI import BRepOffsetAPI_MakeFilling
from OCP.BRepAdaptor import BRepAdaptor_Curve
from OCP.GCPnts import GCPnts_AbscissaPoint, GCPnts_UniformAbscissa

makeFilling=True
#makeFilling=False

box_1 = Solid.makeBox(length=10, width=10, height=10, pnt=Vector(-5, -5, 0), dir=Vector(0,0,1))
box_2 = Solid.makeBox(length=30, width=40, height=10, pnt=Vector(-15, -20, 20), dir=Vector(0,0,1))

wire_1 = StringSyntaxSelector(">Z").filter(box_1.Wires())[0]
wire_2 = StringSyntaxSelector("<Z").filter(box_2.Wires())[0]

loft = Solid.makeLoft([wire_1, wire_2])

continuity = GeomAbs_Shape.GeomAbs_G1
zero_cont = GeomAbs_Shape.GeomAbs_C0

considered_faces = []

if makeFilling:
    n_sided = BRepOffsetAPI_MakeFilling(
        Degree=3, NbPtsOnCur=20, NbIter=2, Anisotropie=False,
        Tol2d=0.0001, Tol3d=0.0001, TolAng=0.01, TolCurv=0.01, MaxDeg=8, MaxSegments=240
        )
    added_edges = []
    for loft_face in StringSyntaxSelector(">X or <X or >Y").filter(loft.Faces()):
        for loft_edge in StringSyntaxSelector("|X or |Y or <Y").filter(loft_face.Edges()):
            adjacent_surface = None
            for adjacent_solid in [box_1, box_2]:
                for solid_face in StringSyntaxSelector("not(>Z or <Z)").filter(adjacent_solid.Faces()):
                    for solid_edge in solid_face.Edges():
                        if solid_edge == loft_edge:
                            adjacent_surface = solid_face
                            break
                    if adjacent_surface is not None:
                        break
                if adjacent_surface is not None:
                    break
            if loft_edge not in added_edges:
                if adjacent_surface is not None:
                    considered_faces.append(adjacent_surface)
                    n_sided.Add(Constr=loft_edge.wrapped, Support=adjacent_surface.wrapped, Order=continuity)
                else:
                    n_sided.Add(Constr=loft_edge.wrapped, Order=zero_cont)
                added_edges.append(loft_edge)
    n_sided.Build()
    shape = n_sided.Shape()
    transformed_loft = Face(shape)#.fix()

workplane = (Workplane()
             #.add(box_1)
             #.add(box_2)
)
if makeFilling:
    workplane = workplane.add(transformed_loft)
else:
    workplane = workplane.add(loft)

exporters.export(workplane, "output.step", exporters.ExportTypes.STEP)