# SINGLEPY_IGNORE_BEGIN
from enum import Enum
from typing import List

import cadquery as cq

from polymoe_body import FluteBodyConfig, FluteBody
from polymoe_config import *
# SINGLEPY_IGNORE_END
from polymoe_cover import Cover, CoverConfig

'''
2222
1  3
   3
   3444
'''
constants_curvedmouthpiece_length1 = 50 # from start to highest point
constants_curvedmouthpiece_length2 = 80 # from highest point sideways
constants_curvedmouthpiece_length3 = 100 # downwards
constants_curvedmouthpiece_length4 = 50 # towards the player

class CurvedMouthpieceConfig:
    def __init__(self, body_config: FluteBodyConfig, cover_config: CoverConfig):
        self.density = constants_curvedmouthpiece_density
        self.body_config = body_config
        self.cover_config = cover_config
        self.start_pos = cq.Vector(body_config.total_length, 0, 0)
        self.mouthpiece_length1 = constants_curvedmouthpiece_length1
        self.mouthpiece_length2 = constants_curvedmouthpiece_length2
        self.mouthpiece_length3 = constants_curvedmouthpiece_length3
        self.mouthpiece_length4 = constants_curvedmouthpiece_length4
        self.mouthpiece_half_height = (body_config.mouthpiece_height + body_config.mouthpiece_outer_border + cover_config.height) / 2


class CurvedMouthpiece:
    class _Shape(Enum):
        body = 0
        cover = 1

    def __init__(self, config: CurvedMouthpieceConfig):
        self.config = config
        self._built = None
        self.body = FluteBody(self.config.body_config)
        self.cover = Cover(self.config.cover_config)


    def _build_sketch(self, workplane: cq.Workplane, shape: _Shape) -> cq.Workplane:
        return workplane.rect(20,10)

    def _build_mouthpiece_part(self, shape: _Shape) -> cq.Workplane:
        w = cq.Workplane("YZ").workplane(offset=self.config.start_pos.x)
        w = self._build_sketch(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length1 - self.config.mouthpiece_half_height)
        w = self._build_sketch(w, shape=shape)
        w = w.workplane(origin=(0,0), offset=self.config.mouthpiece_half_height / 2).transformed(rotate=cq.Vector(90, 0, 0)).workplane(offset=self.config.mouthpiece_half_height / 2)
        w = self._build_sketch(w, shape=shape)
        # w = w.workplane(offset=self.config.mouthpiece_length2 - self.config.mouthpiece_half_width * 2)
        # w = self._build_sketch(w, shape=shape)
        # w = w.workplane().transformed(offset=cq.Vector(self.config.mouthpiece_half_width, -self.config.mouthpiece_half_width, 0), rotate=cq.Vector(0, 0, 90))
        # w = self._build_sketch(w, shape=shape)
        # w = w.workplane(offset=self.config.mouthpiece_length3)
        # w = self._build_sketch(w, shape=shape)
        # w = w.workplane().transformed(offset=cq.Vector(self.config.mouthpiece_half_width, -self.config.mouthpiece_half_width, 0), rotate=cq.Vector(0, 0, -90))
        # w = self._build_sketch(w, shape=shape)
        # w = w.workplane(offset=self.config.mouthpiece_length4)
        # w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)

        return w

    def build(self) -> List[cq.Workplane]:
        body = self._build_mouthpiece_part(shape=CurvedMouthpiece._Shape.body)
        cover = self._build_mouthpiece_part(shape=CurvedMouthpiece._Shape.cover)
        self._built = [body, cover]
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# SINGLEPY_IGNORE_BEGIN
def main() -> cq.Assembly:
    assembly = cq.Assembly()

    body_config = FluteBodyConfig()
    cover_config = CoverConfig(body_config)
    curved_mouthpiece_config = CurvedMouthpieceConfig(body_config, cover_config)
    curved_mouthpiece = CurvedMouthpiece(curved_mouthpiece_config)
    curved_mouthpiece_halfpipe_objs = curved_mouthpiece.build()
    assembly.add(curved_mouthpiece_halfpipe_objs[0])
    assembly.add(curved_mouthpiece_halfpipe_objs[1])

    print(f"volume curved mouthpiece: {curved_mouthpiece.volume():.1f} cm^3")
    print(f"weight curved mouthpiece: {curved_mouthpiece.weight():.1f} g")

    return assembly


if __name__ == '__main__':
    result = main()
    result.save("../polymoe.step")
# SINGLEPY_IGNORE_END