import cadquery as cq
from OCP.GeomAbs import GeomAbs_G1, GeomAbs_G2
from cadquery import Solid


def rounded_rect(self: cq.Workplane, width: float, height: float, fillet_radius: float):
    return self.center(-width / 2, -height / 2 + fillet_radius) \
        .line(0, height - 2 * fillet_radius) \
        .tangentArcPoint((fillet_radius, fillet_radius)) \
        .line(width - 2 * fillet_radius, 0) \
        .tangentArcPoint((fillet_radius, -fillet_radius)) \
        .line(0, -height + 2 * fillet_radius) \
        .tangentArcPoint((-fillet_radius, -fillet_radius)) \
        .line(-width + 2 * fillet_radius, 0) \
        .tangentArcPoint((-fillet_radius, fillet_radius)) \
        .close() \
        .center(width / 2, height / 2 - fillet_radius)


cq.Workplane.rounded_rect = rounded_rect

fillet_radius_xy = 2
fillet_radius_z = 5


obj1 = cq.Workplane("XY") \
        .rounded_rect(10, 10, fillet_radius_xy) \
        .workplane(offset=20 - fillet_radius_z) \
        .rounded_rect(10, 10, fillet_radius_xy).loft()
obj2 = cq.Workplane("XY").workplane(offset=20).rounded_rect(20, 20, fillet_radius_xy) \
    .workplane(offset=20 - fillet_radius_z) \
    .rounded_rect(20, 20, fillet_radius_xy).loft()
#edges = obj1.edges(">Z and >X").vals() + list(reversed(obj2.edges("<Z and >X").vals()))
#vertices = obj1.vertices(">Z and >X").vals() + obj2.vertices("<Z and >X").vals()
#vertices = [vertex.toTuple() for vertex in vertices]
#faces = obj1.faces(">X").vals() + obj2.faces(">X").vals()
#fillet_1_2 = cq.Face.makeNSidedSurface(edges,points=[], tangentialFaces=faces,tangentialFaceContinuity=GeomAbs_G1)
wires = []
obj1.wires(">Z").each(lambda w: wires.append(w))
obj2.wires("<Z").each(lambda w: wires.append(w))
fillet_1_2 = Solid.makeLoft(wires)
obj3 = cq.Workplane("XY").workplane(offset=40).rounded_rect(30, 30, fillet_radius_xy) \
     .workplane(offset=20 - fillet_radius_z) \
     .rounded_rect(30, 30, fillet_radius_xy).loft()
obj4 = cq.Workplane("XY").workplane(offset=60).circle(30) \
     .workplane(offset=20 - fillet_radius_z) \
     .circle(30).loft()

obj = obj1.add(
    fillet_1_2).add(
        obj2.add(
            obj3.add(
                    obj4)))

cq.exporters.export(obj, "../polymoe.step", cq.exporters.ExportTypes.STEP)
