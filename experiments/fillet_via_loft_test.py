import cadquery as cq


def rounded_rect(self: cq.Workplane, width: float, height: float, fillet_radius: float):
    return self.center(-width / 2, -height / 2 + fillet_radius) \
        .line(0, height - 2 * fillet_radius) \
        .tangentArcPoint((fillet_radius, fillet_radius)) \
        .line(width - 2 * fillet_radius, 0) \
        .tangentArcPoint((fillet_radius, -fillet_radius)) \
        .line(0, -height + 2 * fillet_radius) \
        .tangentArcPoint((-fillet_radius, -fillet_radius)) \
        .line(-width + 2 * fillet_radius, 0) \
        .tangentArcPoint((-fillet_radius, fillet_radius)) \
        .close() \
        .center(width / 2, height / 2 - fillet_radius)

def smooth_loft(workplane, func1, func1_params, func2, func2_params, offset):
    w = workplane.faces(">Z").workplane(offset=0)
    w = func1(self=w, **func1_params)
    w = w.workplane(offset=offset/4)
    w = func1(self=w, **func1_params)
    w = w.workplane(offset=offset/2)
    w = func2(self=w, **func2_params)
    w = w.workplane(offset=offset/4)
    w = func2(self=w, **func2_params)
    return w.loft(ruled=False)

cq.Workplane.rounded_rect = rounded_rect

fillet_radius_xy = 2
fillet_radius_z = 5


obj1 = cq.Workplane("XY") \
        .rounded_rect(10, 10, fillet_radius_xy) \
        .workplane(offset=20 - fillet_radius_z) \
        .rounded_rect(10, 10, fillet_radius_xy).loft()
fillet_1_2 = smooth_loft(obj1, rounded_rect, {'width':10, 'height':10, 'fillet_radius':fillet_radius_xy},
                         rounded_rect, {'width':20, 'height':20, 'fillet_radius':fillet_radius_xy}, fillet_radius_z)
obj2 = obj1.faces(">Z").workplane(offset=fillet_radius_z).rounded_rect(20, 20, fillet_radius_xy) \
    .workplane(offset=20 - fillet_radius_z) \
    .rounded_rect(20, 20, fillet_radius_xy).loft()
fillet_2_3 = smooth_loft(obj2, rounded_rect, {'width':20, 'height':20, 'fillet_radius':fillet_radius_xy},
                         rounded_rect, {'width':30, 'height':30, 'fillet_radius':fillet_radius_xy}, fillet_radius_z)
obj3 = obj2.faces(">Z").workplane(offset=fillet_radius_z).rounded_rect(30, 30, fillet_radius_xy) \
     .workplane(offset=20 - fillet_radius_z) \
     .rounded_rect(30, 30, fillet_radius_xy).loft()
fillet_3_4 = smooth_loft(obj3, rounded_rect, {'width':30, 'height':30, 'fillet_radius':fillet_radius_xy},
                         cq.Workplane.circle, {'radius':30}, fillet_radius_z)
obj4 = obj3.faces(">Z").workplane(offset=fillet_radius_z).circle(30) \
     .workplane(offset=20 - fillet_radius_z) \
     .circle(30).loft()

obj = obj1.union(
    fillet_1_2.union(
        obj2.union(
            fillet_2_3.union(
                obj3.union(
                    fillet_3_4.union(
                        obj4))))))

cq.exporters.export(obj, "../polymoe.step", cq.exporters.ExportTypes.STEP)
