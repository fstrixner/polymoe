import cadquery as cq


def log(msg):
    print(msg)


def debuglog(val):
    log(val)
    return val


class FluteConfig:
    num_pipes = 4
    pipe_length = 950
    pipe_width_at_bottom = 25
    pipe_width_at_top = 20
    pipe_height_at_bottom = 35
    pipe_height_at_top = 20
    pipe_border = 10
    pipe_notch_width = 2.5
    pipe_notch_height = 5
    soundpipe_length = 850
    air_hole_length = 10
    wind_channel_height = 2
    wind_channel_length = 20
    wind_channel_ramp_length = 15
    mouthpiece_pipe_height = 6
    mouthpiece_pipe_width = 6
    mouthpiece_length = 50
    mouthpiece_outer_border = 6.5
    mouthpiece_inner_border = 2
    mouthpiece_bottom_border = 3

    def pipe_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_bottom\
               - (self.pipe_height_at_bottom - self.pipe_height_at_top) * position / self.pipe_length

    def pipe_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_bottom\
                - (self.pipe_width_at_bottom - self.pipe_width_at_top) * position / self.pipe_length

    def body_width_at_position(self, position: int) -> float:
        return self.pipe_width_at_position(position) * self.num_pipes + self.pipe_border * (self.num_pipes + 1)

    def body_height_at_position(self, position: int) -> float:
        return self.pipe_height_at_position(position) + self.pipe_border + self.pipe_notch_height


class FluteBody:
    def __init__(self, config: FluteConfig, pipe_index: int):
        self.config = config
        self.pipe_index = pipe_index

    def _build_pipe_sketch(self, workplane: cq.Workplane, position: int, notch_height: float, bottom_height: float):
        sketch = workplane \
            .center(-self.config.body_width_at_position(position) / 2,
                    -self.config.body_height_at_position(position)) \
            .line(0, self.config.body_height_at_position(position)) \
            .line(self.config.pipe_border - self.config.pipe_notch_width, 0) \
            .line(0, -notch_height) \
            .line(self.config.pipe_notch_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            sketch = sketch.line(0, -bottom_height) \
                .line(self.config.pipe_width_at_position(position), 0) \
                .line(0, bottom_height)
            if pipe_num < self.config.num_pipes - 1:
                sketch = sketch.line(self.config.pipe_border, 0)
        sketch = sketch.line(self.config.pipe_notch_width, 0) \
            .line(0, notch_height) \
            .line(self.config.pipe_border - self.config.pipe_notch_width, 0) \
            .line(0, -self.config.body_height_at_position(position)) \
            .close()
        return sketch

    def _build_pipe_soundpipe_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height + self.config.wind_channel_height / 2,
                                       self.config.pipe_height_at_position(position) - self.config.wind_channel_height / 2)

    def _build_pipe_air_hole_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.pipe_height_at_position(position))

    def _build_pipe_body_block_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       self.config.pipe_notch_height,
                                       self.config.wind_channel_height)

    def build(self):
        position = 0
        workplane = cq.Workplane("YZ", origin=(0, 0))

        # Here offset 0 works
        air_hole_lower = self._build_pipe_air_hole_sketch(
            workplane.workplane(origin=(0, 0), offset=0.0), position=position)
        position += self.config.air_hole_length
        air_hole_upper = self._build_pipe_air_hole_sketch(
            air_hole_lower.workplane(offset=self.config.air_hole_length, origin=(0, 0)), position=position)

        # Here offset 0 works
        block_lower = self._build_pipe_body_block_sketch(
            air_hole_upper.workplane(origin=(0, 0), offset=0.0), position=position)
        position += self.config.wind_channel_length
        block_upper = self._build_pipe_body_block_sketch(
            block_lower.workplane(offset=self.config.wind_channel_length, origin=(0, 0)), position=position)


        flute_body = block_upper.loft(ruled=True)

        return flute_body


def main():
    config = FluteConfig()
    body = FluteBody(config, 1)
    return body.build()


result = main()
