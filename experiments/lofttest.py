import cadquery as cq

obj = cq.Workplane("XZ") \
        .rect(10, 10) \
        .workplane(offset=20) \
        .rect(10, 10) \
        .workplane() \
        .rect(20, 20) \
        .workplane(offset=20) \
        .rect(20, 20) \
        .workplane() \
        .circle(30, ) \
        .workplane(offset=20) \
        .circle(30) \
        .loft(ruled=True)
