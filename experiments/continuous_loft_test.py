from cadquery import *

def continuous_connection(start_wire: Wire, end_wire: Wire, offset1: float = 0.0, offset2: float = 0.0):
    middle_wire_1 = start_wire.offset2D(offset1, kind="tangent")[0].translate(start_wire.normal()*0.01)
    middle_wire_2 = end_wire.offset2D(offset2, kind="tangent")[0].translate(end_wire.normal()*-0.01)

    loft = Solid.makeLoft([start_wire, middle_wire_1, middle_wire_2, end_wire], ruled=False)
    return loft


def continuous_endpiece(wire: Wire, fillet_radius: float, offset: float = 0.0):
    middle_wire_1 = wire.offset2D(offset, kind="tangent")[0].translate(wire.normal()*0.01)
    middle_wire_2 = wire.offset2D(-fillet_radius + 0.01, kind="tangent")[0].translate(wire.normal() * fillet_radius)
    upper_wire = wire.offset2D(-fillet_radius, kind="tangent")[0].translate(wire.normal() * fillet_radius)
    loft = Solid.makeLoft([wire, middle_wire_1, middle_wire_2, upper_wire], ruled=False)
    return loft

box_1 = Solid.makeBox(length=200, width=200, height=10, pnt=Vector(-100, -100, 0), dir=Vector(0, 0, 1)).rotate(Vector(1,0,0),Vector(1.2,0,0),15)
box_2 = Solid.makeBox(length=10, width=10, height=10, pnt=Vector(-5, -5, 60), dir=Vector(0, 0, 1))

wire_1 = StringSyntaxSelector(">>Z[3]").filter(box_1.Faces())[0].outerWire()
wire_2 = StringSyntaxSelector("<Z").filter(box_2.Faces())[0].outerWire()

loft = continuous_connection(wire_1, wire_2)

top_wire = StringSyntaxSelector(">Z").filter(box_2.Faces())[0].outerWire()
endpiece = continuous_endpiece(top_wire, 2)

obj = Workplane().add(box_1).add(box_2).add(loft).add(endpiece)

exporters.export(obj, "../polymoe.step", exporters.ExportTypes.STEP)


