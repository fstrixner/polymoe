from OCP.gp import gp_Pnt
from cadquery import *

from OCP.GeomAbs import GeomAbs_Shape
from OCP.BRepOffsetAPI import BRepOffsetAPI_MakeFilling
from OCP.BRepAdaptor import BRepAdaptor_Curve
from OCP.GCPnts import GCPnts_AbscissaPoint, GCPnts_UniformAbscissa

makeFilling=True
#makeFilling=False

o = 20
i = 5
h = 20

surface_1 = Face.makeFromWires(Wire.makePolygon([Vector(-i, -i, h), Vector(-i,i,h),Vector(-o,i,0),Vector(-o,-i,0)]))
surface_2 = Face.makeFromWires(Wire.makePolygon([Vector(-5, -5, h), Vector(i,-i,h),Vector(i,-o,0),Vector(-i,-o,0)]))
surface_3 = Face.makeFromWires(Wire.makePolygon([Vector(5, -5, h), Vector(i,i,h),Vector(o,i,0),Vector(o,-i,0)]))
surface_4 = Face.makeFromWires(Wire.makePolygon([Vector(5, 5, h), Vector(-i,i,h),Vector(-i,o,0),Vector(i,o,0)]))

connection_edge_surface_1 = StringSyntaxSelector(">Z").filter(surface_1.Edges())[0]
connection_edge_surface_2 = StringSyntaxSelector(">Z").filter(surface_2.Edges())[0]
connection_edge_surface_3 = StringSyntaxSelector(">Z").filter(surface_3.Edges())[0]
connection_edge_surface_4 = StringSyntaxSelector(">Z").filter(surface_4.Edges())[0]

continuity = GeomAbs_Shape.GeomAbs_G1

n_sided = BRepOffsetAPI_MakeFilling(
    Degree=3, NbPtsOnCur=20, NbIter=4, Anisotropie=False,
    Tol2d=0.0001, Tol3d=0.0001, TolAng=0.01, TolCurv=0.01, MaxDeg=8, MaxSegments=240
    )
native = connection_edge_surface_1.wrapped
# The error message "BRepFill : The continuity is not G0 G1 or G2" for values outside that range doesn't fit, because G2 would be 3...
n_sided.Add(Constr=connection_edge_surface_1.wrapped, Support=surface_1.wrapped, Order=continuity, IsBound=True)
#n_sided.Add(connection_edge_surface_1.wrapped, Order=GeomAbs_Shape.GeomAbs_C0, IsBound=True)
n_sided.Add(Constr=connection_edge_surface_2.wrapped, Support=surface_2.wrapped, Order=continuity, IsBound=True)
#n_sided.Add(connection_edge_surface_2.wrapped, Order=GeomAbs_Shape.GeomAbs_C0, IsBound=True)
n_sided.Add(Constr=connection_edge_surface_3.wrapped, Support=surface_3.wrapped, Order=continuity, IsBound=True)
#n_sided.Add(connection_edge_surface_3.wrapped, Order=GeomAbs_Shape.GeomAbs_C0, IsBound=True)
n_sided.Add(Constr=connection_edge_surface_4.wrapped, Support=surface_4.wrapped, Order=continuity, IsBound=False)
#n_sided.Add(connection_edge_surface_4.wrapped, Order=GeomAbs_Shape.GeomAbs_C0, IsBound=False)

if makeFilling:
    n_sided.Build()
    face = n_sided.Shape()
    continuous_bridge_surface = Face(face).fix()

workplane = Workplane().add(surface_1).add(surface_2).add(surface_3).add(surface_4)
if makeFilling:
    workplane = workplane.add(continuous_bridge_surface)