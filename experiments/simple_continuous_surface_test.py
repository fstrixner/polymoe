from OCP.gp import gp_Pnt
from cadquery import *

from OCP.GeomAbs import GeomAbs_Shape
from OCP.BRepOffsetAPI import BRepOffsetAPI_MakeFilling

surface_1 = Face.makePlane(length=10, width=20, basePnt=(0, 0, 0), dir=(1, 0, -1))
surface_2 = Face.makePlane(length=10, width=20, basePnt=(20, 0, 0), dir=(1, 0, 1))

connection_edge_surface_1 = StringSyntaxSelector(">Z").filter(surface_1.Edges())[0]
connection_edge_surface_2 = StringSyntaxSelector(">Z").filter(surface_2.Edges())[0]
upper_vertex_left_1 = StringSyntaxSelector(">Z and <Y").filter(connection_edge_surface_1.Vertices())[0]
upper_vertex_right_1 = StringSyntaxSelector(">Z and >Y").filter(connection_edge_surface_1.Vertices())[0]
upper_vertex_left_2 = StringSyntaxSelector(">Z and <Y").filter(connection_edge_surface_2.Vertices())[0]
upper_vertex_right_2 = StringSyntaxSelector(">Z and >Y").filter(connection_edge_surface_2.Vertices())[0]

border_edge_1 = Edge.makeLine(Vector(upper_vertex_left_1.toTuple()), Vector(upper_vertex_left_2.toTuple()))
border_edge_2 = Edge.makeLine(Vector(upper_vertex_right_1.toTuple()), Vector(upper_vertex_right_2.toTuple()))


n_sided = BRepOffsetAPI_MakeFilling(
    Degree=3, NbPtsOnCur=15, NbIter=2, Anisotropie=False,
    Tol2d=0.00001, Tol3d=0.0001, TolAng=0.01, TolCurv=0.1, MaxDeg=8, MaxSegments=9)

# for some reason BRepFill_CurveConstraint restricts continuity to [-1;2].
# The error message "BRepFill : The continuity is not G0 G1 or G2" for values outside that range doesn't fit, because G2 would be 3...
# for C1 this gives some interesting landscapes... and for G1 it's just a flat plane. Needs further investigation...
# One next step could be to look into the code of the FreeCAD surface workbench, they apparently managed to get support
# for surface continuity up to G4
n_sided.Add(Constr=connection_edge_surface_1.wrapped, Support=surface_1.wrapped, Order=GeomAbs_Shape.GeomAbs_C1, IsBound=True)
n_sided.Add(Constr=border_edge_1.wrapped, Order=GeomAbs_Shape.GeomAbs_C0, IsBound=True)
n_sided.Add(Constr=connection_edge_surface_2.wrapped, Support=surface_2.wrapped, Order=GeomAbs_Shape.GeomAbs_C1, IsBound=True)
n_sided.Add(Constr=border_edge_2.wrapped, Order=GeomAbs_Shape.GeomAbs_C0, IsBound=True)
#n_sided.Add(gp_Pnt(10, 5, 25)) # trying to guide the surface by adding a support point

n_sided.Build()
face = n_sided.Shape()
continuous_bridge_surface = Face(face)#.fix()

workplane = Workplane().add(surface_1).add(continuous_bridge_surface).add(surface_2)

exporters.export(workplane, "../polymoe.step", exporters.ExportTypes.STEP)