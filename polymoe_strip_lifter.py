# SINGLEPY_IGNORE_BEGIN
import cadquery as cq
# SINGLEPY_IGNORE_END

constants_save_step = True
constants_show_object = False

constants_striplifter_density = 7.85  # g/cm^3 carbon steel
constants_striplifter_ring_outer_diameter = 15
constants_striplifter_ring_distance = 2
constants_striplifter_ring_width = 2
constants_striplifter_ring_length = 10
constants_striplifter_num_rings = 3
constants_striplifter_plate_thickness = 2

class StripLifterConfig:
    density = constants_striplifter_density
    ring_outer_diameter = constants_striplifter_ring_outer_diameter
    ring_distance = constants_striplifter_ring_distance
    ring_width = constants_striplifter_ring_width
    ring_length = constants_striplifter_ring_length
    num_rings = constants_striplifter_num_rings
    plate_thickness = constants_striplifter_plate_thickness


class StripLifter:
    def __init__(self, config: StripLifterConfig):
        self.config = config
        self._built = None

    def _build_plate_sketch(self) -> cq.Workplane:
        w = (cq.Workplane("XZ")
             .line((self.config.ring_distance + self.config.ring_outer_diameter) * (self.config.num_rings - 1), 0)
             .tangentArcPoint((0, self.config.ring_outer_diameter))
             .line(-(self.config.ring_distance + self.config.ring_outer_diameter) * (self.config.num_rings - 1), 0)
             .tangentArcPoint((0, -self.config.ring_outer_diameter))
             .close()
             .center(0, self.config.ring_outer_diameter / 2)
             .circle(self.config.ring_outer_diameter / 2 - self.config.ring_width))
        for _ in range(self.config.num_rings - 1):
            w = (w.center(self.config.ring_outer_diameter + self.config.ring_distance, 0)
                 .circle(self.config.ring_outer_diameter / 2 - self.config.ring_width))
        return w

    def _build_ring_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = (workplane
             .center(0, self.config.ring_outer_diameter / 2)
             .circle(self.config.ring_outer_diameter / 2 - self.config.ring_width)
             .circle(self.config.ring_outer_diameter / 2))
        for _ in range(self.config.num_rings - 1):
            w = (w.center(self.config.ring_outer_diameter + self.config.ring_distance, 0)
                 .circle(self.config.ring_outer_diameter / 2 - self.config.ring_width)
                 .circle(self.config.ring_outer_diameter / 2))
        return w

    def build(self) -> cq.Workplane:
        plate_sketch = self._build_plate_sketch()
        plate = plate_sketch.extrude(self.config.plate_thickness)
        rings = self._build_ring_sketch(cq.Workplane("XZ")).extrude(self.config.ring_length)
        self._built = plate.union(rings, clean=False)  # TODO: find out why clean doesn't work here
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main() -> cq.Workplane:
    striplifter_config = StripLifterConfig()
    striplifter = StripLifter(striplifter_config)
    striplifter_obj = striplifter.build()
    print(f"striplifter volume: {striplifter.volume():.1f} cm^3")
    print(f"striplifter weight: {striplifter.weight():.1f} g")

    return striplifter_obj


if __name__ == '__main__':
    result = main()
    if constants_save_step:
        cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    if constants_show_object:
        show_object(result)
# SINGLEPY_IGNORE_END