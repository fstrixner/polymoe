# SINGLEPY_IGNORE_BEGIN
from typing import List

import cadquery as cq
# SINGLEPY_IGNORE_END


def log(msg):
    print(msg)


def continuous_connection(start_wire: cq.Wire, end_wire: cq.Wire, invert_normals: bool = False,
                          middle_wires: List[cq.Wire] = None) -> cq.Solid:
    wires = []
    wires.append(start_wire)
    if middle_wires is None:
        wires.append(start_wire.translate(start_wire.normal() * 0.1 * (-1 if invert_normals else 1)))
        wires.append(end_wire.translate(end_wire.normal() * -0.1 * (-1 if invert_normals else 1)))
    else:
        for middle_wire in middle_wires:
            wires.append(middle_wire)
    wires.append(end_wire)

    loft = cq.Solid.makeLoft(wires, ruled=False)
    return loft


def continuous_endpiece(wire: cq.Wire, fillet_radius: float,
                        invert_normals: bool = False, offset: float = 0.0) -> cq.Solid:
    middle_wire_1 = wire.offset2D(offset, kind="tangent")[0].translate(wire.normal()*0.01*(-1 if invert_normals else 1))
    middle_wire_2 = wire.offset2D(-fillet_radius + 0.01, kind="tangent")[0].translate(
        wire.normal() * fillet_radius*(-1 if invert_normals else 1))
    upper_wire = wire.offset2D(-fillet_radius, kind="tangent")[0].translate(
        wire.normal() * fillet_radius*(-1 if invert_normals else 1))
    loft = cq.Solid.makeLoft([wire, middle_wire_1, middle_wire_2, upper_wire], ruled=False)
    return loft