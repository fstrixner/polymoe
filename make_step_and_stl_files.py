import os

import cadquery as cq

from make_polymoe_all import make_all
from polymoe_config import PolyMoeConfig


def export_polymoe(config_abbreviation: str, config_name):
    config = PolyMoeConfig(config_abbreviation)
    config.constants_save_step = False

    import polymoe_all
    polymoe_assembly = polymoe_all.main(config)

    polymoe_assembly.save(os.path.join('step_files', f"{config_name}.step"))
    for part in polymoe_assembly.children:
        compound = part.toCompound()
        compound.exportStep(os.path.join('part_files', f"{config_name}_{part.name}.step"))
        compound.exportStl(os.path.join('part_files', f"{config_name}_{part.name}.stl"))

def delete_step_files():
    for file in [file for file in os.listdir('step_files') if file.endswith('.step')]:
        os.remove(os.path.join('step_files', file))

def delete_stl_files():
    for file in [file for file in os.listdir('part_files') if file.endswith('.stl') or file.endswith(('.step'))]:
        os.remove(os.path.join('part_files', file))


def export_strip_lifter():
    from polymoe_strip_lifter import main as strip_lifter_main
    striplifter = strip_lifter_main()
    cq.exporters.export(striplifter, os.path.join('step_files', f"strip_lifter.step"), cq.exporters.ExportTypes.STEP)
    cq.exporters.export(striplifter, os.path.join('part_files', f"strip_lifter.stl"), cq.exporters.ExportTypes.STL)


if __name__ == '__main__':
    make_all() # ensure that polymoe_all.py is up to date
    delete_step_files()
    delete_stl_files()
    export_polymoe("L", "polymoe_large")
    export_polymoe("M", "polymoe_medium")
    export_polymoe("S", "polymoe_small")
    export_strip_lifter()