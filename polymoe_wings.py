# SINGLEPY_IGNORE_BEGIN
from typing import Optional

import cadquery as cq

from polymoe_body import FluteBodyConfig
from polymoe_config import *
# SINGLEPY_IGNORE_END

class WingsConfig:

    def __init__(self, flute_body_config: FluteBodyConfig):
        config = flute_body_config.config

        self.wing_height = config.wing_height
        self.wing_stopper_length = config.wing_stopper_length
        self.wing_ramp_length = config.wing_ramp_length
        self.wing_ramp_top_length = config.wing_ramp_top_length
        self.blocker_height = config.wing_blocker_height
        self.blocker_gap_length = config.wing_blocker_gap_length
        self.corner_radius = config.wing_corner_radius
        self.mount_depth = config.wing_mount_depth
        self.mount_length = config.wing_mount_length
        self.start_position_x = config.wing_start_position_x
        self.add_mount = True
        self.density = config.wing_density

        self.pipe_border = flute_body_config.pipe_border
        self._flute_body_config = flute_body_config
        self.num_pipes = flute_body_config.num_pipes
        self.z_delta = flute_body_config.wind_channel_height / 2
        self.mount_y_offset = self.mount_depth + self.z_delta * 2
        self.start_position_z = -self.z_delta

    def pipe_width_at_position(self, position: float) -> float:
        return self._flute_body_config.pipe_width_at_position(position)

    def pipe_height_at_position(self, position: float) -> float:
        return self._flute_body_config.pipe_height_at_position(position)

    def width_at_position(self, position: float) -> float:
        return self._flute_body_config.body_width_at_position(position)


class Wings:
    def __init__(self, config: WingsConfig):
        self.config = config
        self._built: Optional[cq.Workplane] = None


    def _build_wings_sketch(self, workplane: cq.Workplane, position: float, height, bottom_height=None) -> cq.Workplane:
        if bottom_height is None:
            bottom_height = self.config.pipe_border
        if height == bottom_height:
            no_radius_compensation = self.config.corner_radius
        else:
            no_radius_compensation = 0
        w = workplane
        w = w.center(-self.config.width_at_position(position) / 2, self.config.start_position_z)
        if height > 0:
            w = w.line(0, height - self.config.corner_radius)
        w = w.tangentArcPoint((self.config.corner_radius, self.config.corner_radius))
        w = w.line(self.config.pipe_border - self.config.corner_radius * 2, 0)
        for pipe_num in range(0,self.config.num_pipes):
            if height != bottom_height:
                w = w.tangentArcPoint((self.config.corner_radius, -self.config.corner_radius))
                w = w.line(0, -height + bottom_height + self.config.corner_radius)
            w = w.line(self.config.pipe_width_at_position(position) + 2 * no_radius_compensation, 0)
            if height != bottom_height:
                w = w.line(0, height - bottom_height - self.config.corner_radius)
                w = w.tangentArcPoint((self.config.corner_radius, self.config.corner_radius))
            w = w.line(self.config.pipe_border - 2 * self.config.corner_radius, 0)
        w = w.tangentArcPoint((self.config.corner_radius, -self.config.corner_radius))
        if height > 0:
            w = w.line(0, -height + self.config.corner_radius)
        w = w.close()
        return w

    def _build_wings_mount_sketch(self, workplane: cq.Workplane, position: float) -> cq.Workplane:
        width = self.config.width_at_position(position)
        w = workplane.center(-width / 2, - self.config.mount_y_offset)
        w = w.line(width, 0)
        w = w.line(0, self.config.pipe_border + self.config.blocker_height + self.config.mount_y_offset
                   - self.config.corner_radius - self.config.z_delta)
        w = w.tangentArcPoint((-self.config.corner_radius, self.config.corner_radius))
        w = w.line(-width + 2 * self.config.corner_radius, 0)
        w = w.tangentArcPoint((-self.config.corner_radius, -self.config.corner_radius))
        w = w.close()
        return w

    def _build_wings_mount(self, position: float) -> cq.Workplane:
        w = cq.Workplane("YZ").workplane(offset=position)
        w = self._build_wings_mount_sketch(w, position=position)
        w = self._build_wings_mount_sketch(w.workplane(offset=self.config.mount_length, origin=(0,0)),
                                           position=position + self.config.mount_length)
        w = w.loft(ruled=True)
        return w

    def build(self) -> cq.Workplane:
        position = self.config.start_position_x

        if self.config.add_mount:
            mount = self._build_wings_mount(position - self.config.mount_length)

        workplane = cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)).center(0, self.config.pipe_border)

        wings_stopper_lower = self._build_wings_sketch(workplane, position=position, height=self.config.blocker_height,
                                                       bottom_height=self.config.blocker_height)
        position += self.config.wing_stopper_length
        wings_stopper_upper = self._build_wings_sketch(
            wings_stopper_lower.workplane(offset=self.config.wing_stopper_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.blocker_height, bottom_height=self.config.blocker_height
        )
        wings_stopper = wings_stopper_upper.loft()

        wings_stopper_gap_lower = self._build_wings_sketch(
            wings_stopper_upper.workplane(offset=0, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.pipe_border
        )
        position += self.config.blocker_gap_length
        wings_stopper_gap_upper = self._build_wings_sketch(
            wings_stopper_gap_lower.workplane(
                offset=self.config.blocker_gap_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.pipe_border
        )
        wings_stopper_gap = wings_stopper_gap_upper.loft()

        wings_ramp_top_lower = self._build_wings_sketch(
            wings_stopper_gap_upper.workplane(origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.wing_height
        )
        position += self.config.wing_ramp_top_length
        wings_ramp_top_upper = self._build_wings_sketch(
            wings_ramp_top_lower.workplane(offset=self.config.wing_ramp_top_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.wing_height
        )
        wings_ramp_top = wings_ramp_top_upper.loft()

        wings_ramp_lower = self._build_wings_sketch(
            wings_ramp_top_upper.workplane(offset=0, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.wing_height
        )
        position += self.config.wing_ramp_length
        wings_ramp_upper = self._build_wings_sketch(
            wings_ramp_lower.workplane(offset=self.config.wing_ramp_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.corner_radius + 5, bottom_height=0
        )
        wings_ramp = wings_ramp_upper.loft()

        wings = cq.Workplane()
        if self.config.add_mount:
            wings = wings.union(mount)
        wings = (wings.union(wings_stopper)
                 .union(wings_stopper_gap)
                 .union(wings_ramp_top)
                 .union(wings_ramp))

        self._built = wings
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig):
    body_config = FluteBodyConfig(config)

    wings_config = WingsConfig(body_config)
    wings = Wings(wings_config)
    wings_obj = wings.build()

    return wings_obj


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END