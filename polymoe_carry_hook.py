# SINGLEPY_IGNORE_BEGIN
import math

import cadquery as cq

from polymoe_body import FluteBodyConfig
from polymoe_cover import CoverConfig
from polymoe_stringplate import StringPlateConfig
from polymoe_config import *
# SINGLEPY_IGNORE_END

class CarryHookConfig:
    def __init__(self, body_config: FluteBodyConfig, stringplate_config: StringPlateConfig):
        self._body_config = body_config

        config = body_config.config

        self.density = config.hook_density
        self.blocker_gap_length = stringplate_config.blocker_gap_length
        self.gap_depth = config.hook_gap_depth
        self.blocker_length = config.hook_blocker_length
        self.start_position_x = stringplate_config.start_position_x + stringplate_config.wing_stopper_length + self.blocker_gap_length + self.blocker_length
        self.end_position_on_pipe_x = self.start_position_x - config.hook_on_pipe_length
        self.start_position_z = -body_config.body_height_at_position(self.start_position_x) + config.body_hook_stacking_depth
        self.end_position_on_pipe_z = -body_config.body_height_at_position(self.end_position_on_pipe_x)
        self.upper_part_length = config.hook_upper_part_length
        self.radius = config.hook_radius
        self.upper_end_position_z = -body_config.body_height_at_position(self.end_position_on_pipe_x - self.upper_part_length) - self.radius
        self.thickness = config.hook_thickness
        self.fillet_radius = config.hook_fillet_radius
        self.body_fillet_radius = body_config.yz_fillets
        self.use_fillets = body_config.fillets
        self.width = config.hook_width if config.hook_width is not None else body_config.body_width_at_position(self.start_position_x) - self._body_config.pipe_yz_fillet_radius * 2
        length_between_points_on_border_xy = math.sqrt(1 + (self.body_width_at_position(self.start_position_x) / 2 - self.body_width_at_position(self.start_position_x - 1) / 2) ** 2)
        length_between_points_on_border_xz = math.sqrt(1 + (self.body_height_at_position(self.start_position_x) / 2 - self.body_height_at_position(self.start_position_x - 1) / 2) ** 2)
        self.rotation_angle_y = config.hook_rotation_angle_y if config.hook_rotation_angle_y is not None else math.acos(1 / length_between_points_on_border_xz) / math.pi * 180
        self.rotation_angle_z = config.hook_rotation_angle_z if config.hook_rotation_angle_z is not None else math.acos(1 / length_between_points_on_border_xy) / math.pi * 180

    def body_height_at_position(self, position: float):
        return self._body_config.body_height_at_position(position)

    def body_width_at_position(self, position: float):  # TODO: let width depend on body width
        return self._body_config.body_width_at_position(position)

class CarryHook:
    def __init__(self, config: CarryHookConfig):
        self.config = config
        self._built = None

    def _build_hook_sketch(self, workplane: cq.Workplane = None, rotation_angle:float = 0) -> cq.Workplane:
        z_position_at_gap = self.config.body_height_at_position(self.config.start_position_x - self.config.blocker_length)
        if workplane is None:
            workplane = cq.Workplane("XZ").workplane(offset=-self.config.width / 2)
        w = (workplane
             .center(self.config.start_position_x, self.config.start_position_z).transformed(rotate=(0, rotation_angle, 0))
             .line(self.config.end_position_on_pipe_x - self.config.start_position_x, self.config.end_position_on_pipe_z - self.config.start_position_z)
             .line(0, -self.config.thickness / 2)
             .tangentArcPoint((-self.config.radius, -self.config.radius))
             #.line(- self.config.upper_part_length, self.config.upper_end_position_z - self.config.end_position_on_pipe_z + self.config.radius)
             .line(- self.config.upper_part_length, 0)
             .line(0, -self.config.thickness)
             .line(self.config.upper_part_length + self.config.thickness, -(self.config.upper_end_position_z - self.config.end_position_on_pipe_z + self.config.radius))
             .tangentArcPoint((self.config.radius, self.config.radius))
             .line(-self.config.end_position_on_pipe_x + self.config.start_position_x - self.config.thickness - self.config.blocker_gap_length - self.config.blocker_length, 0)
             .line(0, self.config.gap_depth)
             .line(self.config.blocker_gap_length, 0)
             .line(0, -self.config.gap_depth)
             .line(self.config.blocker_length, 0)
             .close()
         )
        return w

    def build(self) -> cq.Workplane:
        left_sketch = self._build_hook_sketch(rotation_angle=-self.config.rotation_angle_z)
        right_sketch = self._build_hook_sketch(left_sketch.transformed(rotate=(0,self.config.rotation_angle_z,0)).workplane(offset=self.config.width,origin=(0,0)), rotation_angle=self.config.rotation_angle_z)
        self._built = right_sketch.loft(ruled=True)
        if self.config.use_fillets:
            self._built = self._built.edges().fillet(self.config.fillet_radius)
            #fillet = continuous_endpiece(self._build_hook_sketch().wires().val(), self.config.fillet_radius, invert_normals=True)

        self._built = self._built.rotate(axisStartPoint=cq.Vector(self.config.start_position_x, 0, self.config.start_position_z),
                                         axisEndPoint=cq.Vector(self.config.start_position_x, 1, self.config.start_position_z),
                                         angleDegrees=self.config.rotation_angle_y)

        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight

# SINGLEPY_IGNORE_BEGIN
def main(config: PolyMoeConfig):
    body_config = FluteBodyConfig(config)
    cover_config = CoverConfig(body_config)
    stringplate_config = StringPlateConfig(flute_body_config=body_config, cover_config=cover_config)

    hook_config = CarryHookConfig(body_config=body_config, stringplate_config=stringplate_config)
    hook = CarryHook(hook_config)
    hook_obj = hook.build()

    return hook_obj


if __name__ == '__main__':
    config = PolyMoeConfig()
    result = main(config)
    if config.save_step:
        cq.exporters.export(result, "polymoe.step", cq.exporters.ExportTypes.STEP)
    if 'show_object' in globals():
        show_object(result)
# SINGLEPY_IGNORE_END