'''
This is a concatenated version for Cadhub and CQEditor. For actual work on the scripts the multi-file version from the repository is recommended.
The concatenated version can be regenerated via 'make_polymoe_all.py'

This is a flute that can be played polyphonically with e.g. four independent voices or melody with chords over several octaves. Depending on configuration the notes of a voice can be discrete (chromatic, diatonic, pentatonic etc.) like a usual flute or completely continuous like a violin or trombone.

For full description look here: https://gitlab.com/fstrixner/polymoe

To CadHub users: Many features increase the rendering time over the current 30s limit of CadHub. In order to use the full configurations besides the demo config 'CH', better use CQEditor or a Conda based local CadQuery installation for the moment. Afaik the 30s limit is supposed to be eliminated in newer CadHub versions - then the full PolyMoe configurations should be usable.
'''

from typing import List, Union, Dict, Any
from enum import Enum
import cadquery as cq
import math

# polymoe_config.py


class PolyMoeConfig:

    def __init__(self, config_abbreviation: str = None):
        if config_abbreviation is not None:
            self.configuration = config_abbreviation
        else:
            '''
                Currently active configuration. In the optimal case this could  be the only parameter that might need to be changed.
                
                configurations:
                S: small: only the front flute that is held in place with the thumbs in the carry hooks
                M: medium: with first extension. To be played either by supporting the instrument via a chair or by using a strap around the neck that connects to the upper carry hook
                L: large: with two extensions. Long enough to place one end on the floor. Played with the rounded mouthpiece
                CH: Configuration that still works with CadHub (within the 20s timeout)
                '''
            self.configuration = "S"

        #####################
        # common parameters #
        #####################
        # These parameters are used by the main assembly and control some behavior common to all or at least several parts

        # export result as multi-part STEP file (for CNC, visualization etc.) [True; False]
        self.save_step = self.conf({"CH": False, "default": True})
        # export STL files individually for each part
        self.save_stl_parts = self.conf({"CH": False, "default": True})

        # number of pipes (for polyphonic playing one needs at least two pipes) [1-6]
        self.num_pipes = self.conf({"CH": 1, "default": 3})
        # a curved sax-like tubing can help to play comfortably with longer instrument sizes, so the arms can span a bigger range
        self.use_curved_mouthpiece = self.conf({"L": True, "default": False})

        # layouts define whether notes are continuous (like a trombone) or whether the pitch strips have discrete holes like a flute.
        # Different layouts can be mixed, e.g. to allow easier chord play with discrete pitch strips and continuous strips
        # for expressive melody play with vibrato, continuous portamento. playing with interferences etc.
        # available layouts:
        # * continuous (open slit)
        # * chromatic (12 discrete, equal tempered semitones)
        # * diatonic (major/minor scale)
        # * pentatonic (5 note scale)
        # * octaves (only the base note per octave)
        # * bourdon ( no holes or slit, only the base note of the open pipe, like an overtone flute)

        # Here a number of sane proposals - but new combos can be built at will
        # If num_pipes is smaller than the specified strips then the last entries are skipped, if it has more then
        # continuous is assumed as default
        interference_layout_proposal = { # having two continuous scales allows to play with interferences
            0: "continuous",
            1: "continuous",
            2: "chromatic",
            3: "chromatic",
            4: "chromatic",
        }
        melody_and_chords_layout_proposal = { # chromatic for chords, one continuous melody scale
            0: "continuous",
            1: "chromatic",
            2: "chromatic",
            3: "chromatic",
            4: "chromatic"
        }
        all_continuous = { # profi configuration: allows chords built from pure intervalls, expressive melodies, interferences etc. - but hard to intonate
            0: "continuous",
            1: "continuous",
            2: "continous",
            3: "continous",
            4: "continuous"
        }
        all_diatonic = { # "organ style", for easier intonation, but losing some expressiveness and pure intervals
            0: "chromatic",
            1: "chromatic",
            2: "chromatic",
            3: "chromatic",
            4: "chromatic"
        }
        all_pentatonic = { # "native american flute" like - for easy improvisation
            0: "pentatonic",
            1: "pentatonic",
            2: "pentatonic",
            3: "pentatonic",
            4: "pentatonic"
        }
        try_all_layouts = { # these are mainly for demonstration, doesn't make too much sense musically...
            0: "continuous",
            1: "chromatic",
            2: "diatonic",
            3: "octaves",
            4: "penatonic",
            5: "bourdon"
        }

        # actually used layout
        self.pitchstrip_layout = try_all_layouts

        # the frequency of the lowest hole on a pitch strip (only applies to pitch strips with discrete holes)
        frequency_for_lowest_hole_proposal = self.conf({
            "S": 1046.50, # C6
            "M": 261.63, # C4 #220.00, # A3
            "L": 130.81, # C3
            "CH": 261.63
        })
        frequency_for_lowest_hole_proposal2 = self.conf({"S": 1046.50, "M": 223.5, "L": 130.81, "CH": 223.5}) # example: let scale go down to A3 for config "M" for this pipe - TODO: why do I have to use 223.5 instead of 220 to keep the scale in sync with the scale starting from C4 at 261.63 Hz? The frequencies should already be the well-tempered ones?
        # lowest frequency per pitch strip
        self.frequency_of_lowest_hole = {
            0: frequency_for_lowest_hole_proposal,
            1: frequency_for_lowest_hole_proposal2,
            2: frequency_for_lowest_hole_proposal,
            3: frequency_for_lowest_hole_proposal,
            4: frequency_for_lowest_hole_proposal
        }

        ##############################
        # body and common parameters #
        ##############################
        # these parameters are mainly used for the main instrument body and it's optional extensions
        # But some are also reused by other parts like the cover etc.
        # (in cases where dimensions have to be consistent between several parts)


        # the width and height of a single pipe at the bottom and top (if wider/higher on the bottom than on the top then the
        # pipe is conical which can be helpful so the pipe diameter is smaller for high and bigger for low notes,
        # making it easier to hit them without overblowing)
        # An increasing width/height can also give some haptic feedback at which instrument position the fingers are.
        # But the downside particularly with increasing width is that the stretch between fingers on adjacent strips gets bigger.
        # So one might want experiment with keeping bottom and top width the same and partially (or fully) using the
        # height instead of the width the make the diameter of lower notes bigger.
        # TODO: How do non quadratic pipe shapes might affect the timbre(?)
        #       How does the conicity of the pipe affect the timbre(?)
        self.pipe_width_at_bottom = self.conf({"S": 27.5, "M": 42.402, "L": 57.060, "CH": 42.402}) # 20
        self.pipe_width_at_top = 20
        #self.pipe_height_at_bottom = self.conf({"S": 27.5, "M": 42.402, "L": 57.060, "CH": 42.402})
        self.pipe_height_at_bottom = 20
        self.pipe_height_at_top = 20

        # Width (and height) of the pipe border.
        # Overall instrument width is: num_pipes * width_pipe + (num_pipes + 1) * pipe_border
        # Overall instrument height is: height_pipe + 2 * pipe_border
        self.pipe_border = 7.6

        # length of the open pipe from bottom opening to labium
        self.soundpipe_length = self.conf({"S": 240, "M": 850, "L": 1450, "CH": 850})

        # the sound pipe can be divided into the main instrument and extensions that can be bound to the
        # main instrument to extend it's length.
        # This allows to change the instrument length for different playing approaches:
        # * instrument held in hands with only fingers moving (pipe length ~ 2*finger span)
        # * instrument held by a strip around the neck (pipe length ~ arm length)
        # * instrument standing on the floor and played via curved mouthpiece (as long as a standing person)
        # This also simplifies manufacturing.
        # If modularity isn't a priority and manufacturing allows it, then a large instrument can of course also be made
        # without extensions in one piece
        #self.extension_cuts = self.conf({"S": [], "M": [610], "L": [600, 610], "CH": []})
        self.extension_cuts = self.conf({"S": [], "M": [300,310], "L": [300, 300, 300, 310], "CH": []})

        # length of the open hole between labium and wind channel
        self.air_hole_length = 7

        # height and length of the wind channel (the labium is always vertically centered in front of the wind channel)
        self.wind_channel_height = 2
        self.wind_channel_length = 20
        # length of the ramp that leads to the wind channel (this also affects the slope of the ramp)
        self.wind_channel_ramp_length = 25

        # length of the chamber behind the wind channel ramp that is used to linearize the air stream
        # (similar to native american flutes)
        self.slow_airchamber_total_length = 40

        # width, height and length of the pipes after the slow air chamber that lead to the mouthpiece
        # these have usually a smaller diameter than the slow airchamber before it
        self.mouthpiece_pipe_height = 9 #6
        self.mouthpiece_pipe_width = 9 #6
        self.mouthpiece_length = 60

        # the outer border of the mouthpiece pipes can be thinner than the walls of the soundpipe
        # - particularly the inner borders. This allows a more compact mouth piece that fits into the mouth more comfortably
        self.mouthpiece_outer_border = 6.5
        self.mouthpiece_inner_border = 4
        self.mouthpiece_bottom_border = 3

        # factor how much smaller the chord knob that is used to bind the mouthpiece coverplate to the mouthiece pipes are
        # compared to the normal knobs are (see knob for the normal knob dimensions)
        self.mouthpiece_knob_size_factor = 0.5 # 0.15

        # defines whether some corners of the instrument will be rounded (True) or edgy (False)
        self.fillets = self.conf({"CH": False, "default": True})

        # radius of fillets along the width and height of the instrument
        self.pipe_x_fillet_radius = 10
        # a smaller radius for certain fillets along the width and height of the instrument for cases where the one above is too big
        self.pipe_x__back_fillet_radius = 2 # TODO: still needed?
        # a radius for certain fillets along the length of the instrument (e.g. the rounding of the lower body edges)
        self.pipe_yz_fillet_radius = 2 #5
        # radius for certain fillets of the mouthpiece
        self.mouthpiece_fillet_radius = 2
        # radius for certain fillets of smaller details of the mouthpiece
        self.mouthpiece_front_fillet_radius = 0.9

        # length of the carved stacking adapters that help to keep the extensions connected to each other and to the main body
        self.body_stacking_carving_length = self.conf({"CH": 0, "default": 5})

        # depth of the carving at the bottom that holds the carrying hook in place
        self.body_hook_stacking_depth = self.conf({"CH": 0, "default": 2})

        # density of the material used to create the main instrument body and the extension bodies
        self.body_density = 0.34  # g/cm^3 cedar wood

        #############################
        # cover specific parameters #
        #############################
        # these parameters only affect the cover and cover extensions that are bound on top of the body and it's extensions
        # It closes the sound pipe, slow air pipe and mouthpiece from above and contains functional elements like the labium

        # radius used for certain cover fillets (like the length-wise upper cover fillets)
        self.cover_upper_fillet_radius = 5 #0.2
        # length of the labium
        self.cover_labium_length = 15
        # the labium is narrower than the pipe width by that offset on the left and right side
        self.cover_labium_offset = 2
        # in the case of continuous tracks this is the width of the cut, in all other cases the hole diameter
        self.cover_cut_diameter = 5
        # width and depth of the inlay for the pitch strip
        self.cover_inlay_diameter = 15
        self.cover_inlay_depth = 1
        # length of area between the end of the last hole of the manual and the labium
        self.cover_closed_fingerboard_length = 50
        # length of area between the end of the inlay and the labium
        self.cover_above_inlay_length = 10

        # the body subtracted from the cover to carve some pockets into the cover that help to keep the cover attached to the body
        # this defines the height of this carving
        self.cover_stacking_carving_height = self.conf({"default": 1, "CH": 0})
        # currently the rounded parts of the cover at the transition between body and moundpiece is slightly bigger than the body
        # and thus creates artifacts when cutting out the stacking area. One possible workaround is to make the cover width
        # slightly smaller than the body, so it fits at the rounded area (TODO: get rid of width deviations so no more correction is needed)
        self.cover_stacking_width_correction = 0#1.3
        # density of the cover material
        self.cover_density = 0.34 # g/cm^3 cedar wood

        #############################
        # wings specific parameters #
        #############################
        # The "wings" are a structure that is bound on top of the lower side of the cover.
        # It is used to keep the magnetic, flexible strips away from the metal pitch strips below the finger position
        # If the pipe width at the bottom is unequal to the pipe width at top, then different wings have to be
        # provided depending on which extensions are bound to the main body.
        # If the width is constant then one wings part can be used with or without extensions, assuming the wing height already
        # copes for the maximum extension length (thus even for constant pipe width several wing parts could make sense
        # so a less high and thus more light weight wing part can be used for shorter instruments without extensions)

        # The heigher the wing the further it can keep the flexible strip away from the metal pitch strip. Thus depends on
        # the overall instrument length
        self.wing_height = self.conf({"S": 37.5, "M": 132.81225, "L": 226.56225, "default": 132.81225})
        self.wing_stopper_length = 10
        self.wing_ramp_length = self.conf({"S": 5.647, "M": 20, "L": 34.118, "default": 20})
        self.wing_ramp_top_length = 10
        self.wing_blocker_height = 15
        self.wing_blocker_gap_length = 5
        self.wing_corner_radius = 2
        self.wing_mount_depth = -self.pipe_border / 2
        self.wing_mount_length = self.conf({"default": self.body_stacking_carving_length, "CH": 5})
        self.wing_start_position_x = 0
        self.wing_density = 0.34  # g/cm^3 cedar wood

        ###################################
        # stringplate specific parameters #
        ###################################
        # The stringplate is the counterpart of the "wings". It presses the top of the magnetic, flexible strip to the
        # instrument cover. It should be far enough away from the beginning of the holes/continuous pitch carving
        # that the flexible strip gets pulled away from the metal strip as soon as the finger doesn't press down the
        # flexible strip anymore

        self.stringplate_distance_from_manual = 10
        self.stringplate_height = 12
        self.stringplate_stopper_length = 5
        self.stringplate_ramp_length = 10
        self.stringplate_ramp_top_length = 5
        self.stringplate_blocker_height = 12
        self.stringplate_density = 0.34  # g/cm^3 cedar wood

        ##################################
        # pitchstrip specific parameters #
        ##################################
        # The pitch strips are the metal plates that get bound to the cover. These either contain
        # holes for the discrete case and/or a continuous pitch carving
        # If body and cover extensions are bound to the main instrument then longer pitch strips have to be used
        # (So for intended modular length of the instrument a fitting set of pitch strips has to be provided)

        # if the holes of a discrete strip get closer than this value then a continuous pitch carving is created instead
        # of further holes. That way high pitches notes are still playable even though the holes would start to intersect
        self.pitchstrip_continuous_if_hole_distance_smaller_than = 7
        # radius of holes (only relevant for discrete strips)
        self.pitchstrip_hole_radius = 2.5
        # density of the material for the metal pitch strips (the material should be both magnetic and rustproof)
        self.pitchstrip_density = 7.85  # g/cm^3 carbon steel

        #############################################
        # mouthpiece coverplate specific parameters #
        #############################################
        # The cover plate is either bound to the main instrument body or to the rounded mouthpiece extension
        # It is the part that the player takes into the mouth, though it should be smooth and in one piece

        self.mouthpiece_coverplate_length = 5
        self.mouthpiece_coverplate_density = 0.34  # g/cm^3 cedar wood

        #########################################
        # curved mouthpiece specific parameters #
        #########################################
        # the curves (sax-like) extension of the mouthpiece that can optionally be used for longer instruments

        self.curvedmouthpiece_density = 0.34  # g/cm^3 cedar wood
        # length of different sections of the curved mouthpiece
        '''
        2222
        1  3
           3
           3444
        '''
        self.curvedmouthpiece_length1 = 5 # from start to highest point
        self.curvedmouthpiece_length2 = 70 # from highest point sideways
        self.curvedmouthpiece_length3 = 200 # downwards
        self.curvedmouthpiece_length4 = 50 # towards the player
        # use rounded corners (True) or edgy corners (False)
        self.curvedmouthpiece_round_corners = self.conf({"CH": False, "default": True})

        ##################################
        # carry hook specific parameters #
        ##################################
        # This hook can be used by the thumbs to carry the instrument in the case it is short enough.
        # Or the hook can be attached to a neck strip for longer instrument (that are not long enough that they reach the floor)

        self.hook_width = None # None: use entire body width
        self.hook_gap_depth = 5
        self.hook_blocker_length = 20
        self.hook_on_pipe_length = 100 # length projected to the x axis
        self.hook_density = 0.34 # g/cm^3 cedar wood
        self.hook_radius = 20
        self.hook_upper_part_length = 50 # projected to the x axis
        self.hook_thickness = 10
        self.hook_fillet_radius = 1.5
        # angle between x|z axis and left|right wall of the hook (mirrored)
        self.hook_rotation_angle_y = None  # None: calculate from body slope
        self.hook_rotation_angle_z = None  # None: calculate from body slope

        ############################
        # knob specific parameters #
        ############################
        # The knobs are used to bind several parts of the instrument together with cords.
        # Compared to gluing everything together this has two advantages:
        # * The instrument stays modular, so different slized variants can be assembled from the same base instrument
        # * The instrument can be disassembled for maintenance (e.g. oiling, cleaning etc.)

        self.knob_density = 0.34  # g/cm^3 cedar wood
        self.knob_length = 8
        self.knob_fillet_radius = 5
        self.knob_size_factor = 1.0
        self.knob_width_min = self.pipe_border - 2
        self.knob_width_max = self.pipe_border

    # small helper function that allows to define values per configuration - or a default value
    def conf(self, x: Dict[str, Any]):
        if self.configuration in x.keys():
            return x[self.configuration]
        elif 'default' in x.keys():
            return x['default']
        else:
            raise Exception(f"value not defined for configuration {self.configuration}")
# polymoe_util.py


def log(msg):
    print(msg)


def continuous_connection(start_wire: cq.Wire, end_wire: cq.Wire, invert_normals: bool = False,
                          middle_wires: List[cq.Wire] = None) -> cq.Solid:
    wires = []
    wires.append(start_wire)
    if middle_wires is None:
        wires.append(start_wire.translate(start_wire.normal() * 0.1 * (-1 if invert_normals else 1)))
        wires.append(end_wire.translate(end_wire.normal() * -0.1 * (-1 if invert_normals else 1)))
    else:
        for middle_wire in middle_wires:
            wires.append(middle_wire)
    wires.append(end_wire)

    loft = cq.Solid.makeLoft(wires, ruled=False)
    return loft


def continuous_endpiece(wire: cq.Wire, fillet_radius: float,
                        invert_normals: bool = False, offset: float = 0.0) -> cq.Solid:
    middle_wire_1 = wire.offset2D(offset, kind="tangent")[0].translate(wire.normal()*0.01*(-1 if invert_normals else 1))
    middle_wire_2 = wire.offset2D(-fillet_radius + 0.01, kind="tangent")[0].translate(
        wire.normal() * fillet_radius*(-1 if invert_normals else 1))
    upper_wire = wire.offset2D(-fillet_radius, kind="tangent")[0].translate(
        wire.normal() * fillet_radius*(-1 if invert_normals else 1))
    loft = cq.Solid.makeLoft([wire, middle_wire_1, middle_wire_2, upper_wire], ruled=False)
    return loft
# polymoe_knob.py


class KnobDirection(Enum):
    Upper = 0,
    RightUpper = 1,
    Right = 2,
    RightLower = 3,
    Lower = 4,
    LeftLower = 5,
    Left = 6,
    LeftUpper = 7


class KnobSide(Enum):
    Left = 1.0
    Right = -1.0


class KnobConfig:
    def __init__(self, config: PolyMoeConfig, x_position, y_position, z_position, direction: KnobDirection, side: KnobSide):
        self.density = config.knob_density
        self.x_position = x_position
        self.y_position = y_position
        self.z_position = z_position
        self.direction = direction
        self.knob_length = config.knob_length
        self.knob_width_max = config.knob_width_max
        self.knob_width_min = config.knob_width_min
        self.fillet_radius = config.knob_fillet_radius
        self.size_factor = config.knob_size_factor
        self.side = side


class Knob:
    def __init__(self, config: KnobConfig):
        self.config = config
        self._built = None

    def _build_cord_knob_sketch(self, workplane: cq.Workplane, knob_width: float) -> cq.Workplane:
        if self.config.direction == KnobDirection.Upper:
            start_direction_x = -1
            start_direction_y = 0
            target_direction_x = 2
            target_direction_y = 0
        elif self.config.direction == KnobDirection.RightUpper:
            start_direction_x = 0
            start_direction_y = 1
            target_direction_x = 1
            target_direction_y = -1
        elif self.config.direction == KnobDirection.Right:
            start_direction_x = 0
            start_direction_y = 1
            target_direction_x = 0
            target_direction_y = -2
        elif self.config.direction == KnobDirection.RightLower:
            start_direction_x = 1
            start_direction_y = 0
            target_direction_x = -1
            target_direction_y = -1
        elif self.config.direction == KnobDirection.Lower:
            start_direction_x = 1
            start_direction_y = 0
            target_direction_x = -2
            target_direction_y = 0
        elif self.config.direction == KnobDirection.LeftLower:
            start_direction_x = 0
            start_direction_y = -1
            target_direction_x = -1
            target_direction_y = 1
        elif self.config.direction == KnobDirection.Left:
            start_direction_x = 0
            start_direction_y = -1
            target_direction_x = 0
            target_direction_y = 2
        elif self.config.direction == KnobDirection.LeftUpper:
            start_direction_x = -1
            start_direction_y = 0
            target_direction_x = 1
            target_direction_y = 1
        else:
            raise Exception(f"unexpected direction {self.config.direction}")
        w = workplane.line(start_direction_x * knob_width, start_direction_y * knob_width)
        w = w.line(0.01 * start_direction_y, 0.01 * -start_direction_x)
        w = w.tangentArcPoint((knob_width * target_direction_x, knob_width * target_direction_y))
        w = w.close()
        return w

    def _build_knob(self) -> cq.Workplane:
        w = cq.Workplane("XZ").workplane(
            offset=(-self.config.y_position / 2 - self.config.knob_length * self.config.size_factor) * self.config.side.value)
        w = w.center(self.config.x_position, -self.config.z_position)
        w = self._build_cord_knob_sketch(w, knob_width=1.0 * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=0), knob_width=1.01 * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=self.config.knob_length / 2 * self.config.side.value * self.config.size_factor),
                                         knob_width=self.config.knob_width_max * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=0.01 * self.config.side.value),
                                         knob_width=self.config.knob_width_max * self.config.size_factor)
        w = self._build_cord_knob_sketch(
            w.workplane(offset=(self.config.knob_length / 2 - 0.01 + self.config.fillet_radius) * self.config.side.value * self.config.size_factor),
            knob_width=self.config.knob_width_min * self.config.size_factor)
        w = self._build_cord_knob_sketch(w.workplane(offset=0.01 * self.config.side.value * self.config.size_factor),
                                         knob_width=self.config.knob_width_min * self.config.size_factor)
        #w = w.loft(ruled=True,combine=False,clean=False)
        w = w.loft(ruled=False)
        return w


    def build(self) -> cq.Workplane:
        self._built = self._build_knob()
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight



# polymoe_body.py

global_wire = None

class FluteBodyConfig:

    def __init__(self, config: PolyMoeConfig):
        self.config = config

        self.num_pipes = config.num_pipes
        self.pipe_width_at_bottom = config.pipe_width_at_bottom
        self.pipe_width_at_top = config.pipe_width_at_top
        self.pipe_height_at_bottom = config.pipe_height_at_bottom
        self.pipe_height_at_top = config.pipe_height_at_top
        self.pipe_border = config.pipe_border
        self.soundpipe_length = config.soundpipe_length
        self.extension_cuts = config.extension_cuts
        self.air_hole_length = config.air_hole_length
        self.wind_channel_height = config.wind_channel_height
        self.wind_channel_length = config.wind_channel_length
        self.wind_channel_ramp_length = config.wind_channel_ramp_length
        self.slow_airchamber_total_length = config.slow_airchamber_total_length
        self.mouthpiece_pipe_height = config.mouthpiece_pipe_height
        self.mouthpiece_pipe_width = config.mouthpiece_pipe_width
        self.mouthpiece_length = config.mouthpiece_length
        self.mouthpiece_outer_border = config.mouthpiece_outer_border
        self.mouthpiece_inner_border = config.mouthpiece_inner_border
        self.mouthpiece_bottom_border = config.mouthpiece_bottom_border
        self.mouthpiece_knob_size_factor = config.mouthpiece_knob_size_factor
        self.fillets = config.fillets
        self.x_fillets = self.fillets
        self.yz_fillets = self.fillets
        self.pipe_x_fillet_radius = config.pipe_x_fillet_radius if self.x_fillets else 0
        self.pipe_x__back_fillet_radius = config.pipe_x__back_fillet_radius if self.x_fillets else 0
        self.pipe_yz_fillet_radius = config.pipe_yz_fillet_radius if self.yz_fillets else 0
        self.mouthpiece_fillet_radius = config.mouthpiece_fillet_radius if self.yz_fillets else 0
        self.mouthpiece_front_fillet_radius = config.mouthpiece_front_fillet_radius if self.x_fillets else 0
        self.stacking_carving_length = config.body_stacking_carving_length
        self.hook_stacking_depth = config.body_hook_stacking_depth
        self.density = config.body_density

    @property
    def bounding_box(self) -> cq.Vector:
        total_length =  self.pipe_length + self.mouthpiece_length
        total_width = self.body_width_at_position(0)
        total_height = self.pipe_height_at_bottom + self.pipe_border
        return cq.Vector(total_length, total_width, total_height)

    @property
    def pipe_length(self):
        return self.soundpipe_length + self.air_hole_length + self.wind_channel_length\
               + self.wind_channel_ramp_length + self.slow_airchamber_length

    @property
    def slow_airchamber_length(self):
        return self.slow_airchamber_total_length - self.wind_channel_ramp_length

    #@property
    #def slow_airchamber_length(self):
    #    return self.pipe_length - self.soundpipe_length - self.air_hole_length - self.wind_channel_length\
    #           - self.wind_channel_ramp_length

    def pipe_height_at_position(self, position: float) -> float:
        return self.pipe_height_at_bottom\
               - (self.pipe_height_at_bottom - self.pipe_height_at_top) * position / self.pipe_length

    def pipe_width_at_position(self, position: float) -> float:
        return self.pipe_width_at_bottom\
                - (self.pipe_width_at_bottom - self.pipe_width_at_top) * position / self.pipe_length

    def body_width_at_position(self, position: float) -> float:
        return self.pipe_width_at_position(position) * self.num_pipes + self.pipe_border * (self.num_pipes + 1)

    def body_height_at_position(self, position: float) -> float:
        return self.pipe_height_at_position(position) + self.pipe_border

    @property
    def mouthpiece_width(self) -> float:
        return self.mouthpiece_pipe_width * self.num_pipes + self.mouthpiece_outer_border * 2 + self.mouthpiece_inner_border * (self.num_pipes - 1)

    @property
    def mouthpiece_height(self) -> float:
        return self.mouthpiece_pipe_height + self.mouthpiece_bottom_border

    @property
    def total_length(self):
        return self.pipe_length + self.mouthpiece_length


class FluteBody:
    def __init__(self, config: FluteBodyConfig):
        self.config = config
        self._built = None

    def build_width_height_pipe_sketch(self, workplane: cq.Workplane, width: float, height: float,
                                       pipe_width: float,
                                       outer_border_width: float,
                                       inner_border_width: float,
                                       border_height_delta: float,
                                       bottom_height: float, bottom_fillet_radius: float):
        w = workplane.center(-width / 2,
                             -height + self.config.pipe_yz_fillet_radius)
        w = w.line(0, height - self.config.pipe_yz_fillet_radius + border_height_delta)
        w = w.line(outer_border_width, 0)
        for pipe_num in range(0, self.config.num_pipes):
            w = w.line(0, -bottom_height - border_height_delta + bottom_fillet_radius)
            if self.config.yz_fillets:
                w = w.tangentArcPoint((bottom_fillet_radius, -bottom_fillet_radius))
            w = w.line(pipe_width - 2 * bottom_fillet_radius, 0)
            if self.config.yz_fillets:
                w = w.tangentArcPoint((bottom_fillet_radius, bottom_fillet_radius))
            w = w.line(0, bottom_height + border_height_delta - bottom_fillet_radius)
            if pipe_num < self.config.num_pipes - 1:
                w = w.line(inner_border_width, 0)
        w = w.line(outer_border_width, 0)
        w = w.line(0, -height + self.config.pipe_yz_fillet_radius - border_height_delta - outer_border_width)
        if self.config.yz_fillets:
            w = w.tangentArcPoint((-self.config.pipe_yz_fillet_radius, -self.config.pipe_yz_fillet_radius))
        w = w.line(-width + 2 * self.config.pipe_yz_fillet_radius, 0)
        if self.config.yz_fillets:
            w = w.tangentArcPoint((-self.config.pipe_yz_fillet_radius, self.config.pipe_yz_fillet_radius))
        w = w.close()
        return w

    def _build_pipe_sketch(self, workplane: cq.Workplane, position: float, border_height_delta: float,
                           bottom_height: float, bottom_fillet_radius: float, height: float = None,
                           outer_border_width: float = None, inner_border_width: float = None
                           ):
        if height is None:
            height = self.config.pipe_height_at_position(position)
        if outer_border_width is None:
            outer_border_width = self.config.pipe_border
        if inner_border_width is None:
            inner_border_width = self.config.pipe_border
        return self.build_width_height_pipe_sketch(workplane=workplane,
                                                   width=self.config.pipe_width_at_position(position) *
                                                          self.config.num_pipes + inner_border_width
                                                          * (self.config.num_pipes - 1) + outer_border_width * 2,
                                                   height=height,
                                                   pipe_width=self.config.pipe_width_at_position(position),
                                                   outer_border_width=outer_border_width,
                                                   inner_border_width=inner_border_width,
                                                   border_height_delta=border_height_delta,
                                                   bottom_height=bottom_height,
                                                   bottom_fillet_radius=bottom_fillet_radius)

    def _build_pipe_soundpipe_sketch(self, workplane: cq.Workplane, position: float, height: float = None,
                                     bottom_height: float = None,
                                     outer_border_width: float = None,
                                     inner_border_width: float = None
                                     ) -> cq.Workplane:
        if bottom_height is None:
            bottom_height = self.config.pipe_height_at_position(position)
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=-self.config.wind_channel_height / 2,
                                       bottom_height=bottom_height,
                                       bottom_fillet_radius=self.config.pipe_yz_fillet_radius,
                                       height=height,
                                       outer_border_width=outer_border_width,
                                       inner_border_width=inner_border_width)

    def _build_pipe_air_hole_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=0,
                                       bottom_height=self.config.pipe_height_at_position(position),
                                       bottom_fillet_radius=self.config.pipe_yz_fillet_radius)

    def _build_pipe_body_block_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        if self.config.yz_fillets:
            bottom_radius = self.config.wind_channel_height - 0.001
        else:
            bottom_radius = 0
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=0,
                                       bottom_height=self.config.wind_channel_height,
                                       bottom_fillet_radius=bottom_radius)

    def _build_pipe_slow_airchamber_sketch(self, workplane: cq.Workplane, position) -> cq.Workplane:
        return self._build_pipe_sketch(workplane, position,
                                       border_height_delta=0,
                                       bottom_height=self.config.pipe_height_at_position(
                                           position),
                                       bottom_fillet_radius=self.config.mouthpiece_fillet_radius)

    def build_mouth_piece(self, workplane: cq.Workplane, extra_height=0.0) -> cq.Workplane:
        return self.build_width_height_pipe_sketch(workplane=workplane,
                                                   width=self.config.mouthpiece_width,
                                                   height=self.config.mouthpiece_height+extra_height,
                                                   pipe_width=self.config.mouthpiece_pipe_width,
                                                   outer_border_width=self.config.mouthpiece_outer_border,
                                                   inner_border_width=self.config.mouthpiece_inner_border,
                                                   border_height_delta=0,
                                                   bottom_height=self.config.mouthpiece_height,
                                                   bottom_fillet_radius=self.config.mouthpiece_fillet_radius).translate((0, 0, extra_height))

    def build_soundpipe(self, position: float, length: float,
                        lower_height: float = None, upper_height: float = None,
                        lower_bottom_height: float = None, upper_bottom_height: float = None,
                        outer_border_width: float = None,
                        inner_border_width: float = None
                        ):
        # if self.config._x_fillets:
        #     soundpipe_fillet_sketch = self._build_pipe_soundpipe_sketch(
        #         cq.Workplane("YZ").workplane(offset=position, origin=(0,0)), position=position, offset=0.0)
        #     sound_pipe_fillet = continuous_endpiece(
        #         soundpipe_fillet_sketch.wires().val(),
        #         fillet_radius=self.config.pipe_x__back_fillet_radius
        #     )

        soundpipe_lower = self._build_pipe_soundpipe_sketch(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position,
            height=lower_height, bottom_height=lower_bottom_height,
            outer_border_width=outer_border_width,
            inner_border_width=inner_border_width)
        position += length #- self.config.pipe_x__back_fillet_radius
        soundpipe_upper = self._build_pipe_soundpipe_sketch(
            soundpipe_lower.workplane(offset=length, #- self.config.pipe_x__back_fillet_radius,
                                      origin=(0, 0)), position=position, height=upper_height,
            bottom_height=upper_bottom_height,
            outer_border_width=outer_border_width,
            inner_border_width=inner_border_width)
        sound_pipe = soundpipe_upper.loft()
        return sound_pipe

    def build_stacking_carving(self, position: float) -> cq.Workplane:
        stacking_position = position - self.config.stacking_carving_length
        stacking_carving = self.build_soundpipe(stacking_position, length=self.config.stacking_carving_length,
                                                lower_height=self.config.pipe_height_at_position(
                                                    stacking_position),
                                                upper_height=self.config.pipe_height_at_position(
                                                    position),
                                                lower_bottom_height=self.config.pipe_height_at_position(
                                                    stacking_position),
                                                upper_bottom_height=self.config.pipe_height_at_position(
                                                    position),
                                                outer_border_width = self.config.pipe_border / 2,
                                                inner_border_width = self.config.pipe_border)
        return stacking_carving

    def build(self) -> List[cq.Workplane]:
        sound_pipe_extensions = []
        remaining_soundpipe = self.config.soundpipe_length
        position = 0
        for extension_length in self.config.extension_cuts:
            if self.config.stacking_carving_length != 0:
                stacking_carving = self.build_stacking_carving(position)

            knob_config_upper = KnobConfig(self.config.config, x_position=position,
                                           y_position=self.config.body_width_at_position(position),
                                           z_position=self.config.wind_channel_height / 2,
                                           direction=KnobDirection.RightLower,
                                           side=KnobSide.Left)
            cord_knob_upper_left = Knob(knob_config_upper).build()
            knob_config_upper.side = KnobSide.Right
            cord_knob_upper_right = Knob(knob_config_upper).build()
            knob_config_upper.z_position += self.config.body_height_at_position(position) - knob_config_upper.knob_width_max
            knob_config_upper.direction = KnobDirection.Right
            cord_knob_upper_right_down = Knob(knob_config_upper).build()
            knob_config_upper.side = KnobSide.Left
            cord_knob_upper_left_down = Knob(knob_config_upper).build()
            extension = cq.Workplane().add(self.build_soundpipe(position, length=extension_length))
            position += extension_length
            knob_config_lower = KnobConfig(self.config.config, x_position=position,
                                           y_position=self.config.body_width_at_position(position),
                                           z_position=self.config.wind_channel_height / 2,
                                           direction=KnobDirection.LeftLower,
                                           side=KnobSide.Left)
            cord_knob_lower_left = Knob(knob_config_lower).build()
            knob_config_lower.side = KnobSide.Right
            cord_knob_lower_right = Knob(knob_config_lower).build()
            knob_config_lower.z_position += self.config.body_height_at_position(position) - knob_config_lower.knob_width_max
            knob_config_lower.direction = KnobDirection.Left
            cord_knob_lower_right_down = Knob(knob_config_lower).build()
            knob_config_lower.side = KnobSide.Left
            cord_knob_lower_left_down = Knob(knob_config_lower).build()
            extension = extension.union(cord_knob_upper_right, clean=False)   # TODO: find out why clean leads to defective surfaces here
            extension = extension.union(cord_knob_upper_left, clean=False)
            extension = extension.union(cord_knob_lower_right, clean=False)
            extension = extension.union(cord_knob_lower_left, clean=False)
            extension = extension.union(cord_knob_upper_right_down, clean=False)
            extension = extension.union(cord_knob_upper_left_down, clean=False)
            extension = extension.union(cord_knob_lower_right_down, clean=False)
            extension = extension.union(cord_knob_lower_left_down, clean=False)
            if self.config.stacking_carving_length != 0:
                extension = extension.add(stacking_carving)  # TODO: why doesn't union work here?
            sound_pipe_extensions.append(extension)
            remaining_soundpipe -= extension_length
        remaining_soundpipe -= self.config.pipe_x__back_fillet_radius
        knob_config_upper = KnobConfig(self.config.config, x_position=position,
                                       y_position=self.config.body_width_at_position(position),
                                       z_position=self.config.wind_channel_height / 2,
                                       direction=KnobDirection.RightLower,
                                       side=KnobSide.Left)
        cord_knob_left = Knob(knob_config_upper).build()
        knob_config_upper.side = KnobSide.Right
        cord_knob_right = Knob(knob_config_upper).build()
        knob_config_upper.z_position += self.config.body_height_at_position(position) - knob_config_upper.knob_width_max
        knob_config_upper.direction = KnobDirection.Right
        cord_knob_right_down = Knob(knob_config_upper).build()
        knob_config_upper.side = KnobSide.Left
        cord_knob_left_down = Knob(knob_config_upper).build()
        sound_pipe = self.build_soundpipe(position, remaining_soundpipe)
        if self.config.stacking_carving_length != 0:
            stacking_carving = self.build_stacking_carving(position)
            sound_pipe.add(stacking_carving) # TODO: why doesn't union work here?
        position += remaining_soundpipe

        air_hole_lower = self._build_pipe_air_hole_sketch(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
        position += self.config.air_hole_length
        air_hole_upper = self._build_pipe_air_hole_sketch(
            air_hole_lower.workplane(offset=self.config.air_hole_length
                                     ,origin=(0, 0)), position=position)
        air_hole = air_hole_upper.loft()

        # if self.config._x_fillets:
        #     air_hole_to_block_connection_lower = self._build_pipe_air_hole_sketch(
        #         workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
        #     position += self.config.pipe_x_fillet_radius
        #     air_hole_to_block_connection_upper = self._build_pipe_body_block_sketch(workplane= air_hole_to_block_connection_lower.workplane(
        #         offset=self.config.pipe_x_fillet_radius, origin=(0, 0)), position=position)
        #     air_hole_to_block_connection = continuous_connection(
        #         air_hole_to_block_connection_lower.wires().val(),
        #         air_hole_to_block_connection_upper.wires().val(),
        #         invert_normals=True)

        block_lower = self._build_pipe_body_block_sketch(
            cq.Workplane("YZ", origin=(0, 0)).workplane(offset=position, origin=(0, 0)), position=position)
        position += self.config.wind_channel_length
        block_upper = self._build_pipe_body_block_sketch(
            block_lower.workplane(offset=self.config.wind_channel_length, origin=(0, 0)), position=position)
        block = block_upper.loft()

        if self.config.x_fillets:
            block_ramp_connection_1 = self._build_pipe_body_block_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += 0.1
            block_ramp_connection_2 = self._build_pipe_body_block_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.wind_channel_ramp_length - 0.2
            block_ramp_connection_3 = self._build_pipe_slow_airchamber_sketch(workplane=cq.Workplane("YZ").workplane(
                offset=position, origin=(0, 0)), position=position)
            position += 0.1
            block_ramp_connection_4 = self._build_pipe_slow_airchamber_sketch(workplane= cq.Workplane("YZ").workplane(
                offset=position, origin=(0, 0)), position=position)
            block_ramp = continuous_connection(
                block_ramp_connection_1.wires().val(),
                block_ramp_connection_4.wires().val(),
                invert_normals=True,
                middle_wires=[block_ramp_connection_2.wires().val(), block_ramp_connection_3.wires().val()]
            )
        else:
            block_ramp_lower = self._build_pipe_body_block_sketch(
                cq.Workplane("YZ", origin=(0, 0)).workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.wind_channel_ramp_length
            block_ramp_upper = self._build_pipe_slow_airchamber_sketch(
                block_ramp_lower.workplane(offset=self.config.wind_channel_ramp_length, origin=(0, 0)), position=position)
            block_ramp = block_ramp_upper.loft()

        slow_airchamber_lower = self._build_pipe_slow_airchamber_sketch(
            cq.Workplane("YZ", origin=(0, 0)).workplane(offset=position, origin=(0, 0)), position=position)
        position += self.config.slow_airchamber_length
        slow_airchamber_upper = self._build_pipe_slow_airchamber_sketch(
            slow_airchamber_lower.workplane(offset=self.config.slow_airchamber_length, origin=(0, 0)), position=position)
        slow_airchamber = slow_airchamber_upper.loft()

        position_after_slow_airchamger = position
        
        if self.config.x_fillets:
            pipe_to_mouthpiece__connection_1 = self._build_pipe_slow_airchamber_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += 0.1
            pipe_to_mouthpiece__connection_2 = self._build_pipe_slow_airchamber_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8  - 0.1
            pipe_to_mouthpiece__connection_3 = self._build_pipe_slow_airchamber_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 * 6 - 0.1
            pipe_to_mouthpiece__connection_4 = self.build_mouth_piece(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            position += 0.1
            pipe_to_mouthpiece__connection_5 = self.build_mouth_piece(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            mouthpiece1 = continuous_connection(
                pipe_to_mouthpiece__connection_1.wires().val(),
                pipe_to_mouthpiece__connection_5.wires().val(),
                invert_normals=True,
                middle_wires=[pipe_to_mouthpiece__connection_2.wires().val(),
                              pipe_to_mouthpiece__connection_3.wires().val(),
                              pipe_to_mouthpiece__connection_4.wires().val()
                              ]
            )    
        else:
            mouthpiece_lower = self._build_pipe_slow_airchamber_sketch(
                cq.Workplane("YZ", origin=(0,0)).workplane(offset=position, origin=(0, 0)), position=position)
            mouthpiece_middle = self.build_mouth_piece(
                mouthpiece_lower.workplane(
                    offset=self.config.mouthpiece_length / 8 * 7,
                    origin=(0, 0)
                ))
            mouthpiece1 = mouthpiece_middle.loft()
            position += self.config.mouthpiece_length / 8 * 7

        mouthpiece_middle2 = self.build_mouth_piece(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
        mouthpiece_upper = self.build_mouth_piece(
            mouthpiece_middle2.workplane(
                offset=self.config.mouthpiece_length / 8 * 1, origin=(0, 0)))
        mouthpiece2 = mouthpiece_upper.loft()
        position += self.config.mouthpiece_length / 8 * 1

        knob_config_mouthpiece = KnobConfig(self.config.config, x_position=position,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.LeftLower,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.z_position += self.config.mouthpiece_height
        knob_config_mouthpiece.direction = KnobDirection.Left
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()

        if self.config.x_fillets:
            flute_body = (cq.Workplane()
                          #.union(sound_pipe_fillet, clean=True)
                          .union(sound_pipe, clean=True)
                          .union(air_hole, clean=True)
                          #.union(air_hole_to_block_connection, clean=True)
                          .union(block, clean=True)
                          .union(block_ramp, clean=True)
                          .union(slow_airchamber, clean=True)
                          .union(mouthpiece1, clean=True)
                          .union(mouthpiece2, clean=True)
                          .union(cord_knob_left, clean=False) # TODO: why is cleaning leading to defective surfaces in these cases?
                          .union(cord_knob_right, clean=False)
                          .union(cord_knob_left_down, clean=False)
                          .union(cord_knob_right_down, clean=False)
                          .union(cord_knob_mouthpiece_left, clean=False)
                          .union(cord_knob_mouthpiece_right, clean=False)
                          .union(cord_knob_mouthpiece_left_down, clean=False)
                          .union(cord_knob_mouthpiece_right_down, clean=False)
                          )
        else:
            flute_body = sound_pipe.union(
                air_hole.union(
                    block.union(
                        block_ramp.union(
                            slow_airchamber.union(
                                mouthpiece1.union(
                                    mouthpiece2, clean=True
                                ), clean=True
                            ), clean=True
                        ), clean=True
                    ), clean=True
                ), clean=True
            ).union(cord_knob_left, clean=False).union(cord_knob_right, clean=False).union(cord_knob_left_down, clean=False).union(cord_knob_right_down, clean=False) \
                .union(cord_knob_mouthpiece_left, clean=False).union(cord_knob_mouthpiece_right, clean=False) \
                .union(cord_knob_mouthpiece_left_down, clean=False).union(cord_knob_mouthpiece_right_down, clean=False) # TODO: why is cleaning leading to defective surfaces in these cases?

        self._built = sound_pipe_extensions
        self._built.append(flute_body)

        return self._built

    def add_stacking(self) -> List[cq.Workplane]:
        if self._built is None:
            self.build()
        if self.config.stacking_carving_length != 0:
            for index in range(0, len(self._built) - 1):
                self._built[index] = self._built[index].cut(self._built[index + 1], clean=False)  # TODO: find out why clean doesn't work here
        return self._built

    def carve_mouthpiece_stacking(self, mouthpiece_cover_obj: cq.Workplane):
        if self._built is None:
            self.build()
        if self.config.stacking_carving_length != 0:
            self._built[-1] = self._built[-1].cut(mouthpiece_cover_obj, clean=False)  # TODO: find out why clean doesn't work here
        return self._built

    def carve_hook_stacking(self, carrying_hook_obj: cq.Workplane):
        if self._built is None:
            self.build()
        if self.config.hook_stacking_depth != 0:
            self._built[-1] = self._built[-1].cut(carrying_hook_obj, clean=False)  # TODO: find out why clean doesn't work here
        return self._built


    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# polymoe_wings.py

class WingsConfig:

    def __init__(self, flute_body_config: FluteBodyConfig):
        config = flute_body_config.config

        self.wing_height = config.wing_height
        self.wing_stopper_length = config.wing_stopper_length
        self.wing_ramp_length = config.wing_ramp_length
        self.wing_ramp_top_length = config.wing_ramp_top_length
        self.blocker_height = config.wing_blocker_height
        self.blocker_gap_length = config.wing_blocker_gap_length
        self.corner_radius = config.wing_corner_radius
        self.mount_depth = config.wing_mount_depth
        self.mount_length = config.wing_mount_length
        self.start_position_x = config.wing_start_position_x
        self.add_mount = True
        self.density = config.wing_density

        self.pipe_border = flute_body_config.pipe_border
        self._flute_body_config = flute_body_config
        self.num_pipes = flute_body_config.num_pipes
        self.z_delta = flute_body_config.wind_channel_height / 2
        self.mount_y_offset = self.mount_depth + self.z_delta * 2
        self.start_position_z = -self.z_delta

    def pipe_width_at_position(self, position: float) -> float:
        return self._flute_body_config.pipe_width_at_position(position)

    def pipe_height_at_position(self, position: float) -> float:
        return self._flute_body_config.pipe_height_at_position(position)

    def width_at_position(self, position: float) -> float:
        return self._flute_body_config.body_width_at_position(position)


class Wings:
    def __init__(self, config: WingsConfig):
        self.config = config
        self._built: Optional[cq.Workplane] = None


    def _build_wings_sketch(self, workplane: cq.Workplane, position: float, height, bottom_height=None) -> cq.Workplane:
        if bottom_height is None:
            bottom_height = self.config.pipe_border
        if height == bottom_height:
            no_radius_compensation = self.config.corner_radius
        else:
            no_radius_compensation = 0
        w = workplane
        w = w.center(-self.config.width_at_position(position) / 2, self.config.start_position_z)
        if height > 0:
            w = w.line(0, height - self.config.corner_radius)
        w = w.tangentArcPoint((self.config.corner_radius, self.config.corner_radius))
        w = w.line(self.config.pipe_border - self.config.corner_radius * 2, 0)
        for pipe_num in range(0,self.config.num_pipes):
            if height != bottom_height:
                w = w.tangentArcPoint((self.config.corner_radius, -self.config.corner_radius))
                w = w.line(0, -height + bottom_height + self.config.corner_radius)
            w = w.line(self.config.pipe_width_at_position(position) + 2 * no_radius_compensation, 0)
            if height != bottom_height:
                w = w.line(0, height - bottom_height - self.config.corner_radius)
                w = w.tangentArcPoint((self.config.corner_radius, self.config.corner_radius))
            w = w.line(self.config.pipe_border - 2 * self.config.corner_radius, 0)
        w = w.tangentArcPoint((self.config.corner_radius, -self.config.corner_radius))
        if height > 0:
            w = w.line(0, -height + self.config.corner_radius)
        w = w.close()
        return w

    def _build_wings_mount_sketch(self, workplane: cq.Workplane, position: float) -> cq.Workplane:
        width = self.config.width_at_position(position)
        w = workplane.center(-width / 2, - self.config.mount_y_offset)
        w = w.line(width, 0)
        w = w.line(0, self.config.pipe_border + self.config.blocker_height + self.config.mount_y_offset
                   - self.config.corner_radius - self.config.z_delta)
        w = w.tangentArcPoint((-self.config.corner_radius, self.config.corner_radius))
        w = w.line(-width + 2 * self.config.corner_radius, 0)
        w = w.tangentArcPoint((-self.config.corner_radius, -self.config.corner_radius))
        w = w.close()
        return w

    def _build_wings_mount(self, position: float) -> cq.Workplane:
        w = cq.Workplane("YZ").workplane(offset=position)
        w = self._build_wings_mount_sketch(w, position=position)
        w = self._build_wings_mount_sketch(w.workplane(offset=self.config.mount_length, origin=(0,0)),
                                           position=position + self.config.mount_length)
        w = w.loft(ruled=True)
        return w

    def build(self) -> cq.Workplane:
        position = self.config.start_position_x

        if self.config.add_mount:
            mount = self._build_wings_mount(position - self.config.mount_length)

        workplane = cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)).center(0, self.config.pipe_border)

        wings_stopper_lower = self._build_wings_sketch(workplane, position=position, height=self.config.blocker_height,
                                                       bottom_height=self.config.blocker_height)
        position += self.config.wing_stopper_length
        wings_stopper_upper = self._build_wings_sketch(
            wings_stopper_lower.workplane(offset=self.config.wing_stopper_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.blocker_height, bottom_height=self.config.blocker_height
        )
        wings_stopper = wings_stopper_upper.loft()

        wings_stopper_gap_lower = self._build_wings_sketch(
            wings_stopper_upper.workplane(offset=0, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.pipe_border
        )
        position += self.config.blocker_gap_length
        wings_stopper_gap_upper = self._build_wings_sketch(
            wings_stopper_gap_lower.workplane(
                offset=self.config.blocker_gap_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.pipe_border
        )
        wings_stopper_gap = wings_stopper_gap_upper.loft()

        wings_ramp_top_lower = self._build_wings_sketch(
            wings_stopper_gap_upper.workplane(origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.wing_height
        )
        position += self.config.wing_ramp_top_length
        wings_ramp_top_upper = self._build_wings_sketch(
            wings_ramp_top_lower.workplane(offset=self.config.wing_ramp_top_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.wing_height
        )
        wings_ramp_top = wings_ramp_top_upper.loft()

        wings_ramp_lower = self._build_wings_sketch(
            wings_ramp_top_upper.workplane(offset=0, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.wing_height
        )
        position += self.config.wing_ramp_length
        wings_ramp_upper = self._build_wings_sketch(
            wings_ramp_lower.workplane(offset=self.config.wing_ramp_length, origin=(0, 0)).center(0, self.config.pipe_border),
            position=position, height=self.config.corner_radius + 5, bottom_height=0
        )
        wings_ramp = wings_ramp_upper.loft()

        wings = cq.Workplane()
        if self.config.add_mount:
            wings = wings.union(mount)
        wings = (wings.union(wings_stopper)
                 .union(wings_stopper_gap)
                 .union(wings_ramp_top)
                 .union(wings_ramp))

        self._built = wings
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# polymoe_cover.py


class CoverConfig:


    def __init__(self, flute_body_config: FluteBodyConfig):
        self._polymoe_config = flute_body_config.config

        self.upper_fillet_radius = self._polymoe_config.cover_upper_fillet_radius
        self.labium_length = self._polymoe_config.cover_labium_length
        self.labium_offset = self._polymoe_config.cover_labium_offset
        self.cut_diameter = self._polymoe_config.cover_cut_diameter
        self.inlay_diameter = self._polymoe_config.cover_inlay_diameter
        self.inlay_depth = self._polymoe_config.cover_inlay_depth
        self.closed_fingerboard_length = self._polymoe_config.cover_closed_fingerboard_length
        self.above_inlay_length = self._polymoe_config.cover_above_inlay_length
        self.stacking_carving_height = self._polymoe_config.cover_stacking_carving_height
        self.stacking_width_correction = self._polymoe_config.cover_stacking_width_correction
        self.density = self._polymoe_config.cover_density

        self._flute_body_config = flute_body_config
        # the cover for the sound chamber is windchannel_offset mm thicker than the rest of the cover in order to place
        # the labium at the center of the windchannel
        self.windchannel_offset = self._flute_body_config.wind_channel_height / 2
        self.height = flute_body_config.pipe_border
        self.width_bottom = flute_body_config.body_width_at_position(0) - self.stacking_width_correction * self.stacking_carving_height
        self.length = flute_body_config.soundpipe_length
        self.width_top = flute_body_config.body_width_at_position(self.length) - self.stacking_width_correction * self.stacking_carving_height
        self.num_pipes = flute_body_config.num_pipes
        self.labium_width = flute_body_config.pipe_width_at_position(self.length) - self.labium_offset
        self.air_hole_length = flute_body_config.air_hole_length
        self.pipe_border = flute_body_config.pipe_border
        self.mouthpiece_length = flute_body_config.mouthpiece_length
        self.mouthpiece_width = flute_body_config.mouthpiece_width - self.stacking_width_correction * self.stacking_carving_height
        self.mouthpiece_front_fillet_radius = flute_body_config.mouthpiece_front_fillet_radius
        self.mouthpiece_knob_size_factor = flute_body_config.mouthpiece_knob_size_factor
        self.slow_airchamber_cover_length = flute_body_config.slow_airchamber_total_length + flute_body_config.wind_channel_length
        self.x_fillets = flute_body_config.x_fillets
        self.yz_fillets = flute_body_config.yz_fillets
        self.extension_cuts = flute_body_config.extension_cuts
        self.stacking_adapter_length = flute_body_config.stacking_carving_length

    def width_at_position(self, position: float) -> float:
        return self.width_bottom \
               - (self.width_bottom - self.width_top) * position / self.length

    def labium_border_width_at_position(self, position: float) -> float:
        return (self.width_at_position(position) - self.num_pipes * self.labium_width) / (self.num_pipes + 1)

    def fingerboard_border_at_position(self, position: float) -> float:
        return (self.width_at_position(position) - self.pipe_border) / (self.num_pipes * 2) - self.cut_diameter / 2
        #return (self.width_at_position(position) - self.cut_diameter * self.num_pipes) / (self.num_pipes * 2)

    def inlay_border_at_position(self, position: float) -> float:
        return (self.width_at_position(position) - self.pipe_border) / (self.num_pipes * 2) - self.inlay_diameter / 2
        #return self.width_at_position(position) / (self.num_pipes * 2) - self.inlay_diameter / 2

    def manual_length(self) -> float:
        return self.length - self.closed_fingerboard_length - self.labium_length

    @property
    def inlay_length(self) -> float:
        return self.length - self.above_inlay_length - self.labium_length



class Cover:
    def __init__(self, config: CoverConfig):
        self.config = config
        self._built = None

    def _build_stacking_adapter_sketch(self, workplane: cq.Workplane, position: float, part_index: int) -> cq.Workplane:
        width = self.config.width_at_position(position) - self.config.pipe_border
        stacking_width = self.config.pipe_border / 2
        x1_step = width / (self.config.num_pipes * 2) - self.config.cut_diameter / 2
        x1_offset = -width / 2
        y1_offset = -self.config.windchannel_offset - self.config.stacking_carving_height
        x2_step = 2 * x1_step
        x2_offset = x1_step + part_index * self.config.cut_diameter + (part_index - 1) * x2_step
        y2_offset = stacking_width

        w = workplane.center(x1_offset, y1_offset)
        if part_index == 0:
            w = w.line(0, stacking_width)
            w = w.line(x1_step, 0)
            w = w.line(0, - stacking_width)
            w = w.close()
        elif part_index < self.config.num_pipes:
            w = w.center(x2_offset, y2_offset)
            w = w.line(x2_step, 0)
            w = w.line(0, -stacking_width)
            w = w.line(-x2_step, 0)
            w = w.close()
            w = w.center(-x2_offset, -y2_offset)
        else:
            w = w.center(x2_offset, y2_offset)
            w = w.line(x1_step, 0)
            w = w.line(0, -stacking_width)
            w = w.line(-x1_step, 0)
            w = w.close()
            w = w.center(-x2_offset, -y2_offset)

        w = w.center(-x1_offset, -y1_offset)

        return w

    def build_stacking_adapter(self, position: float) -> List[cq.Workplane]:
        parts = []
        for part_index in range(self.config.num_pipes + 1):
            lower_sketch = self._build_stacking_adapter_sketch(workplane=cq.Workplane("YZ").workplane(offset=position),
                                                               position=position,
                                                               part_index=part_index)
            upper_sketch = self._build_stacking_adapter_sketch(workplane=lower_sketch.workplane(
                offset=self.config.stacking_adapter_length),
                position=position + self.config.stacking_adapter_length,
                part_index=part_index)
            parts.append(upper_sketch.loft(ruled=False))
        return parts

    def _build_plate_sketch(self, workplane: cq.Workplane, position: float, height_offset = 0) -> cq.Workplane:
        w = workplane.center(-self.config.width_at_position(position) / 2, -height_offset - self.config.stacking_carving_height)
        w = w.line(0, self.config.height - self.config.upper_fillet_radius + height_offset)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
        w = w.line(self.config.width_at_position(position) - 2 * self.config.upper_fillet_radius, 0)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
        w = w.line(0, -self.config.height + self.config.upper_fillet_radius - height_offset)
        w = w.close()
        return w

    def _build_labium_sketch(self, workplane: cq.Workplane, position: float, labium_height) -> cq.Workplane:
        w = workplane.center(-self.config.width_at_position(position) / 2, -self.config.windchannel_offset
                             - self.config.stacking_carving_height)
        w = w.line(0, self.config.height - self.config.upper_fillet_radius + self.config.windchannel_offset)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
        w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
        for pipe_num in range(0, self.config.num_pipes):
            w = w.line(0, -labium_height)
            w = w.line(self.config.labium_width, 0)
            w = w.line(0, labium_height)
            if pipe_num < self.config.num_pipes - 1:
                w = w.line(self.config.labium_border_width_at_position(position), 0)
        w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
        w = w.line(0, -self.config.height + self.config.upper_fillet_radius - self.config.windchannel_offset)
        w = w.close()
        return w

    def build_pitch_carving(self, pipe_num: int, length: float, start_pos: float = 0) -> cq.Workplane:
        w = cq.Workplane("XY").workplane(offset=self.config.height + self.config.windchannel_offset
                                                - self.config.stacking_carving_height, origin=(0, 0))
        y_pos_0 = (self.config.width_at_position(start_pos) / 2 - self.config.pipe_border / 2
                   - self.config.fingerboard_border_at_position(start_pos)
                   - pipe_num * self.config.cut_diameter
                   - self.config.fingerboard_border_at_position(start_pos) * pipe_num * 2
                   )
        w = w.center(start_pos,
                     -y_pos_0
        )
        y_manual_end = (y_pos_0
                        - self.config.width_at_position(length) / 2 + self.config.pipe_border / 2
                        + self.config.fingerboard_border_at_position(length)
                        + pipe_num * self.config.cut_diameter
                        + self.config.fingerboard_border_at_position(length) * pipe_num * 2
                        )
        length = length - start_pos
        w = w.line(length, y_manual_end)

        w = w.line(0, self.config.cut_diameter)
        w = w.line(-length, -y_manual_end)
        w = w.close()
        w = w.extrude(-self.config.height - self.config.upper_fillet_radius - self.config.windchannel_offset)
        return w

    def build_cover_inlay_carving(self, pipe_num: int, length: float) -> cq.Workplane:
        w = cq.Workplane("XY").workplane(offset=self.config.height + self.config.windchannel_offset
                                                - self.config.inlay_depth
                                                - self.config.stacking_carving_height, origin=(0, 0))
        y_pos_0 = (self.config.width_at_position(0) / 2 - self.config.pipe_border / 2
                   - self.config.inlay_border_at_position(0)
                   - pipe_num * self.config.inlay_diameter
                   - self.config.inlay_border_at_position(0) * pipe_num * 2
                   )
        w = w.center(0,
                     -y_pos_0
                     )
        y_manual_end = (y_pos_0
                        - self.config.width_at_position(length) / 2 + self.config.pipe_border / 2
                        + self.config.inlay_border_at_position(length)
                        + pipe_num * self.config.inlay_diameter
                        + self.config.inlay_border_at_position(length) * pipe_num * 2
                        )
        w = w.line(length, y_manual_end)

        w = w.line(0, self.config.inlay_diameter)
        w = w.line(-length, -y_manual_end)
        w = w.close()
        w = w.extrude(-self.config.inlay_depth)
        return w

    def _build_air_hole_wall_sketch(self, workplane: cq.Workplane, position: float, wall_index: int):
        w = workplane.center(-self.config.width_at_position(position) / 2
                             + wall_index * (self.config.labium_border_width_at_position(position) + self.config.labium_width),
                             -self.config.stacking_carving_height)
        if wall_index == 0: # leftmost, rounded wall
            w = w.line(0, self.config.height - self.config.upper_fillet_radius)
            w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
            w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
            w = w.line(0, -self.config.height)
            w = w.close()
        elif wall_index == self.config.num_pipes: # rightmost, rounded wall
            w = w.line(0, self.config.height)
            w = w.line(self.config.labium_border_width_at_position(position) - self.config.upper_fillet_radius, 0)
            w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
            w = w.line(0, -self.config.height + self.config.upper_fillet_radius)
            w = w.close()
        else: # straight middle walls
            w = w.line(0, self.config.height)
            w = w.line(self.config.labium_border_width_at_position(position), 0)
            w = w.line(0, -self.config.height)
            w = w.close()
        return w

    #def _build_slow_airchamber_cover_sketch(self, workplane: cq.Workplane, position: float) -> cq.Workplane:
    #    pass

    def build_mouthpiece_cover_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = workplane.center(-self.config.mouthpiece_width / 2, -self.config.stacking_carving_height)
        w = w.line(0, self.config.height - self.config.upper_fillet_radius)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, self.config.upper_fillet_radius))
        w = w.line(self.config.mouthpiece_width - 2 * self.config.upper_fillet_radius, 0)
        w = w.tangentArcPoint((self.config.upper_fillet_radius, -self.config.upper_fillet_radius))
        w = w.line(0, -self.config.height + self.config.upper_fillet_radius)
        w = w.close()
        return w

    def _build_soundpipe_cover(self, position: float, length: float) -> cq.Workplane:
        plate_lower = self._build_plate_sketch(cq.Workplane('YZ').workplane(offset=position), position,
                                               height_offset=self.config.windchannel_offset)
        position += length
        plate_upper = self._build_plate_sketch(plate_lower.workplane(offset=length,
                                                                     origin=(0, 0)),
                                               position, height_offset=self.config.windchannel_offset)
        plate = plate_upper.loft()
        return plate

    def build(self) -> List[cq.Workplane]:
        if self._built is not None:
            return self._built

        position = 0
        pitch_carving = []
        inlay_carving = []
        for pipe_num in range(self.config.num_pipes):
            pitch_carving.append(self.build_pitch_carving(pipe_num, start_pos=0, length=self.config.inlay_length))
            inlay_carving.append(self.build_cover_inlay_carving(pipe_num, length=self.config.inlay_length))
        sound_pipe_extensions = []
        remaining_soundpipe_cover = self.config.length
        for extension_length in self.config.extension_cuts:
            if self.config.stacking_adapter_length != 0:
                stacking_adapters = self.build_stacking_adapter(position=position - self.config.stacking_adapter_length)
            knob_config_upper = KnobConfig(config=self.config._polymoe_config,
                                           x_position=position,
                                           y_position=self.config.width_at_position(position),
                                           z_position=self.config.windchannel_offset,
                                           direction=KnobDirection.RightUpper,
                                           side=KnobSide.Left)
            cord_knob_upper_left = Knob(knob_config_upper).build()
            knob_config_upper.side = KnobSide.Right
            cord_knob_upper_right = Knob(knob_config_upper).build()
            extension = cq.Workplane().union(self._build_soundpipe_cover(position, length=extension_length), clean=True)
            position += extension_length
            knob_config_lower = KnobConfig(config=self.config._polymoe_config,
                                           x_position=position,
                                           y_position=self.config.width_at_position(position),
                                           z_position=self.config.windchannel_offset,
                                           direction=KnobDirection.LeftUpper,
                                           side=KnobSide.Left)
            cord_knob_lower_left = Knob(knob_config_lower).build()
            knob_config_lower.side = KnobSide.Right
            cord_knob_lower_right = Knob(knob_config_lower).build()
            extension = extension.union(cord_knob_upper_right, clean=True)
            extension = extension.union(cord_knob_upper_left, clean=True)
            extension = extension.union(cord_knob_lower_right, clean=True)
            extension = extension.union(cord_knob_lower_left, clean=True)
            if self.config.stacking_adapter_length != 0:
                for stacking_adapter in stacking_adapters:
                    extension = extension.union(stacking_adapter, clean=True)
            #for pipe_num in range(self.config.num_pipes):
            #    extension = extension.cut(pitch_carving[pipe_num])
            #    extension = extension.cut(inlay_carving[pipe_num])
            sound_pipe_extensions.append(extension)
            remaining_soundpipe_cover -= extension_length
        remaining_soundpipe_cover -=  self.config.x_fillets * 2 + self.config.labium_length
        if self.config.stacking_adapter_length != 0:
            stacking_adapters = self.build_stacking_adapter(position=position - self.config.stacking_adapter_length)
        knob_config_upper = KnobConfig(config=self.config._polymoe_config,
                                       x_position=position,
                                       y_position=self.config.width_at_position(position),
                                       z_position=self.config.windchannel_offset,
                                       direction=KnobDirection.RightUpper,
                                       side=KnobSide.Left)
        cord_knob_left = Knob(knob_config_upper).build()
        knob_config_upper.side = KnobSide.Right
        cord_knob_right = Knob(knob_config_upper).build()
        plate = self._build_soundpipe_cover(position, length=remaining_soundpipe_cover)
        if self.config.stacking_adapter_length != 0:
            for stacking_adapter in stacking_adapters:
                plate = plate.union(stacking_adapter, clean=True)
        position += remaining_soundpipe_cover

        labium_lower = self._build_labium_sketch(cq.Workplane('YZ').workplane(offset=position, origin=(0,0)),
                                                 position, labium_height=0.01)
        position += self.config.labium_length
        labium_upper = self._build_labium_sketch(labium_lower.workplane(offset=self.config.labium_length, origin=(0,0)),
                                                 position,
                                                 labium_height=self.config.height + self.config.windchannel_offset)
        labium = labium_upper.loft()
        for extension_index in range(len(sound_pipe_extensions)):
            for pipe_num in range(self.config.num_pipes):
                sound_pipe_extensions[extension_index] = sound_pipe_extensions[extension_index].cut(pitch_carving[pipe_num])
                sound_pipe_extensions[extension_index] = sound_pipe_extensions[extension_index].cut(inlay_carving[pipe_num])
        for pipe_num in range(self.config.num_pipes):
            plate = plate.cut(pitch_carving[pipe_num])
            plate = plate.cut(inlay_carving[pipe_num])

        air_hole_walls = []
        for wall_index in range(0, self.config.num_pipes + 1):
            air_hole_wall_lower = self._build_air_hole_wall_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0,0)), position=position, wall_index=wall_index)
            air_hole_wall_upper = self._build_air_hole_wall_sketch(air_hole_wall_lower.workplane(
                offset=self.config.air_hole_length, origin=(0,0)), position=position+self.config.air_hole_length, wall_index=wall_index)
            air_hole_walls.append(air_hole_wall_upper.loft())
        position += self.config.air_hole_length

        slow_airchamber_cover_lower = self._build_plate_sketch(cq.Workplane('YZ').workplane(offset=position, origin=(0,0)), position = position)
        position += self.config.slow_airchamber_cover_length
        slow_airchamber_cover_upper = self._build_plate_sketch(
                workplane= slow_airchamber_cover_lower.workplane(
                offset=self.config.slow_airchamber_cover_length, origin=(0, 0)), position=position)
        slow_airchamber_cover = slow_airchamber_cover_upper.loft()

        if self.config.x_fillets:
            pipe_to_mouthpiece__connection_1 = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0,0)), position = position)
            position += 0.1
            pipe_to_mouthpiece__connection_2 = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 - 0.1
            pipe_to_mouthpiece__connection_3 = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 * 6 - 0.1
            pipe_to_mouthpiece__connection_4 = self.build_mouthpiece_cover_sketch(
                workplane= cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            position += 0.1
            pipe_to_mouthpiece__connection_5 = self.build_mouthpiece_cover_sketch(
                workplane=cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
            mouthpiece1 = continuous_connection(
                pipe_to_mouthpiece__connection_1.wires().val(),
                pipe_to_mouthpiece__connection_5.wires().val(),
                invert_normals=True,
                middle_wires=[pipe_to_mouthpiece__connection_2.wires().val(),
                              pipe_to_mouthpiece__connection_3.wires().val(),
                              pipe_to_mouthpiece__connection_4.wires().val()]
            )
        else:
            pipe_to_mouthpiece__connection_lower = self._build_plate_sketch(
                cq.Workplane('YZ').workplane(offset=position, origin=(0, 0)), position=position)
            position += self.config.mouthpiece_length / 8 * 7
            pipe_to_mouthpiece__connection_upper = self.build_mouthpiece_cover_sketch(
                workplane=pipe_to_mouthpiece__connection_lower.workplane(
                    offset=self.config.mouthpiece_length / 8 * 7, origin=(0, 0)))
            mouthpiece1 = pipe_to_mouthpiece__connection_upper.loft()

        mouthpiece_middle2 = self.build_mouthpiece_cover_sketch(
            cq.Workplane("YZ").workplane(offset=position, origin=(0, 0)))
        mouthpiece_upper = self.build_mouthpiece_cover_sketch(
            mouthpiece_middle2.workplane(
                offset=self.config.mouthpiece_length / 8 * 1, origin=(0, 0)))
        mouthpiece2 = mouthpiece_upper.loft()
        position += self.config.mouthpiece_length / 8 * 1

        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=position,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.LeftUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()

        top_cover = plate.union(labium, clean=True)\
            .union(cord_knob_left, clean=True)\
            .union(cord_knob_right, clean=True)
        for air_hole_wall in air_hole_walls:
            top_cover = top_cover.union(air_hole_wall, clean=True)\
                .union(slow_airchamber_cover, clean=True)\
                .union(mouthpiece1, clean=True)\
                .union(mouthpiece2, clean=True)\
                .union(cord_knob_mouthpiece_left, clean=True)\
                .union(cord_knob_mouthpiece_right, clean=True)

        self._built = sound_pipe_extensions
        self._built.append(top_cover)
        if self.config.stacking_adapter_length != 0:
            for index in range(0, len(self._built) - 1):
                self._built[index] = self._built[index].cut(self._built[index + 1], clean=False)  # TODO: find out why clean fails here
        return self._built

    def carve_body_stacking(self, body_with_extensions: List[cq.Workplane]) -> cq.Workplane:
        if self._built is None:
            self.build()
        if self.config.stacking_carving_height != 0:
            for index, part in enumerate(self._built):
                self._built[index] = part.cut(body_with_extensions[index], clean=False)  # TODO: find out why clean fails here
        return self._built


    def carve_mouthpiece_stacking(self, mouthpiece_cover_obj: cq.Workplane) -> cq.Workplane:
        if self._built is None:
            self.build()
        if self.config.stacking_adapter_length != 0:
            self._built[-1] = self._built[-1].cut(mouthpiece_cover_obj, clean=False)  # TODO: find out why clean fails here
        return self._built

    # def _artifact_remover_sketch(self, position, width, left_side):
    #     side = -1 if left_side else 1
    #     sketch = (cq.Workplane("YZ").workplane(offset=position)
    #               .center((width / 2 - self.config.pipe_border / 2) * side, 0)
    #               .line(self.config.pipe_border * side, 0)
    #               .line(0, -20)
    #               .line(-self.config.pipe_border * side, 0)
    #               .close())
    #     return sketch

    # # the mouthpiece curvature of the cover and body isn't exactly the same, so some artifacts remain when subtracting
    # # the body from the cover via carve_body_stacking. As fixing this divergence isn't easy the current workaround is
    # # to remove the artifacts
    # def remove_artifacts(self):
    #     position = self.config.length + self.config.slow_airchamber_cover_length + self.config.air_hole_length - 5
    #     #left_0 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     #right_0 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position),left_side=False)
    #     position += 5
    #     left_1 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     right_1 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=False)
    #     position += 0.1
    #     left_2 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     right_2 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=False)
    #     position += self.config.mouthpiece_length / 8 - 0.1
    #     left_3 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=True)
    #     right_3 = self._artifact_remover_sketch(position, width=self.config.width_at_position(position), left_side=False)
    #     position += self.config.mouthpiece_length / 8 * 6 - 0.1
    #     left_4 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=True)
    #     right_4 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=False)
    #     position += 0.1
    #     left_5 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=True)
    #     right_5 = self._artifact_remover_sketch(position, width=self.config.mouthpiece_width, left_side=False)
    #     left_block = continuous_connection(left_1.wires().val(), left_5.wires().val(), invert_normals=False,
    #                                        middle_wires=[left_2.wires().val(), left_3.wires().val(), left_4.wires().val()])
    #     right_block = continuous_connection(right_1.wires().val(), right_5.wires().val(), invert_normals=False,
    #                                         middle_wires=[right_2.wires().val(), right_3.wires().val(), right_4.wires().val()])
    #     #left_miniblock = continuous_connection(left_0.wires().val(), left_1.wires().val(), invert_normals=False)
    #     #right_miniblock = continuous_connection(right_0.wires().val(), right_1.wires().val(), invert_normals=False)
    #     if self._built is None:
    #         self.build()
    #     self._built[-1] = self._built[-1].cut(left_block, clean=False)
    #     self._built[-1] = self._built[-1].cut(right_block, clean=False)
    #     # removing the mini face-only artifact doesn't seem to work
    #     #self._built[-1] = self._built[-1].cut(left_miniblock, clean=False)
    #     #self._built[-1] = self._built[-1].cut(right_miniblock, clean=False)
    #     return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# polymoe_stringplate.py

class StringPlateConfig(WingsConfig):

    def __init__(self, flute_body_config: FluteBodyConfig, cover_config: CoverConfig):
        super().__init__(flute_body_config)

        config = flute_body_config.config

        self.distance_from_manual = config.stringplate_distance_from_manual

        self.start_position_x = cover_config.manual_length() + self.distance_from_manual
        self.wing_height = config.stringplate_height
        self.wing_stopper_length = config.stringplate_stopper_length
        self.wing_ramp_length = config.stringplate_ramp_length
        self.wing_ramp_top_length = config.stringplate_ramp_top_length
        self.blocker_height = config.stringplate_blocker_height
        #self.blocker_gap_length = 5
        #self.corner_radius = 2
        self.density = config.stringplate_density
        self.add_mount = False


# polymoe_pitch_strips.py

# hole_positions:
#   * None: completely open - continuous
#   * list of cent values - hole at given cent position per octave
#   * empty list: no holes - bourdon pipe
HolePositions = Union[List[int], None]

class PitchStripConfig:

    @staticmethod
    def continuous(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int):
        config = PitchStripConfig(body_config, cover_config, hole_positions = None, pipe_index = pipe_index)
        return config

    @staticmethod
    def chromatic(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions = [100 for i in range(12)], pipe_index = pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def diatonic(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions = [200, 200, 100, 200, 200, 200, 100], pipe_index = pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def pentatonic(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions = [300, 200, 200, 300, 200], pipe_index = pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def octaves(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int, frequency_of_lowest_hole: float):
        config = PitchStripConfig(body_config, cover_config, hole_positions=[1200], pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        return config

    @staticmethod
    def bourdon(body_config: FluteBodyConfig, cover_config: CoverConfig, pipe_index: int):
        config = PitchStripConfig(body_config, cover_config, hole_positions=[], pipe_index=pipe_index)
        return config

    def __init__(self, body_config: FluteBodyConfig, cover_config: CoverConfig, hole_positions: HolePositions, pipe_index: int, frequency_of_lowest_hole: float=None):
        self.polymoe_config = body_config.config

        self.density = self.polymoe_config.pitchstrip_density
        self.continuous_if_hole_distance_smaller_than = self.polymoe_config.pitchstrip_continuous_if_hole_distance_smaller_than
        self.hole_radius = self.polymoe_config.pitchstrip_hole_radius

        self.hole_positions = hole_positions
        self.frequency_of_lowest_hole=frequency_of_lowest_hole
        self.pipe_index = pipe_index
        self._cover_config = cover_config
        self.cover = Cover(self._cover_config)
        self.height = cover_config.height
        self.windchannel_offset = cover_config.windchannel_offset
        self.cut_diameter = cover_config.cut_diameter
        self.upper_fillet_radius = cover_config.upper_fillet_radius
        self.manual_length = cover_config.manual_length()
        self.inlay_depth = cover_config.inlay_depth
        self.inlay_length = cover_config.inlay_length
        self.soundpipe_length = body_config.soundpipe_length
        self.pipe_border = body_config.pipe_border

    def width_at_position(self, position: float):
        return self._cover_config.width_at_position(position)

    def fingerboard_border_at_position(self, position: float):
        return self._cover_config.fingerboard_border_at_position(position)


class PitchStrip:
    def __init__(self, config: PitchStripConfig):
        self.config = config
        self._built = None

    def _build_cover_inlay_carving(self, pipe_num: int) -> cq.Workplane:
        return self.config.cover.build_cover_inlay_carving(pipe_num, length=self.config.inlay_length)

    def _build_continuous_pitch_carving(self, pipe_num, continuous_start_pos: float) -> cq.Workplane:
        return self.config.cover.build_pitch_carving(pipe_num, length=self.config.manual_length, start_pos=continuous_start_pos)

    def _carve_hole(self, hole_position: float) -> cq.Workplane:
        w = cq.Workplane("XY").workplane(offset=self.config.height + self.config.windchannel_offset, origin=(0, 0))
        y_pos_0 = (self.config.width_at_position(hole_position) / 2 - self.config.pipe_border / 2
                   - self.config.fingerboard_border_at_position(hole_position)
                   - self.config.pipe_index * self.config.cut_diameter
                   - self.config.fingerboard_border_at_position(hole_position) * self.config.pipe_index * 2
                   - self.config.cut_diameter / 2
                   )
        w = w.center(hole_position,
                     -y_pos_0
                     ).circle(self.config.hole_radius).extrude(-self.config.inlay_depth * 3)
        return w

    def _hole_position_for_frequency(self, frequency_hz: float):
        speed_of_sound_mps = 343.2
        wavelength_m = speed_of_sound_mps / frequency_hz
        open_pipe_length_m = wavelength_m / 2
        position_in_mm = self.config.soundpipe_length - open_pipe_length_m * 1000
        return position_in_mm

    def _next_hole_position(self, current_hole_position: float, cent_offset: int) -> float:
        manual_length = self.config.manual_length
        remaining_manual = manual_length - current_hole_position
        if current_hole_position == 0:
            position = self._hole_position_for_frequency(self.config.frequency_of_lowest_hole)
        else:
            if cent_offset != 0:
                frequency_ratio = (2 ** (cent_offset / 1200))
                position = current_hole_position + remaining_manual * (1 - 1 / frequency_ratio)
            else:
                position = current_hole_position
        return position


    def build(self) -> cq.Workplane:
        self._built = self._build_cover_inlay_carving(self.config.pipe_index)
        current_hole_position = 0
        if self.config.hole_positions is None or len(self.config.hole_positions) != 0: # not bourdon
            if self.config.hole_positions is not None:
                hole_cent_index = 0
                current_hole_position = self._next_hole_position(0, 0)
                last_hole_position = -1000
                while current_hole_position - last_hole_position > self.config.continuous_if_hole_distance_smaller_than \
                    and current_hole_position < self.config.manual_length:
                    self._built = self._built.cut(self._carve_hole(current_hole_position))
                    last_hole_position = current_hole_position
                    cent_offset = self.config.hole_positions[hole_cent_index]
                    current_hole_position = self._next_hole_position(current_hole_position, cent_offset)
                    hole_cent_index = (hole_cent_index + 1) % len(self.config.hole_positions)
            self._built = self._built.cut(self._carve_hole(current_hole_position))
            self._built = self._built.cut(self._build_continuous_pitch_carving(self.config.pipe_index,
                                                                               continuous_start_pos=current_hole_position))
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# polymoe_mouthpiece_coverplate.py

class MouthpieceCoverplateConfig:
    def __init__(self, flute_body_config: FluteBodyConfig, cover_config: CoverConfig):
        self._polymoe_config = flute_body_config.config

        self.density = self._polymoe_config.mouthpiece_coverplate_density
        self.length = self._polymoe_config.mouthpiece_coverplate_length
        self.flute_body_config = flute_body_config
        self.cover_config = cover_config
        self.mouthpiece_front_fillet_radius = flute_body_config.mouthpiece_front_fillet_radius
        self.mouthpiece_fillet_radius = flute_body_config.mouthpiece_fillet_radius
        self.mouthpiece_knob_size_factor = flute_body_config.mouthpiece_knob_size_factor
        self.startpos = flute_body_config.total_length - flute_body_config.pipe_x__back_fillet_radius
        self.use_curved_mouthpiece = self._polymoe_config.use_curved_mouthpiece
        self.stacking_carving_length = flute_body_config.stacking_carving_length
        self.mouthpiece_pipe_width = flute_body_config.mouthpiece_pipe_width
        self.mouthpiece_pipe_height = flute_body_config.mouthpiece_pipe_height
        self.mouthpiece_outer_border = flute_body_config.mouthpiece_outer_border
        self.mouthpiece_inner_border = flute_body_config.mouthpiece_inner_border
        self.mouthpiece_width = flute_body_config.mouthpiece_width
        self.mouthpiece_height = flute_body_config.mouthpiece_height
        self.mouthpiece_bottom_border = flute_body_config.mouthpiece_bottom_border
        self.stacking_carving_height = cover_config.stacking_carving_height
        self.curvedmouthpiece_length1 = self._polymoe_config.curvedmouthpiece_length1
        self.curvedmouthpiece_length2 = self._polymoe_config.curvedmouthpiece_length2
        self.curvedmouthpiece_length3 = self._polymoe_config.curvedmouthpiece_length3
        self.curvedmouthpiece_length4 = self._polymoe_config.curvedmouthpiece_length4
        self.mouthpiece_coverplate_length = self._polymoe_config.mouthpiece_coverplate_length


class MouthpieceCoverplate:
    def __init__(self, config: MouthpieceCoverplateConfig):
        self._built = None
        self.config = config
        self.flute_body = FluteBody(config.flute_body_config)
        self.cover = Cover(config.cover_config)

    def _build_mouth_piece(self, workplane: cq.Workplane, extra_height: float):
        w = self.flute_body.build_mouth_piece(workplane, extra_height)
        w = w.center(self.config.flute_body_config.mouthpiece_width / 2,
                     self.config.flute_body_config.mouthpiece_width - self.config.flute_body_config.pipe_yz_fillet_radius)
        return w

    def build_stacking_carving_body(self, position: float):
        body_stacking_body_lower = self.flute_body.build_width_height_pipe_sketch(
            workplane=cq.Workplane("YZ").workplane(offset=position),
            width=self.config.mouthpiece_width - self.config.mouthpiece_outer_border,
            height=self.config.mouthpiece_height,
            pipe_width=self.config.mouthpiece_pipe_width,
            outer_border_width=self.config.mouthpiece_outer_border / 2,
            inner_border_width=self.config.mouthpiece_inner_border,
            border_height_delta=0,
            bottom_height=self.config.mouthpiece_height,
            bottom_fillet_radius=self.config.mouthpiece_fillet_radius)
        body_stacking_body_upper = self.flute_body.build_width_height_pipe_sketch(
            workplane=body_stacking_body_lower.workplane(offset=self.config.stacking_carving_length, origin=(0, 0)),
            width=self.config.mouthpiece_width - self.config.mouthpiece_outer_border,
            height=self.config.mouthpiece_height,
            pipe_width=self.config.mouthpiece_pipe_width,
            outer_border_width=self.config.mouthpiece_outer_border / 2,
            inner_border_width=self.config.mouthpiece_inner_border,
            border_height_delta=0,
            bottom_height=self.config.mouthpiece_height,
            bottom_fillet_radius=self.config.mouthpiece_fillet_radius)
        body_stacking = body_stacking_body_upper.loft(ruled=True)
        return body_stacking

    def build_stacking_carving_cover(self, position: float):
        cover_stacking = cq.Workplane("YZ").workplane(offset=position)\
            .center(0, self.config.mouthpiece_bottom_border / 2)\
            .rect(self.config.mouthpiece_width - self.config.mouthpiece_outer_border,
                 self.config.mouthpiece_bottom_border + self.config.stacking_carving_height * 2)\
            .extrude(self.config.stacking_carving_length)
        return cover_stacking

    def build_stacking_carving(self, position: float):
        stacking_position = position - self.config.stacking_carving_length
        body_stacking = self.build_stacking_carving_body(stacking_position)
        cover_stacking = self.build_stacking_carving_cover(stacking_position)
        return body_stacking.union(cover_stacking)

    def build(self):
        position = self.config.startpos

        if self.config.stacking_carving_length != 0:
            stacking = self.build_stacking_carving(position)

        mouthpiece_fillet_sketch = self.flute_body.build_mouth_piece(
            cq.Workplane("YZ").workplane(offset=position),
            extra_height=0)
        mouthpiece_base_lower = mouthpiece_fillet_sketch.extrude(self.config.length)
        mouthpiece_fillet_sketch = self.cover.build_mouthpiece_cover_sketch(
            cq.Workplane("YZ").workplane(offset=position))
        mouthpiece_base_upper = mouthpiece_fillet_sketch.extrude(self.config.length)

        knob_config_mouthpiece = KnobConfig(self.config._polymoe_config, x_position=position,
                                            y_position=self.config.flute_body_config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.Right,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.z_position += self.config.flute_body_config.mouthpiece_height
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()

        position += self.config.length

        mouthpiece_fillet_lower_sketch = self.flute_body.build_mouth_piece(
            cq.Workplane("YZ").workplane(offset=position),
            extra_height=self.config.mouthpiece_front_fillet_radius * 2)
        if self.config.mouthpiece_front_fillet_radius > 0:
            mouthpiece_lower_fillet = continuous_endpiece(mouthpiece_fillet_lower_sketch.wires().val(),
                                                          self.config.mouthpiece_front_fillet_radius,
                                                          invert_normals=True)
        else:
            mouthpiece_lower_fillet = mouthpiece_fillet_lower_sketch.extrude(self.config._polymoe_config.mouthpiece_front_fillet_radius)
        # position += self.config.mouthpiece_front_fillet_radius

        mouthpiece_fillet_upper_sketch = self.cover.build_mouthpiece_cover_sketch(
            cq.Workplane("YZ").workplane(offset=position))
        if self.config.mouthpiece_front_fillet_radius > 0:
            mouthpiece_upper_fillet = continuous_endpiece(mouthpiece_fillet_upper_sketch.wires().val(),
                                                          self.config.mouthpiece_front_fillet_radius,
                                                          invert_normals=True)
        else:
            mouthpiece_upper_fillet = mouthpiece_fillet_upper_sketch.extrude(self.config._polymoe_config.mouthpiece_front_fillet_radius)

        #position += self.config.mouthpiece_front_fillet_radius

        coverplate = cq.Workplane().union(mouthpiece_base_lower, clean=True).union(mouthpiece_base_upper, clean=True)
        if self.config.stacking_carving_length != 0:
            coverplate = coverplate.union(stacking, clean=True)
        coverplate = (coverplate.union(mouthpiece_lower_fillet, clean=True)
            .union(mouthpiece_upper_fillet, clean=True, tol=0.2)
            .union(cord_knob_mouthpiece_left, clean=True).union(cord_knob_mouthpiece_right, clean=True)
            .union(cord_knob_mouthpiece_left_down, clean=True).union(cord_knob_mouthpiece_right_down, clean=True))

        self._built = coverplate
        return self._built

    def move_coverplate_behind_curved_mouthpiece(self):
        if self._built is None:
            self.build()
        angle = 90
        mouthpiece_cover_height = self.config.flute_body_config.mouthpiece_height\
                                  + self.config.flute_body_config.mouthpiece_outer_border\
                                  + self.config.cover_config.height
        offset = cq.Vector(self.config.curvedmouthpiece_length1 - self.config.curvedmouthpiece_length3
                           - self.config.cover_config.height
                           - self.config.mouthpiece_coverplate_length
                           -0.128, # TODO where is this constant coming from (wrong positioning of rotation axis)?
                           0,
                           -self.config.curvedmouthpiece_length2 - self.config.curvedmouthpiece_length4
                           - mouthpiece_cover_height / 2
                           -self.config.length
                           + 3.8   # TODO where is this constant coming from (wrong positioning of rotation axis)?
        )
        position = self.config.startpos + self.config.length
        self._built = self._built.rotate(axisStartPoint=cq.Vector(position, -1, 0), axisEndPoint=cq.Vector(position, 1, 0), angleDegrees=angle).translate(offset)
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# polymoe_carry_hook.py

class CarryHookConfig:
    def __init__(self, body_config: FluteBodyConfig, stringplate_config: StringPlateConfig):
        self._body_config = body_config

        config = body_config.config

        self.density = config.hook_density
        self.blocker_gap_length = stringplate_config.blocker_gap_length
        self.gap_depth = config.hook_gap_depth
        self.blocker_length = config.hook_blocker_length
        self.start_position_x = stringplate_config.start_position_x + stringplate_config.wing_stopper_length + self.blocker_gap_length + self.blocker_length
        self.end_position_on_pipe_x = self.start_position_x - config.hook_on_pipe_length
        self.start_position_z = -body_config.body_height_at_position(self.start_position_x) + config.body_hook_stacking_depth
        self.end_position_on_pipe_z = -body_config.body_height_at_position(self.end_position_on_pipe_x)
        self.upper_part_length = config.hook_upper_part_length
        self.radius = config.hook_radius
        self.upper_end_position_z = -body_config.body_height_at_position(self.end_position_on_pipe_x - self.upper_part_length) - self.radius
        self.thickness = config.hook_thickness
        self.fillet_radius = config.hook_fillet_radius
        self.body_fillet_radius = body_config.yz_fillets
        self.use_fillets = body_config.fillets
        self.width = config.hook_width if config.hook_width is not None else body_config.body_width_at_position(self.start_position_x) - self._body_config.pipe_yz_fillet_radius * 2
        length_between_points_on_border_xy = math.sqrt(1 + (self.body_width_at_position(self.start_position_x) / 2 - self.body_width_at_position(self.start_position_x - 1) / 2) ** 2)
        length_between_points_on_border_xz = math.sqrt(1 + (self.body_height_at_position(self.start_position_x) / 2 - self.body_height_at_position(self.start_position_x - 1) / 2) ** 2)
        self.rotation_angle_y = config.hook_rotation_angle_y if config.hook_rotation_angle_y is not None else math.acos(1 / length_between_points_on_border_xz) / math.pi * 180
        self.rotation_angle_z = config.hook_rotation_angle_z if config.hook_rotation_angle_z is not None else math.acos(1 / length_between_points_on_border_xy) / math.pi * 180

    def body_height_at_position(self, position: float):
        return self._body_config.body_height_at_position(position)

    def body_width_at_position(self, position: float):  # TODO: let width depend on body width
        return self._body_config.body_width_at_position(position)

class CarryHook:
    def __init__(self, config: CarryHookConfig):
        self.config = config
        self._built = None

    def _build_hook_sketch(self, workplane: cq.Workplane = None, rotation_angle:float = 0) -> cq.Workplane:
        z_position_at_gap = self.config.body_height_at_position(self.config.start_position_x - self.config.blocker_length)
        if workplane is None:
            workplane = cq.Workplane("XZ").workplane(offset=-self.config.width / 2)
        w = (workplane
             .center(self.config.start_position_x, self.config.start_position_z).transformed(rotate=(0, rotation_angle, 0))
             .line(self.config.end_position_on_pipe_x - self.config.start_position_x, self.config.end_position_on_pipe_z - self.config.start_position_z)
             .line(0, -self.config.thickness / 2)
             .tangentArcPoint((-self.config.radius, -self.config.radius))
             #.line(- self.config.upper_part_length, self.config.upper_end_position_z - self.config.end_position_on_pipe_z + self.config.radius)
             .line(- self.config.upper_part_length, 0)
             .line(0, -self.config.thickness)
             .line(self.config.upper_part_length + self.config.thickness, -(self.config.upper_end_position_z - self.config.end_position_on_pipe_z + self.config.radius))
             .tangentArcPoint((self.config.radius, self.config.radius))
             .line(-self.config.end_position_on_pipe_x + self.config.start_position_x - self.config.thickness - self.config.blocker_gap_length - self.config.blocker_length, 0)
             .line(0, self.config.gap_depth)
             .line(self.config.blocker_gap_length, 0)
             .line(0, -self.config.gap_depth)
             .line(self.config.blocker_length, 0)
             .close()
         )
        return w

    def build(self) -> cq.Workplane:
        left_sketch = self._build_hook_sketch(rotation_angle=-self.config.rotation_angle_z)
        right_sketch = self._build_hook_sketch(left_sketch.transformed(rotate=(0,self.config.rotation_angle_z,0)).workplane(offset=self.config.width,origin=(0,0)), rotation_angle=self.config.rotation_angle_z)
        self._built = right_sketch.loft(ruled=True)
        if self.config.use_fillets:
            self._built = self._built.edges().fillet(self.config.fillet_radius)
            #fillet = continuous_endpiece(self._build_hook_sketch().wires().val(), self.config.fillet_radius, invert_normals=True)

        self._built = self._built.rotate(axisStartPoint=cq.Vector(self.config.start_position_x, 0, self.config.start_position_z),
                                         axisEndPoint=cq.Vector(self.config.start_position_x, 1, self.config.start_position_z),
                                         angleDegrees=self.config.rotation_angle_y)

        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for shape in self._built.all():
            volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight


# polymoe_curved_mouthpiece.py

class CurvedMouthpieceConfig:
    def __init__(self, body_config: FluteBodyConfig, cover_config: CoverConfig,
                 mouthpiece_coverplate_config: MouthpieceCoverplateConfig):
        self._polymoe_config = body_config.config

        self.density = self._polymoe_config.curvedmouthpiece_density
        self.body_config = body_config
        self.cover_config = cover_config
        self.mouthpiece_coverplate_config = mouthpiece_coverplate_config
        self.start_pos = cq.Vector(body_config.total_length - self.body_config.pipe_x__back_fillet_radius, 0, 0)
        self.mouthpiece_length1 = self._polymoe_config.curvedmouthpiece_length1
        self.mouthpiece_length2 = self._polymoe_config.curvedmouthpiece_length2
        self.mouthpiece_length3 = self._polymoe_config.curvedmouthpiece_length3
        self.mouthpiece_length4 = self._polymoe_config.curvedmouthpiece_length4
        self.mouthpiece_height = body_config.mouthpiece_height + body_config.mouthpiece_outer_border
        self.mouthpiece_width = body_config.mouthpiece_width
        self.mouthpiece_knob_size_factor = body_config.mouthpiece_knob_size_factor
        self.round_corners = self._polymoe_config.curvedmouthpiece_round_corners
        self.stacking_carving_length = body_config.stacking_carving_length


class CurvedMouthpiece:
    class _Shape(Enum):
        body = 0
        cover = 1

    def __init__(self, config: CurvedMouthpieceConfig):
        self.config = config
        self._built = None
        self.body = FluteBody(self.config.body_config)
        self.cover = Cover(self.config.cover_config)
        self.mouthpiece_coverplate = MouthpieceCoverplate(self.config.mouthpiece_coverplate_config)

    def _build_body_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = self.body.build_mouth_piece(workplane)
        return w


    def _build_cover_sketch(self, workplane: cq.Workplane) -> cq.Workplane:
        w = self.cover.build_mouthpiece_cover_sketch(workplane=workplane)
        return w

    def _build_stacking_adapter(self, position: float, shape: _Shape) -> cq.Workplane:
        if shape == CurvedMouthpiece._Shape.body:
            return self.mouthpiece_coverplate.build_stacking_carving_body(position)
        elif shape == CurvedMouthpiece._Shape.cover:
            return self.mouthpiece_coverplate.build_stacking_carving_cover(position)
        else:
            raise Exception(f"unecpacted shape {shape}")

    def _build_sketch(self, workplane: cq.Workplane, shape: _Shape) -> cq.Workplane:
        if shape == CurvedMouthpiece._Shape.body:
            return self._build_body_sketch(workplane)
        elif shape == CurvedMouthpiece._Shape.cover:
            return self._build_cover_sketch(workplane)
        else:
            raise Exception(f"unexpected shape {shape}")

    def _recenter(self, workplane: cq.Workplane, shape: _Shape):
        if shape == CurvedMouthpiece._Shape.body:
            w = workplane.center(self.config.body_config.mouthpiece_width / 2,
                         self.config.body_config.mouthpiece_height - self.config.body_config.pipe_yz_fillet_radius)
        elif shape == CurvedMouthpiece._Shape.cover:
            w = workplane.center(self.config.cover_config.mouthpiece_width / 2, 1)
        else:
            raise Exception(f"unexpected shape {shape}")
        return w

    def _build_mouthpiece_part(self, shape: _Shape) -> cq.Workplane:
        NINETY_DEGREE = 89.99 # 90 TODO: why does 90° not work...
        position_x = self.config.start_pos.x
        position_z = 0
        if self.config.stacking_carving_length != 0:
            stacking = self._build_stacking_adapter(position_x - self.config.stacking_carving_length, shape)
        w = cq.Workplane("YZ").workplane(offset=position_x, origin=(0,0))
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length1, origin=(0, 0))
        w = self._build_sketch(w, shape=shape)
        part = w.loft(ruled=True)
        if self.config.stacking_carving_length != 0:
            part = part.union(stacking, clean=True)
        position_x += self.config.mouthpiece_length1

        if self.config.round_corners:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x, origin=(0, 0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch1_centered = self._recenter(sketch1, shape=shape)
            part = part.union(sketch1_centered.revolve(NINETY_DEGREE,axisStart=cq.Vector(0,-self.config.mouthpiece_height,0) ,axisEnd=cq.Vector(1,-self.config.mouthpiece_height,0)), clean=True)
        else:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x, origin=(0,0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch2 = self._recenter(sketch1, shape=shape)
            sketch2 = sketch2.workplane(offset=self.config.mouthpiece_height, origin=(0, 0))\
                .transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=self.config.mouthpiece_height)
            sketch2 = self._build_sketch(sketch2, shape=shape)
            part = part.union(sketch2.loft(ruled=True), clean=True)
        position_x += self.config.mouthpiece_height
        position_z += self.config.mouthpiece_height

        w = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length2 - self.config.mouthpiece_height * 2)
        w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)
        part = part.union(w, clean=True)
        position_z += self.config.mouthpiece_length2 - self.config.mouthpiece_height * 2

        if self.config.round_corners:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch1_centered = self._recenter(sketch1, shape=shape)
            part = part.union(sketch1_centered.revolve(NINETY_DEGREE, axisStart=cq.Vector(0, -self.config.mouthpiece_height, 0), axisEnd=cq.Vector(1, -self.config.mouthpiece_height, 0)), clean=True)
        else:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch2 = self._recenter(sketch1, shape=shape)
            sketch2 = sketch2.workplane(offset=self.config.mouthpiece_height)\
                .transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=self.config.mouthpiece_height) # TODO: why does 90° not work?
            sketch2 = self._build_sketch(sketch2, shape=shape)
            part = part.union(sketch2.loft(ruled=True), clean=True)
        position_x -= self.config.mouthpiece_height
        position_z += self.config.mouthpiece_height

        w = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
        w = w.transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0))
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length3)
        w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)
        part = part.union(w, clean=True)
        position_x -= self.config.mouthpiece_length3

        if self.config.round_corners:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = sketch1.transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch1_centered = self._recenter(sketch1, shape=shape)
            part = part.union(sketch1_centered.revolve(NINETY_DEGREE, axisStart=cq.Vector(0, self.config.cover_config.height, 0),axisEnd=cq.Vector(-1, self.config.cover_config.height, 0)), clean=True)
        else:
            sketch1 = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
            sketch1 = sketch1.transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0))
            sketch1 = self._build_sketch(sketch1, shape=shape)
            sketch2 = self._recenter(sketch1, shape=shape)
            sketch2 = sketch2.workplane(offset=self.config.mouthpiece_height / 2) \
                .transformed(rotate=cq.Vector(-NINETY_DEGREE, 0, 0)).workplane(
                offset=self.config.mouthpiece_height / 2)
            sketch2 = self._build_sketch(sketch2, shape=shape)
            part = part.union(sketch2.loft(ruled=True), clean=True)
        position_x -= self.config.mouthpiece_height / 2
        position_z += self.config.mouthpiece_height / 2

        w = cq.Workplane("YZ").workplane(offset=position_x).transformed(rotate=cq.Vector(NINETY_DEGREE, 0, 0)).workplane(offset=position_z)
        w = self._build_sketch(w, shape=shape)
        w = self._recenter(w, shape=shape)
        w = w.workplane(offset=self.config.mouthpiece_length4)
        w = self._build_sketch(w, shape=shape)
        w = w.loft(ruled=True)
        part = part.union(w, clean=True)

        return part

    def _build_body_knobs_body_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.RightLower,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.z_position += self.config.body_config.mouthpiece_height
        knob_config_mouthpiece.direction = KnobDirection.Right
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)\
            .union(cord_knob_mouthpiece_right_down, clean=True).union(cord_knob_mouthpiece_left_down, clean=True)
        return knobs

    def _build_body_knobs_mouth_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x + self.config.mouthpiece_length1
                                                       - self.config.mouthpiece_length3
                                                       - self.config.mouthpiece_height / 2,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=self.config.mouthpiece_height / 2
                                                       + self.config.mouthpiece_length2
                                                       + self.config.mouthpiece_length4,
                                            direction=KnobDirection.LeftUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.x_position -= self.config.body_config.mouthpiece_height
        knob_config_mouthpiece.direction = KnobDirection.Upper
        cord_knob_mouthpiece_right_down = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Left
        cord_knob_mouthpiece_left_down = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)\
            .union(cord_knob_mouthpiece_right_down, clean=True).union(cord_knob_mouthpiece_left_down, clean=True)
        return knobs


    def _build_cover_knobs_body_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=0,
                                            direction=KnobDirection.RightUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)
        return knobs

    def _build_cover_knobs_mouth_side(self) -> cq.Workplane:
        knob_config_mouthpiece = KnobConfig(config=self.config._polymoe_config,
                                            x_position=self.config.start_pos.x + self.config.mouthpiece_length1
                                                       - self.config.mouthpiece_length3
                                                       - self.config.mouthpiece_height / 2,
                                            y_position=self.config.mouthpiece_width,
                                            z_position=self.config.mouthpiece_height / 2
                                                       + self.config.mouthpiece_length2
                                                       + self.config.mouthpiece_length4,
                                            direction=KnobDirection.RightUpper,
                                            side=KnobSide.Left)
        knob_config_mouthpiece.size_factor = self.config.mouthpiece_knob_size_factor
        cord_knob_mouthpiece_left = Knob(knob_config_mouthpiece).build()
        knob_config_mouthpiece.side = KnobSide.Right
        cord_knob_mouthpiece_right = Knob(knob_config_mouthpiece).build()
        knobs = cord_knob_mouthpiece_left.union(cord_knob_mouthpiece_right, clean=True)
        return knobs

    def build(self) -> List[cq.Workplane]:
        body = self._build_mouthpiece_part(shape=CurvedMouthpiece._Shape.body)
        body_knobs_body_side = self._build_body_knobs_body_side()
        body_knobs_mouth_side = self._build_body_knobs_mouth_side()
        body = body.union(body_knobs_body_side, clean=True).union(body_knobs_mouth_side, clean=True)
        cover = self._build_mouthpiece_part(shape=CurvedMouthpiece._Shape.cover)
        cover_knobs_body_side = self._build_cover_knobs_body_side()
        cover_knobs_mouth_side = self._build_cover_knobs_mouth_side()
        cover = cover.union(cover_knobs_body_side, clean=True).union(cover_knobs_mouth_side, clean=True)
        self._built = [body, cover]
        return self._built

    def carve_stacking(self, mouthpiece_cover_obj: cq.Workplane):
        if self._built is None:
            self.build()
        for part_index in range(len(self._built)):
            self._built[part_index] = self._built[part_index].cut(mouthpiece_cover_obj, clean=True)
        return self._built

    # volume in cm^3
    def volume(self):
        volume = 0
        if self._built is None:
            self.build()
        for part in self._built:
            for shape in part.all():
                volume += shape.val().Volume()
        return volume / 1000

    # weight in g
    def weight(self):
        volume = self.volume()
        weight = volume * self.config.density
        return weight



# polymoe.py

class WeightAndVolumeCounter:
    def __init__(self):
        self.volume = 0
        self.weight = 0

    def add(self, obj, name):
        volume = obj.volume()
        weight = obj.weight()

        print(f"volume {name}: {volume:.1f} cm^3")
        print(f"weight {name}: {weight:.1f} g")

        self.volume += volume
        self.weight += weight

    def print_sum(self):
        print(f"weight total: {self.weight:.1f} g")
        print(f"volume total: {self.volume:.1f} cm^3")

def main(config: PolyMoeConfig):
    body_config = FluteBodyConfig(config)
    body = FluteBody(body_config)

    fingerboard_config = CoverConfig(body_config)
    stringplate_config = StringPlateConfig(body_config, fingerboard_config)
    hook_config = CarryHookConfig(body_config=body_config, stringplate_config=stringplate_config)
    mouthpiece_cover_config = MouthpieceCoverplateConfig(body_config, fingerboard_config)
    wings_config = WingsConfig(body_config)
    if config.use_curved_mouthpiece:
        curved_mouthpiece_config = CurvedMouthpieceConfig(body_config=body_config, cover_config=fingerboard_config,
                                                          mouthpiece_coverplate_config=mouthpiece_cover_config)

    hook = CarryHook(hook_config)
    fingerboard = Cover(fingerboard_config)
    wings = Wings(wings_config)
    stringplate = Wings(stringplate_config)
    mouthpiece_cover = MouthpieceCoverplate(mouthpiece_cover_config)
    if config.use_curved_mouthpiece:
        curved_mouthpiece = CurvedMouthpiece(curved_mouthpiece_config)

    hook_obj = hook.build()
    body_with_extensions_objs = body.build()
    mouthpiece_cover_obj = mouthpiece_cover.build()
    fingerboard.build()
    fingerboard.carve_body_stacking(body_with_extensions_objs)
    #fingerboard.remove_artifacts()
    fingerboard_and_extensions_objs = fingerboard.carve_mouthpiece_stacking(mouthpiece_cover_obj)
    wings_obj = wings.build()
    stringplate_obj = stringplate.build()
    if config.use_curved_mouthpiece:
        curved_mouthpiece_objs = curved_mouthpiece.build()

    body.carve_mouthpiece_stacking(mouthpiece_cover_obj)
    body.carve_hook_stacking(hook_obj)
    body_with_extensions_objs_and_stacking = body.add_stacking()

    if config.use_curved_mouthpiece:
        mouthpiece_cover_obj = mouthpiece_cover.move_coverplate_behind_curved_mouthpiece()
        #TODO: why is this intersection not working? curved_mouthpiece_objs = curved_mouthpiece.carve_stacking(mouthpiece_cover_obj)

    flute = cq.Assembly()
    counter = WeightAndVolumeCounter()
    for index, part in enumerate(body_with_extensions_objs_and_stacking):
        if index < len(body_with_extensions_objs) - 1:
            text = f"body extension {index + 1}"
        else:
            text = "body"
        flute = flute.add(part, name=text, color=cq.Color("burlywood3"))
    for index, part in enumerate(fingerboard_and_extensions_objs):
        if index < len(fingerboard_and_extensions_objs) - 1:
            text = f"fingerboard extension {index + 1}"
        else:
            text = "fingerboard"
        flute = flute.add(part, name=text, color=cq.Color("burlywood3"))
    flute = flute.add(wings_obj, name="wings", color=cq.Color("burlywood3"))
    flute = flute.add(stringplate_obj, name="stringplate", color=cq.Color("burlywood3"))
    flute = flute.add(hook_obj, name="hook", color=cq.Color("burlywood3"))
    if config.use_curved_mouthpiece:
        flute = flute.add(curved_mouthpiece_objs[0], name="curved mouthpiece body", color=cq.Color("burlywood3"))
        flute = flute.add(curved_mouthpiece_objs[1], name="curved mouthpiece cover", color=cq.Color("burlywood3"))
        counter.add(curved_mouthpiece, "curved mouthpiece")
    flute = flute.add(mouthpiece_cover_obj, name="mouthpiece cover", color=cq.Color("burlywood3"))

    for pipe_index in range(body_config.num_pipes):
        if pipe_index in config.frequency_of_lowest_hole:
            frequency_of_lowest_hole = config.frequency_of_lowest_hole[pipe_index]
        else:
            frequency_of_lowest_hole = 0
        if pipe_index in config.pitchstrip_layout.keys():
            pitchstrip_layout = config.pitchstrip_layout[pipe_index]
        else:
            pitchstrip_layout = "continuous"
        if pitchstrip_layout == "continuous":
            strip_config = PitchStripConfig.continuous(body_config, fingerboard_config, pipe_index=pipe_index)
        elif pitchstrip_layout == "octaves":
            strip_config = PitchStripConfig.octaves(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "diatonic":
            strip_config = PitchStripConfig.diatonic(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "chromatic":
            strip_config = PitchStripConfig.chromatic(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "pentatonic":
            strip_config = PitchStripConfig.pentatonic(body_config, fingerboard_config, pipe_index=pipe_index, frequency_of_lowest_hole=frequency_of_lowest_hole)
        elif pitchstrip_layout == "bourdon":
            strip_config = PitchStripConfig.bourdon(body_config, fingerboard_config, pipe_index=pipe_index)
        else:
            raise Exception(f"unexpected pitchstrip layout {pitchstrip_layout}")
        strip = PitchStrip(strip_config)
        strip_obj = strip.build()
        flute.add(strip_obj, name=f"pitch strip {pipe_index + 1}", color=cq.Color("gainsboro"))
        counter.add(strip, f"pitch strip {pipe_index + 1}")

    counter.add(body, "body")
    counter.add(fingerboard, "fingerboard")
    counter.add(wings, "wings")
    counter.add(mouthpiece_cover, "mouthpiece cover")
    counter.add(stringplate, "stringplate")
    counter.add(hook, "hook")

    counter.print_sum()

    return flute

# %%


config = PolyMoeConfig()
assembly = main(config)
if config.save_step:
   assembly.save('polymoe.step')
if 'show_object' in globals():
   compound = assembly.toCompound()
   show_object(compound)
